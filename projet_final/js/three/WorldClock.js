var WorldClock = (function () {
  var clock;

  function createInstance() {
    var clock = new THREE.Clock();
	 return clock;
  };

  return {
	 getInstance: function() {
      if (!clock) {
		  clock = createInstance();
	 }
		return this;
	 }
  };

WorldClock.prototype.getTime = function() {
  var elapsdTime = this.clock.getElapsedTime();
  return elapsedTime%24;
}
})();

/* INF5071 - Infographie - Travail Pratique 2 - UQAM A2015
 *
 *   Etudiant   : Stephane Olssen
 *   Code perm  : OLSS14037300
 *   Courriel   : olssen.stephane@courrier.uqam.ca
 *   
 *   Etudiant   : Thomas Robert de Massy
 *   Code perm  : ROBT24057409
 *   Courriel   : robert_de_massy.thomas@courrier.uqam.ca     
 *
 *   Professeur : Alexandre Blondin Massé
 */
 
var Building = function( thescene, building_name, building_type, building_pos, building_size, building_rot ) {
      
    // Constantes (publiques)
    
    // TODO: Ne marche pas, ne peuvent pas être utilisées de l'extérieur. JE LES AI MIS DANS LES VARIABLES PRIVÉES
    
    this.TYPE_SKYSCRAPER_A   =  0;
    this.TYPE_SKYSCRAPER_B   =  1;
    this.TYPE_HOUSE_A1       = 10;  
    this.TYPE_HOUSE_A2       = 11;    
    this.TYPE_HOUSE_B1       = 12;      
    this.TYPE_HOUSE_C1       = 13;    
    this.TYPE_HOUSE_A_GARAGE = 20;  

    this.TYPE_HOUSE_CONSTRUCT= 30;    
    
    this.PANELSNUM           =  4;
    
    // Membres publiques
    
    // Membres privés
      
    var that       = this;
    var arg_length = arguments.length;    
    var scene;
    var position;
    var rotation; // rotation sur l'axe des y, TODO: La rotation du building n'est pas implémentée encore!
    var type;
    var rooftype;
    var width;
    var length;
    var stories;
    var scale         = 1.0;
    var name          = "";
    var panels        = new Array();
    var preloadpanels = new Array();
    var collider;
    
    // Constantes (privées)
    
    var BLOC_WIDTH    = 3.0;
    var BLOC_HEIGHT   = 1;     
    var BLOC_OFFSET   = 0.019;
    
    // Modèles
    var MODEL_ROOT  = 'assets/models_lowres/buildings/';    
    var MODELS      = new Array();
    
    
    
    /** "Constructeur"
     * 
     */		
    ( function () {  
        
        if ( arg_length !== 0 ) {
            
            scene    = thescene;
            name     = building_name;        
            position = building_pos;     // a THREE.Vector3( x, y, z );
        
            // TODO: La rotation du building n'est pas implémentée encore!
            if ( building_rot !== undefined )
                rotation = building_rot; // rotation sur l'axe des y      
            else
                rotation = 0; 
        
            type     = building_type;
            width    = building_size.x;  // a THREE.Vector3( x, y, z );
            length   = building_size.z;  // a THREE.Vector3( x, y, z );
            stories  = building_size.y;  // a THREE.Vector3( x, y, z );
            rooftype = 1;

            switch ( type ) {
            
                case that.TYPE_SKYSCRAPER_A   : MODELS   = [ 'building_a_0.json', 'building_a_1.json', 'building_a_2.json', 'building_a_3.json', 'building_a_4.json' ];
                                                break;
                case that.TYPE_SKYSCRAPER_B   : MODELS   = [ 'building_b_0.json', 'building_b_1.json', 'building_b_2.json', 'building_b_3.json', 'building_a_4.json' ];
                                                break;
                case that.TYPE_HOUSE_A1       : MODELS   = [ 'house_a_0.json', 'house_a_1.json', 'house_a_2.json', 'house_a_3.json', 'house_a_4.json' ];
                                                rooftype = 1;
                                                break;  
                case that.TYPE_HOUSE_A2       : MODELS   = [ 'house_a_0.json', 'house_a_1_2.json', 'house_a_2.json', 'house_a_3.json', 'building_a_4.json' ];
                                                break;                                               
                case that.TYPE_HOUSE_B1       : MODELS   = [ 'house_b_0.json', 'house_b_1.json', 'house_b_2.json', 'house_b_3.json', 'building_a_4.json' ];
                                                break;                                                                                                 
                case that.TYPE_HOUSE_C1       : MODELS   = [ 'house_a_0.json', 'house_a_1.json', 'house_c_2.json', 'house_a_3.json', 'building_a_4.json' ];
                                                break;                                                
                case that.TYPE_HOUSE_A_GARAGE : MODELS   = [ 'house_a_2.json', 'house_a_1_garage.json', 'house_a_2.json', 'house_a_3.json', 'building_a_4.json' ];    
                                                break;                                                
                case that.TYPE_HOUSE_CONSTRUCT: MODELS   = [ 'house_construction_0.json', 'house_construction_1.json', 'house_construction_0.json', 'house_construction_0.json', 'house_construction_0.json' ];    
                                                break;
                                                
                                                
            }
        
        }
        
    } )();
    
    
    
    // Getters/Setters
    this.setScene           = function ( s )  { scene = s; };        
    this.getScene           = function ()     { return scene; };     
    this.setName            = function ( n )  { name = n; };        
    this.getName            = function ()     { return name; };      
    this.setPosition        = function ( p )  { position = p; };        
    this.getPosition        = function ()     { return position; };     
    this.setRotation        = function ( r )  { rotation = r; };        
    this.getRotation        = function ()     { return rotation; };  
    this.setType            = function ( t )  { type = t; };        
    this.getType            = function ()     { return type; }; 
    this.setWidth           = function ( w )  { width = w; };        
    this.getWidth           = function ()     { return width; }; 
    this.setLength          = function ( l )  { length = l; };        
    this.getLength          = function ()     { return length; }; 
    this.setStories         = function ( s )  { stories = s; };        
    this.getStories         = function ()     { return stories; }; 
    this.setScale           = function ( s )  { scale = s; };        
    this.getScale           = function ()     { return scale; };     
    this.addPanel           = function ( p )  { panels.push( p ); };        
    this.setPanels          = function ( p )  { panels = p; };     
    this.getPanels          = function ()     { return panels; }; 
    this.setPreloadPanels   = function ( pp ) { preloadpanels = pp; };     
    this.getPreloadPanels   = function ()     { return preloadpanels; };     
    this.getRoofType        = function ()     { return rooftype; }; 
    this.setCollider        = function ( c )  { collider = c; };        
    this.getCollider        = function ()     { return collider; };     
    
    // Getters (constantes privées)
    this.get_BLOC_WIDTH     = function ()     { return BLOC_WIDTH; }; 
    this.get_BLOC_HEIGHT    = function ()     { return BLOC_HEIGHT; }; 
    this.get_BLOC_OFFSET    = function ()     { return BLOC_OFFSET; };      
    this.get_MODEL_ROOT     = function ()     { return MODEL_ROOT; }; 
    this.get_MODELS         = function ()     { return MODELS; }; 
    
    return this;
    
};



Building.prototype.preloadPanels = function ( callback, preloadonly ) {
   
    var that      = this;       
    var panel0    = new Model( "preload_panel_0", this.get_MODEL_ROOT() + this.get_MODELS()[0] );
    this.preloads = new Array( this.PANELSNUM );
    
    panel0.loadComplex( function( model0 ) {            
                    
        that.preloads[0] = model0;
        
        var panel1= new Model( "preload_panel_1", that.get_MODEL_ROOT() + that.get_MODELS()[1] );
        
        panel1.loadComplex( function( model1 ) {            
                    
            that.preloads[1] = model1;
        
            var panel2 = new Model( "preload_panel_2", that.get_MODEL_ROOT() + that.get_MODELS()[2] );
    
            panel2.loadComplex( function( model2 ) {            
                    
                that.preloads[2] = model2;
        
                var panel3 = new Model( "preload_panel_3", that.get_MODEL_ROOT() + that.get_MODELS()[3] );
    
                panel3.loadComplex( function( model3 ) {            
                    
                    that.preloads[3] = model3;
        
                    var panel4 = new Model( "preload_panel_4", that.get_MODEL_ROOT() + that.get_MODELS()[4] );
    
                    panel4.loadComplex( function( model4 ) {            
                    
                        that.preloads[4] = model4;
                        
                        // Sauvegarde les preloads
                        that.setPreloadPanels( that.preloads );
        
                        // Caller la fonction de creation 
                        if ( preloadonly === undefined || preloadonly === false )
                            that.create( callback, that.preloads );
                        else if ( preloadonly !== undefined && preloadonly === true && callback !== undefined )
                            callback( that.preloads );
                        
                    } ); 
                
                } );  
                
            } );  
        
        } );         
    
    } );    
        
};



/*
 *  NOTE: Toujours appeler cette fonction sans arguments, la fonction de preload va être
 *        automatiquement appelée si le préload est activé pour réappeler la fonction en
 *        lui fournissant l'argument preloadpanels.
 *
 *  ATTENTION le toit ne marche pas si preload n'est pas à true, c'est juste pour ne pas trop perdre de temps!
 *
 */
Building.prototype.create = function ( callback, preloadpanels ) {
    
    var that          = this;
    var preload       = true; // Changer à false pour désactiver l'optimisation de preload des panneaux
    var rotation      = 0.0;
    var panelnum;
    var mur;
    var type;    
    var pos           = this.getPosition();
    var x_offset      = 0.0;
    var z_offset      = 0.0;
    var panelwidth    = this.get_BLOC_WIDTH()  * this.getScale();
    var panelheight   = this.get_BLOC_HEIGHT() * this.getScale();
    var paneloffset   = this.get_BLOC_OFFSET() * this.getScale();
    var width         = this.getWidth();
    var length        = this.getLength();
    var optimize      = true;
    var brickstexture;
    this.loadedpanels;
    
    if ( preloadpanels === undefined && preload ) {
        
        this.preloadPanels( callback ); 
        return; 
        
    } else if ( preload && preloadpanels !== undefined )        
        this.loadedpanels = preloadpanels;        
    else
        this.loadedpanels = new Array( this.PANELSNUM );
    
    // Place les panneaux un étage à la fois
    for ( var etage = 0; etage < this.getStories(); ++etage ) {
        
        // Place les panneaux des 4 cotés du building
        for ( var cote = 0; cote < 4; ++cote ) {     

            // Calcule la position et le nombre de panneaux de chaque cotés
            switch ( cote ) {
            
                case 0 : x_offset = pos.x - ( ( panelwidth * width )  / 2 ) + panelwidth / 2; 
                         z_offset = pos.z - ( ( panelwidth * length ) / 2 ) + paneloffset;
                         panelnum = width;
                         break;
                case 1 : x_offset = pos.x + ( ( panelwidth * width )  / 2 ) - paneloffset;
                         z_offset = pos.z - ( ( panelwidth * length ) / 2 ) + panelwidth / 2;
                         panelnum = length;
                         break;
                case 2 : x_offset = pos.x + ( ( panelwidth * width )  / 2 ) - panelwidth / 2;
                         z_offset = pos.z + ( ( panelwidth * length ) / 2 ) - paneloffset;
                         panelnum = width;
                         break;
                case 3 : x_offset = pos.x - ( ( panelwidth * width )  / 2 ) + paneloffset; 
                         z_offset = pos.z + ( ( panelwidth * length ) / 2 ) - panelwidth / 2;
                         panelnum = length;
                         break;                         
                         
            }
            
            rotation = Math.PI - ( cote * ( Math.PI / 2 ) ); // rotation de 90 degrés
            
            for ( var panel = 0; panel < panelnum; ++panel ) {              
        
                // Est-ce qu'on place l'entree?
                if ( cote === this.getRotation() && etage === 0 && panel === Math.floor( width / 2 ) )
                    type = 1;
                else if ( etage === 0 ) // Rez-de-chaussé mais pas porte
                    type = 0;
                else if ( etage === this.getStories() - 1 ) // Dernier étage
                    type = 3;
                else
                    type = 2;
                
                mur = new Model( "dummy", this.get_MODEL_ROOT() + this.get_MODELS()[type] );
                
                mur.setDeferedData ( {
                    
                    name : this.getName() + "_mur_" + etage + '_' + cote + '_' + panel,
                    pos  : new THREE.Vector3( x_offset, etage * panelheight + pos.y, z_offset ),
                    rot  : new THREE.Vector3( 0.0, rotation, 0.0 ),
                    type : type
                    
                } );                
                
                // Calcule les offsets des panneaux
                switch ( cote ) {
            
                    case 0 : x_offset += panelwidth;
                             break;
                    case 1 : z_offset += panelwidth;
                             break;
                    case 2 : x_offset -= panelwidth;
                             break;
                    case 3 : z_offset -= panelwidth;
                             break;                         
                         
                }
        
                // On doit charger/télécharger le panneau?
                if ( this.loadedpanels[type] === undefined && !preload ) {
                    
                    mur.loadComplex( function( model ) {
            
                        // Configure ses attributs
                        model.setName( model.getDeferedData().name ); 
                        model.setRotation( model.getDeferedData().rot.x, model.getDeferedData().rot.y , model.getDeferedData().rot.z );            
                        model.setPosition( model.getDeferedData().pos.x, model.getDeferedData().pos.y , model.getDeferedData().pos.z );                    
                        model.scale( that.getScale() );
                                                
                        // On l'ajoute à la scène
                        that.getScene().addModel( model );   

                        // Retire l'objet mur pour optimisation
                        if ( optimize ) {
                            
                            var bricks = model.getChild( "mur" );
                        
                            if ( bricks !== undefined ) {
                                
                                bricks.visible = false;
                            
                                bricks.parent = that.getScene().getScene();                                                    

                            }
                         
                        } 
                    
                        // Le modèle est déjà chargé/téléchargé
                        that.loadedpanels[model.getDeferedData().type] = model;
                        
                        this.addPanel( model );
         
                    } );
                    
                } else if ( this.loadedpanels[type] !== undefined ) { // Panneau déjà loader
                    
                    // Clone le model preloader
                    var newModel = this.loadedpanels[type].clone();
                                       
                    // Configure ses attributs
                    newModel.setName( mur.getDeferedData().name ); 
                    newModel.setRotation( mur.getDeferedData().rot.x, mur.getDeferedData().rot.y , mur.getDeferedData().rot.z );            
                    newModel.setPosition( mur.getDeferedData().pos.x, mur.getDeferedData().pos.y , mur.getDeferedData().pos.z );                                        
                    newModel.scale( this.getScale() );  
                                                            
                    // On l'ajoute à la scène
                    this.getScene().addModel( newModel ); 
                    
                    // Retire l'objet mur pour optimisation
                    if ( optimize ) {
                            
                        var bricks = newModel.getChild( "mur" );
                        
                        if ( bricks !== undefined ) {
                            
                            brickstexture = bricks.material.clone();
                            
                            bricks.visible = false;
                            
                            bricks.parent = that.getScene().getScene();                                                    
                            
                        }
                         
                    }                         

                    this.addPanel( newModel );                    
                    
                }
                
            }
            
        }
            
    } // Fin for ( var etage = 0; etage < this.getStories(); ++etage )
        
    // TODO: ATTENTION le toit ne marche pas si preload n'est pas à true, c'est juste pour ne pas trop perdre de temps!  
    // Affiche le toit
    if ( this.getRoofType() === 0 && this.getType() !== this.TYPE_HOUSE_CONSTRUCT ) { // Toit plat
        
        for ( var x = 0; x < width; ++x ) {
        
            for ( var z = 0; z < length; ++z ) {
            
                var roof = this.loadedpanels[4].clone();
            
                roof.setName( this.getName() + "_roof_" + x + "_" + z );                       
                roof.setPosition( x * panelwidth + pos.x - panelwidth / 2 + paneloffset, pos.y + panelheight * this.getStories(), z * panelwidth + pos.z - panelwidth / 2 + paneloffset );                                        
                roof.scale( this.getScale() );                
                    
                // On l'ajoute à la scène
                this.getScene().addModel( roof, false );  

                this.addPanel( roof );             
            
            }
        
        }        
        
    } else if ( this.getType() !== this.TYPE_HOUSE_CONSTRUCT ) { // Toit plus complexe
    
        // TODO: Pour l'instant ne marche que quand preload est activé
        if ( !preload || this.loadedpanels[type] === undefined )
            return;
                
        var roof = this.loadedpanels[4].clone();
            
        roof.setName( this.getName() + "_roof_" + x + "_" + z );                       
        roof.setPosition( pos.x, pos.y + panelheight * this.getStories(), pos.z );                                        
  
        //roof.scale( this.getScale() );  
        roof.getModel().scale.set ( this.getScale() * width, this.getScale(), this.getScale() * length );
                   
        // On l'ajoute à la scène
        this.getScene().addModel( roof, false );  

        this.addPanel( roof );                   
        
    } 
    
    // Ajoute le building à la liste des building de la scène
    this.getScene().addBuilding( this ); 
    
    // Pas de collider pour le building en construction
    if ( this.getType() !== this.TYPE_HOUSE_CONSTRUCT ) {   
    
        // Ajoute la boite invisibles de physique pour la détection de collision
        if ( optimize && brickstexture !== undefined && brickstexture.map !== undefined ) {
            
            brickstexture.map.wrapS = brickstexture.wrapT = THREE.RepeatWrapping;
            brickstexture.map.repeat.set( width * 3, this.getStories() * 2 );//( 7, 10 );               
        
            // On doit changer le nom pour éviter que le maping soit modifier par un autre building!
            // TODO: Faire un renommage plus intelligent ici!
            brickstexture.name = brickstexture.name + '_' + Math.floor( 1000000000 * Math.random() );
                           
            // On doit faire un update!                           
            brickstexture.needsUpdate = true;
                
            physmaterial  = new THREE.MeshPhongMaterial( { color: 0xCCCCCC, transparent: false, opacity: 1, map: brickstexture.map, shininess: 0 } );
          
        } else
            physmaterial   = new THREE.MeshBasicMaterial( { color: 0xFFFFFF, transparent: true, opacity: 0 } );
    
        var physbox        = new Physijs.BoxMesh( new THREE.CubeGeometry( panelwidth * width, panelheight * this.getStories(), panelwidth * length ), physmaterial, 0 );
    
        if ( !optimize )
            physbox.visible = false;
    
        physbox.position.x = pos.x;
        physbox.position.y = pos.y + ( panelheight * this.getStories() ) / 2;
        physbox.position.z = pos.z;
        physbox.name       = "collider";
    
        // Ajoute le MESH de collision à la scène et à la liste des modèle du building
        var collidermodel = new Model( this.getName() + "_collider" );
        collidermodel.setModel( physbox );
        collidermodel.setComplex( false );
        this.addPanel( collidermodel );
        this.setCollider( collidermodel );
        this.getScene().addModel( collidermodel );
    
        // Ceci est nécessaire pour le caméra culling pour faire apparaitre tout le building en même temps que les fenêtres
        var panels = this.getPanels();
        for ( var i = 0; i < panels.length; ++i )
            panels[i].setCollider( collidermodel );
        
    }
    
    // Collider du balcon
    if ( this.getType() !== this.TYPE_HOUSE_A1 || this.getType() !== this.TYPE_HOUSE_B1 || this.getType() !== this.TYPE_HOUSE_C1 ) {
        
        var panels = this.getPanels();
        
        for ( var i = 0; i < panels.length; ++i ) {
            
            if ( panels[i].getChild( 'balcon' ) !== undefined && panels[i].getModel().position.y === 0 ) {
                
                var balconPosition = panels[i].getModel().position;
                var balconRotation = panels[i].getModel().rotation;//panels[i].getRotation();
                var balconSize     = new THREE.Vector3( 1.8 * this.getScale(), 0.8 * this.getScale(), 1.45 * this.getScale() );                
                var balconCollider = this.getScene().addCollider( 0, balconPosition, balconRotation, balconSize, 0, 0 );
                
            }
            
        }
        
    }
    
    // Appel de la fonction callback en mode preload
    if ( callback !== undefined && preload )
        callback( this );    
    
};



Building.prototype.remove = function () {
    
    var scene  = this.getScene();
    var models = this.getPanels();
    
    for ( var i = 0; i < models.length; ++i ) {
        
        if ( models[i] !== undefined )
            scene.removeModel( models[i] );
        
    } 
    
    this.setPanels( new Array() );

};



Building.prototype.tint = function ( color, part ) {    
        
        var panels     = this.getPanels();
        var tintedpart = "mur";
        
        if ( part !== undefined )
            tintedpart = part;

        for ( index in panels ) {
            
            var model = panels[index].getModel();
            
            model.traverse( function( child ) {
                        
                if ( child instanceof THREE.Mesh ) {
                    
                    if ( child.name === tintedpart ) {
                                                        
                        child.material = new THREE.MeshPhongMaterial( {
                        
                            color       : color,
                            map         : child.material.map,
                            transparent : false
                        
                        } );
                    
                    }
                    
                }
                                        
            } );
            
        }
        
};


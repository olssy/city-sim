/* INF5071 - Infographie - Travail Pratique 2 - UQAM A2015
 *
 *   Etudiant   : Stephane Olssen
 *   Code perm  : OLSS14037300
 *   Courriel   : olssen.stephane@courrier.uqam.ca
 *   
 *   Etudiant   : Thomas Robert de Massy
 *   Code perm  : ROBT24057409
 *   Courriel   : robert_de_massy.thomas@courrier.uqam.ca     
 *
 *   Professeur : Alexandre Blondin Massé
 */
 
 
 
var ID = {    
    
    BUILDING              :  0,
    CAR                   : 10,
    CONE                  : 11,
    CONES_LINE            : 12,
    BOX                   : 13,
    BOXES_PILE            : 14,    
    BALL                  : 15,
    THREE                 : 16,
    FOREST                : 17,
    PARK                  : 18,
    STOP                  : 19,
    STOP_4_DIRECTIONS     : 20,
    TRAFFIC_LIGHT         : 21,
    TRAFFIC_LIGHTS_SET    : 22,
    TRASH_CAN             : 23,
    TABLE                 : 24,
    STREET_LIGHT          : 25,
    TELEPHONE_POLE        : 26,
    ELECTRIC_WIRES        : 27,
    ELECTRIC_LINE         : 28,
    BARRIER               : 29,
    FENCES                : 30,
    FENCES_BOX            : 31,
    BASCULE               : 32,
    CONSTRUCTION_SITE     : 33,
    FENCES_WALL           : 34,
    
    // Les types de buildings
    
    TYPE_SKYSCRAPER_A     :  0,
    TYPE_SKYSCRAPER_B     :  1,
    TYPE_HOUSE_A1         : 10,  
    TYPE_HOUSE_A2         : 11,    
    TYPE_HOUSE_B1         : 12,     
    TYPE_HOUSE_C1         : 13,     
    TYPE_HOUSE_A_GARAGE   : 20,
    
    TYPE_HOUSE_CONSTRUCT  : 30,

    // Directions
    NORTH                 : 0,
    WEST                  : 1,
    SOUTH                 : 2,
    EAST                  : 3
    
}

ID.items       = { BUILDING: ID.BUILDING, CAR: ID.CAR, CONE: ID.CONE, CONES_LINE: ID.CONES_LINE, BOX: ID.BOX, BOXES_PILE: ID.BOXES_PILE, BALL: ID.BALL, THREE: ID.THREE, FOREST: ID.FOREST, STOP: ID.STOP, STOP_4_DIRECTIONS: ID.STOP_4_DIRECTIONS, TRAFFIC_LIGHT: ID.TRAFFIC_LIGHT, TRAFFIC_LIGHTS_SET: ID.TRAFFIC_LIGHTS_SET, TRASH_CAN: ID.TRASH_CAN, TABLE: ID.TABLE, STREET_LIGHT: ID.STREET_LIGHT, TELEPHONE_POLE: ID.TELEPHONE_POLE, ELECTRIC_WIRES: ID.ELECTRIC_WIRES, ELECTRIC_LINE: ID.ELECTRIC_LINE, BARRIER: ID.BARRIER, FENCES: ID.FENCES, FENCES_BOX: ID.FENCES_BOX, FENCES_WALL: ID.FENCES_WALL, BASCULE: ID.BASCULE };
ID.industrial  = { TYPE_SKYSCRAPER_A: ID.TYPE_SKYSCRAPER_A, TYPE_SKYSCRAPER_B: ID.TYPE_SKYSCRAPER_B };
ID.residential = { TYPE_HOUSE_A1: ID.TYPE_HOUSE_A1, TYPE_HOUSE_A2: ID.TYPE_HOUSE_A2, TYPE_HOUSE_B1: ID.TYPE_HOUSE_B1, TYPE_HOUSE_C1: ID.TYPE_HOUSE_C1, TYPE_HOUSE_A_GARAGE: ID.TYPE_HOUSE_A_GARAGE };
ID.publique    = { PARK: ID.PARK, CONSTRUCTION_SITE: ID.CONSTRUCTION_SITE };
ID.directions  = { NORTH: ID.NORTH, WEST: ID.WEST, SOUTH: ID.SOUTH, EAST: ID.EAST };



var Objects3D = function( thescene, basehref ) {
    
    this.DEBUG = false;
    
    // Membres privés
      
    var that = this;
    var scene;
    var scale;
    var baseDir;
    var preloads    = {};
    var obj_counter = {};
    
    /** "Constructeur"
     * 
     */		
    ( function () {               
        
        scene = thescene;
        scale = 1;
        
        if ( basehref === undefined )
            baseDir = 'assets/models_lowres/';
        else
            baseDir = basehref;
        
    } )();
    
    this.setScene          = function ( scn )   { scene = scn; };  
    this.getScene          = function ()        { return scene; };
    this.incObjCounter     = function ( id )    { return obj_counter[id] = obj_counter[id] === undefined ? 1 : obj_counter[id] + 1; }; 
    this.decObjCounter     = function ( id )    { return obj_counter[id] = obj_counter[id] === undefined ? undefined : obj_counter[id] - 1; };
    this.getObjCounter     = function ( id )    { return obj_counter[id]; };
    this.getObjsCounters   = function ()        { return obj_counter; };
    this.clearObjCounter   = function ( id )    { obj_counter[id] = undefined; };
    this.clearObjsCounters = function ()        { obj_counter = {}; };
    this.getBaseDir        = function ()        { return baseDir; };
    this.getScale          = function ()        { return scale; };
    this.setScale          = function ( s )     { scale = s; };
    this.setPreload        = function ( id, p ) { preloads[id] = p; };
    this.setPreloads       = function ( p )     { preloads = p; };
    this.getPreload        = function ( id )    { return preloads[id]; };
    this.getPreloads       = function ()        { return preloads; };   
    this.clearPreload      = function ( id )    { preloads[id] = undefined; };
    this.clearPreloads     = function ( p )     { preloads = {}; };
    
};



Objects3D.prototype.add = function ( id, position, rotation, params, callback ) {
           
    switch ( id ) {
        
        case ID.BUILDING :            return this.addBuilding( position, rotation, params, callback );
                                      break;
                                      
        case ID.CAR :                 // TODO: SI ON A DU TEMPS
                                      break;

        case ID.CONE :                return this.addCone( position, rotation, params, callback );
                                      break;

        case ID.CONES_LINE :
                                      break;

        case ID.BOX :                 return this.addBox( position, rotation, params, callback ); 
                                      break;

        case ID.BOXES_PILE :
                                      break;

        case ID.BALL :                return this.addBall( position, rotation, params, callback );
                                      break;

        case ID.TREE :                return this.addTree( position, rotation, params, callback );
                                      break;

        case ID.FOREST :              return this.addForest( position, rotation, params, callback );
                                      break;

        case ID.PARK :                return this.addParc( position, rotation, params, callback );
                                      break;  
                                      
        case ID.CONSTRUCTION_SITE :   return this.addConstructionParc( position, rotation, params, callback );
                                      break;

        case ID.STOP :                return this.addStop( position, rotation, params, callback ); 
                                      break; 

        case ID.STOP_4_DIRECTIONS :   return this.addStopsSet( position, rotation, params, callback );
                                      break; 

        case ID.TRAFFIC_LIGHT :       return this.addTrafficLight( position, rotation, params, callback );
                                      break;                                       
                            
        case ID.TRAFFIC_LIGHTS_SET :  return this.addTrafficLightsSet( position, rotation, params, callback );
                                      break;                            
             
        case ID.TRASH_CAN :           return this.addTrashCan( position, rotation, params, callback );
                                      break;

        case ID.TABLE :               return this.addTable( position, rotation, params, callback );
                                      break;  

        case ID.STREET_LIGHT :        return this.addStreetLight( position, rotation, params, callback );
                                      break; 

        case ID.TELEPHONE_POLE :      return this.addTelephonePole( position, rotation, params, callback );
                                      break;

        case ID.ELECTRIC_WIRES :      return this.addElectricWires( position, rotation, params, callback );
                                      break;                                       
                            
        case ID.ELECTRIC_LINE :       return this.addElectricLine( position, rotation, params, callback );
                                      break; 
                                      
        case ID.BARRIER :             return this.addBarrier( position, rotation, params, callback );
                                      break;   

        case ID.FENCES :              return this.addFences( position, rotation, params, callback );
                                      break; 
                                      
        case ID.FENCES_WALL :         return this.addFencesWall( position, rotation, params, callback );
                                      break;                                      

        case ID.FENCES_BOX :          return this.addFencesBox( position, rotation, params, callback );
                                      break;  

        case ID.BASCULE :             return this.addBascule( position, rotation, params, callback );
                                      break;                                       
                                      
    }
    
};



// TODO: Fonction à tester
Objects3D.prototype.preloadModel = function ( name, filename, callback ) {
    
    var scale    = 1;
    var position = new THREE.Vector3( 0, 0, 0 );
    var rotation = new THREE.Vector3( 0, 0, 0 );
    
    this.loadModel( name, filename, scale, position, rotation, function() { 
    
        if ( callback !== undefined )
            callback();
        
    } , true );
    
};



// TODO: Fonction à faire
Objects3D.prototype.preloadBuilding = function ( type, callback ) {
    
    var that     = this;
    var name     = 'building_type_' + type;
    var building = new Building( this.getScene(), name, type, new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( 2, 2, 3 ), 0 );
    
    building.preloadPanels( function( panels ) {    

        if ( that.DEBUG )
             console.log( 'loading objects of building type : ' + type );     
                
        that.setPreload( name, panels );
        
        if ( callback !== undefined )
             callback();
        
    }, true );
  
};


 
Objects3D.prototype.loadModel = function ( name, filename, scale, position, rotation, callback, loadonly ) {
    
    var that        = this;
    
    // TODO: ATTENTION CA COMPTE LE NOMBRE DE COPIES PAS NECESSAIREMENT DE CLONES
    var num         = this.incObjCounter( name );
    var preloadonly = false;
    
    if ( loadonly !== undefined && loadonly === true )
        preloadonly = true;
    
    // Est-ce que le modèle de l'objet est déjà préloadé en mémoire?
    if ( this.getPreload( name ) === undefined ) {
        
        var model = new Model( name + '_' + num, this.getBaseDir() + filename );    
        model.loadComplex( function( newmodel ) {
            
            if ( that.DEBUG )
                console.log( 'loading object : ' + that.getBaseDir() + filename );         
        
            if ( !preloadonly ) {
                
                newmodel.setScale( scale * that.getScale() );
                newmodel.setPosition( position.x, position.y, position.z );
                newmodel.setRotation( rotation.x, rotation.y, rotation.z );
                
                that.getScene().addModel( newmodel );
            
            }
            
            // Ajoute l'objet au objet préloadé
            that.setPreload( name, newmodel );
        
            if ( callback !== undefined )
                callback( newmodel );
        
        } );
        return model;
    } else { // L'objet est déjà préloadé
        
        if ( this.DEBUG )
            console.log( 'clone number ' + num + ' of object : ' + this.getBaseDir() + filename ); 
        
        var newmodel = this.getPreload( name ).clone();
        
        // On se débarrasse du collider
        newmodel.setCollider( undefined );
        
        newmodel.setName( name + '_' + num );
        
        newmodel.setScale( scale * this.getScale() );
        newmodel.setPosition( position.x, position.y, position.z );
        newmodel.setRotation( rotation.x, rotation.y, rotation.z );
        
        this.getScene().addModel( newmodel );

        if ( callback !== undefined )
             callback( newmodel );        
       
          return newmodel;
    }
    
};



Objects3D.prototype.adjustPosition = function ( position ) {
    
    var adj_position = new THREE.Vector3( 0, 0, 0 );
    
    if ( position !== undefined && position.x !== undefined && position.y !== undefined && position.z !== undefined )
        adj_position = new THREE.Vector3( position.x, position.y, position.z );
    else if ( position !== undefined && position.z === undefined )
        adj_position = new THREE.Vector3( position.x, 0, position.y );  
    else if ( position !== undefined && position.z !== undefined )
        adj_position = new THREE.Vector3( position.x, 0, position.z ); 
    
    return adj_position;
    
};



Objects3D.prototype.adjustRotation = function ( rotation ) {
    
    var adj_rotation;
    
    if ( rotation !== undefined && rotation.x === undefined )
        adj_rotation = this.adjustRotation90degree( rotation );
    else if ( rotation !== undefined && rotation.x !== undefined && rotation.y !== undefined && rotation.z !== undefined )
        adj_rotation = rotation; // C'est un THREE.Vector3
    else
        adj_rotation = new THREE.Vector3( 0, 0, 0 );
    
    return adj_rotation;
    
};

    

Objects3D.prototype.adjustRotation90degree = function ( rotation ) {
    
    var adj_rotation = new THREE.Vector3( 0, 0, 0 );
    
    if ( rotation === undefined )
        return adj_rotation;
    
    switch ( rotation ) {
      
         case 0  : adj_rotation.y = 0;           break;
         case 1  : adj_rotation.y = Math.PI/2;   break;
         case 2  : adj_rotation.y = Math.PI;     break;
         case 3  : adj_rotation.y = 3*Math.PI/2; break;
         default : adj_rotation.y = 0;           break;
        
    }
    
    return adj_rotation;
    
};
    


// Création d'un building
// Paramètres : { showCollider: true/false, type: id, name: string, size: THREE.Vector3, animateLights: true/false, 
//                animateLightsProp: number, animateLightsColor: color, animateLightsVariation: number, scale: number, 
//                color1: wallcolor, color2: doorcolor, color3: roofcolor, color4: balconcolor, color5: windowcolor }
Objects3D.prototype.addBuilding = function ( position, rotation, params, callback ) {
    
    var that         = this;
    var adj_position = this.adjustPosition( position );
    var rotation90   = 0;
    var size         = new THREE.Vector3( 1, 1, 1 );    
    
    if ( params !== undefined ) {
        
        var building;               
        var type         = params.type;
        var name         = 'building_type_' + type;
        var num          = this.incObjCounter( name );
        var storepreload = true;
        
        if ( rotation !== undefined )
            rotation90 = rotation;
        
        if ( params.name !== undefined )
            name = params.name;
        
        if ( params.size !== undefined )
            size = params.size;        
        
        if ( type !== undefined ) {
            
            building = new Building( this.getScene(), name, type, adj_position, size, rotation90 );
            
            if ( params.scale !== undefined )
                building.setScale( params.scale );
            else
                building.setScale( 1.12 * this.getScale() );
            
            var preloads;
            
            if ( this.getPreload( 'building_type_' + type ) !== undefined ) {
                
                if ( this.DEBUG ) 
                    console.log( 'clone number ' + num + ' of building type : ' + type );                
                
                preloads     = this.getPreload( 'building_type_' + type );                
                storepreload = false;
                
            } else {
                
                if ( this.DEBUG )
                    console.log( 'loading objects of building type : ' + type ); 
        
            }
            
            building.create( function( newbuilding ) {                                
                
                if ( storepreload )
                    that.setPreload( 'building_type_' + type, newbuilding.getPreloadPanels() );
                
                // Teinte du building                
                if ( params.color1 !== undefined ) {
                
                    if ( newbuilding.getCollider() !== undefined )
                        newbuilding.getCollider().tint( params.color1, 'collider' );
                    newbuilding.tint( params.color1, 'mur' );
                }
            
                if ( params.color2 !== undefined )
                    newbuilding.tint( params.color2, 'door' );
            
                if ( params.color3 !== undefined )
                    newbuilding.tint( params.color3, 'roof' );
            
                if ( params.color4 !== undefined )
                    newbuilding.tint( params.color4, 'balcon' );
                
                if ( params.color5 !== undefined ) {
                    
                    newbuilding.tint( params.color4, 'window1' );                
                    newbuilding.tint( params.color4, 'window2' );
                }
                
                // Est-ce que les lumières des fenêtres du building sont animées?
                if ( params.animateLights !== undefined && params.animateLights === true ) {
                    
                    // Configuration des paramètre du callback d'animation de lumières des fenêtres
                    
                    var panels       = newbuilding.getPanels();
                    var defaultcolor = 0x555588;
                    var prob         = 100;
                    var variation    = 0;
                    var changemin    = 3000; // Le temps minimum en ms qu'une lumière doit resté dans un état
                    var maxanimdist  = that.getScene().getConfig( 'camera_culling' );
                    
                    if ( params.animateLightsColor !== undefined )
                        defaultcolor = params.animateLightsColor;
                    
                    if ( params.animateLightsProp !== undefined )
                        prob = params.animateLightsProp; 

                    // TODO: Le calcul de variation d'intensité n'est pas correct, mais donne un meilleur résultat que le calcul correcte
                    if ( params.animateLightsVariation !== undefined )
                        variation = params.animateLightsVariation;
                    
                    // Distance (x ou y, pas distance réel pour fin d'optimisation) maximum pour que les animations soient actives
                    if ( params.maxanimdist !== undefined )
                        maxanimdist = params.maxanimdist;   
                    
                    for ( var i = 0; i < panels.length; ++i ) {
                        
                        // Est-ce qu'il y a une fenêtre animable sur ce panel?
                        if ( panels[i].getChild( 'glass1' ) !== undefined ) {

                            // Calcul correcte
                            // panels[i].getAnimCallbackData().color      = [ newbuilding.rgbToInt( defaultcolor - ( Math.random() * variation ), defaultcolor - ( Math.random() * variation ), defaultcolor - ( Math.random() * variation ) ), 
                            //                                                newbuilding.rgbToInt( defaultcolor - ( Math.random() * variation ), defaultcolor - ( Math.random() * variation ), defaultcolor - ( Math.random() * variation ) ) ];                             
                            
                            panels[i].getAnimCallbackData().color          = [ defaultcolor - ( Math.random() * variation ), defaultcolor - ( Math.random() * variation ) ];  
                            panels[i].getAnimCallbackData().states         = [ false, false ]; // Les lumières des 2 fenêtres sont éteintes   
                            panels[i].getAnimCallbackData().changetime     = [ 0, 0 ];
                            panels[i].getAnimCallbackData().maxanimdist    = maxanimdist;
                            panels[i].getAnimCallbackData().forceanim      = true;
                            
                            // On doit cloner le matériel pour ne pas affecter les autres building clone
                            panels[i].getChild( 'glass1' ).material = panels[i].getChild( 'glass1' ).material.clone();
                            
                            if ( panels[i].getChild( 'glass2' ) !== undefined ) 
                                panels[i].getChild( 'glass2' ).material = panels[i].getChild( 'glass2' ).material.clone();                            
                            
                            
                            
                            // Ajoute le callback d'animation des lumières du panneau                
                            panels[i].setAnimCallback( function( scene, model ) {

                                var clock      = scene.getClock();
                                var color      = model.getAnimCallbackData().color;
                                var states     = model.getAnimCallbackData().states;
                                var changetime = model.getAnimCallbackData().changetime;
                                
                                // La première animation forcé est terminé
                                model.getAnimCallbackData().forceanim = false;
                                
                                if ( clock.isNight() ) { // On est la nuit, on allume une lumière?
                                    
                                    if ( Math.floor( Math.random() * prob ) === 1 ) {
                                    
                                        var windownum = 1; // C'est la première fenêtre qu'on modifie
                                    
                                        if ( model.getChild( 'glass2' ) !== undefined ) { // Il y a 2 fenêtres?
                                    
                                            if ( Math.random() < 0.5 )
                                                windownum = 2; // C'est la deuxième fenêtre qu'on modifie
                                        
                                        }
                                    
                                        if ( states[windownum - 1] === true && ( new Date() ).getTime() - changetime[windownum - 1] > changemin ) {
                                        
                                            model.getChild( 'glass' + windownum ).material.emissive = new THREE.Color( 0x000000 );
                                            states[windownum - 1] = false;
                                            model.getAnimCallbackData().changetime[windownum - 1] = ( new Date() ).getTime();
                                        
                                        } else if ( ( new Date() ).getTime() - changetime[windownum - 1] > changemin ) {
                                        
                                            model.getChild( 'glass' + windownum ).material.emissive = new THREE.Color( color[windownum - 1] );                                     
                                            states[windownum - 1] = true;
                                            model.getAnimCallbackData().changetime[windownum - 1] = ( new Date() ).getTime();
                                        
                                        }
                                        
                                    }
                                    
                                } else if ( clock.isDay() && ( states[0] === true || states[1] === true ) ) { // On est le jour, on doit éteindre toute les lumières
                                    
                                    setTimeout( function() {
                                        
                                        if ( model.getChild( 'glass1' ) !== undefined )
                                            model.getChild( 'glass1' ).material.emissive = new THREE.Color( 0x000000 );                                   
                                    
                                    }, Math.random() * 5000, model );
                                    
                                    setTimeout( function() {
                                    
                                        if ( model.getChild( 'glass2' ) !== undefined )
                                            model.getChild( 'glass2' ).material.emissive = new THREE.Color( 0x000000 );                                    
                                    
                                    }, Math.random() * 5000, model );                                    
                                    
                                    model.getAnimCallbackData().changetime = [ 0, 0 ];
                                    states = [ false, false ]; // Toutes les lumières sont éteintes
                                    
                                }
                                
                            } );
                            
                            
                            
                            // Ajoute le callback d'animation dans la liste des callbacks d'animations de la scène 
                            that.getScene().addToAnimCallbacks( panels[i] );
                            
                        }
                        
                    }                    
                    
                }
                
                if ( callback !== undefined )
                    callback( newbuilding );
        
            }, preloads );
            
        }
        
        return building;
        
    }
    
    return undefined;

};

    

// Création d'une lumière de rue
// Paramètres : { showCollider: true/false, light: true/false, castshadow: true/false, color: a color (0xFFFF88), defective: 0-infinity }
Objects3D.prototype.addStreetLight = function ( position, rotation, params, callback ) {
    
    var that         = this;
    var adj_position = this.adjustPosition( position );
    var adj_rotation = this.adjustRotation90degree( rotation );

    var model = this.loadModel( 'streetlight', 'streetlight.json', 1 * that.getScale(), adj_position, adj_rotation, function( newmodel ) {
        
        var collider = that.getScene().addCollider( 3, adj_position, new THREE.Vector3( 0, 0, 0 ), new THREE.Vector2( 0.052 * that.getScale(), 6.7 * that.getScale() ), 0, 0 );
        
        // On doit cloner le material pour ne pas qu'il soit changé par un autre lampadaire clone
        // TODO: DEVRAIT CLONER TOUS LES MATERIELS DANS Model.clone()
        var material = newmodel.getChild( 'light' ).material.clone();
        newmodel.getChild( 'light' ).material = material;
        
        newmodel.getChild( 'light' ).material.emissive = new THREE.Color( 0x000000 );
        newmodel.getChild( 'light' ).material.color    = new THREE.Color( 0x555555 );
        
        if ( params !== undefined ) {                                 
            
            // Est-ce que le lampadaie à une lumière?
            if ( params.light !== undefined && params.light === true ) {
                
                var color = 0xFFFF88;
                
                // Couleur de la lumière
                if ( params.color !== undefined ) 
                    color = params.color;
                
                // Cone de lumière
                var lightcone_geometry  = new THREE.CylinderGeometry( 0.07 * that.getScale(), 3 * that.getScale(), 5.7 * that.getScale(), 32, 32 );
                var lightcone_material  = new THREE.MeshBasicMaterial( { color: color, transparent: true, opacity: 0.05 } )
                var lightcone           = new THREE.Mesh( lightcone_geometry, lightcone_material );
                // lightcone.material.side = THREE.DoubleSide;
                lightcone.name          = 'lightcone';
                
                lightcone.position.set( 1.1 * that.getScale(), 0, 0 );
                lightcone.rotation.set( 0, 0, 0 );
                
                // SpotLight(hex, intensity, distance, angle, exponent, decay)
                var light = new THREE.SpotLight( color, 10, 10, Math.PI/8, 50 ); // Math.PI/12 est exactement la bonne taille d'angle
                light.position.set( 0, 5.7 * that.getScale(), 0  );
                light.target  = lightcone;
                
                // Change la couleur de l'ampoulle ( on doit cloner le material pour ne pas qu'il soit changé par un autre lampadaire )
                
                material.emissive = new THREE.Color( 0x000000 );
                material.color    = new THREE.Color( 0x222222 );                
                
                // TODO: LE OMBRES NE MARCHE PAS, A REVENIR SUR ÇA PLUS TARD
                if ( that.getScene().getShadowsEnabled() && params.castshadow !== undefined && params.castshadow === true ) {
                    
                    light.castShadow         =  true; 
                    light.shadowCameraFar    =  50;   
                    light.shadowMapSoft      =  true;
                    light.shadowCameraNear   =  -50;
                    light.shadowCameraFov    =  50;
                    light.shadowMapBias      =  0.0039;
                    light.shadowMapDarkness  =  0.5;
                    light.shadowMapWidth     =  2048;
                    light.shadowMapHeight    =  2048; 
                    light.shadowCameraRight  =  5;
                    light.shadowCameraLeft   = -5;
                    light.shadowCameraTop    =  5;
                    light.shadowCameraBottom = -5;                      
                    
                } else
                    light.castShadow = false; 
                
                newmodel.getModel().add( lightcone );
                lightcone.add( light );
                
                // TODO: JE NE COMPRENDS PAS POURQUOI JE NE PEUX PAS METTRE A true ICI
                var lightison   = false;
                var forceUpdate = false;
                
                // Configure les données d'animation de l'objet
                newmodel.getAnimCallbackData().lightison = lightison;
                
                // Distance (x ou y, pas distance réel pour fin d'optimisation) maximum pour que les animations soient actives
                if ( params.maxanimdist !== undefined )  
                    newmodel.getAnimCallbackData().maxanimdist = params.maxanimdist;
                else
                    newmodel.getAnimCallbackData().maxanimdist = that.getScene().getConfig( 'camera_culling' ); // + ( 3 * that.getScale() );
                
                newmodel.getAnimCallbackData().forceanim = false;                
                
                // Ajoute le callback d'animation des lumières du lampadaire dans la liste des callbacks d'animations de la scène                
                newmodel.setAnimCallback( function( scene, model ) {
                    
                    var clock = scene.getClock();
                    
                    // le nombre de flashs times doit être impaire pour arriver au bon état final
                    var flashLight = function( times, model, ison ) {   

                        if ( model.getChild( 'lightcone' ) === undefined )
                            return;
                        
                        if ( ison ) { // Éteindre le lampadaire
                        
                            model.getChild( 'lightcone' ).visible             = false;
                            //model.getChild( 'lightcone' ).opacity             = 0;
                            //model.getChild( 'lightcone' ).children[0].visible = false;                        
                            model.getChild( 'light' ).material.emissive       = new THREE.Color( 0x000000 );
                            model.getChild( 'light' ).material.color          = new THREE.Color( 0x555555 );
                        
                        } else if ( !ison ) { // Allumer le lampadaire

                            //model.getChild( 'lightcone' ).opacity             = 0.05;
                            model.getChild( 'lightcone' ).visible             = true;
                            //model.getChild( 'lightcone' ).children[0].visible = true; 
                            model.getChild( 'light' ).material.emissive       = new THREE.Color( 0x000000 );
                            model.getChild( 'light' ).material.color          = new THREE.Color( 0x222222 );
                        
                        }
                        
                        times--;
                        
                        if ( times > 0 ) 
                            setTimeout( flashLight, Math.random() * 100 + 50, times, model, !ison );
                        
                    };
                    
                    if ( clock.isDay() && ( lightison || forceUpdate ) ) { // On est le jour, il faut éteindre le lampadaire
                                               
                        lightison   = false; 
                        if ( forceUpdate ) {
                            
                            flashLight( 1, model, true );
                            forceUpdate = false;  
                            
                        } else                        
                            flashLight( 9, model, true ); // Ferme (le nombre de flashs doit être impaire)
                        
                        // Configure les données d'animation de l'objet
                        model.getAnimCallbackData().lightison = lightison;                        
                        
                    } else if ( clock.isNight() && ( !lightison || forceUpdate ) ) { // On est la nuit, il faut allumer le lampadaire

                        lightison   = true;
                        if ( forceUpdate ) {
                            
                            flashLight( 1, model, true );
                            forceUpdate = false;  
                            
                        } else  
                            flashLight( 9, model, false ); // Allume (le nombre de flashs doit être impaire)
                        
                        // Configure les données d'animation de l'objet
                        model.getAnimCallbackData().lightison = lightison;
                        
                    } else if ( clock.isNight() && lightison && params.defective !== undefined && params.defective !== 0 ) {                   

                        // On est la nuit, si le lampadaire est allumé on le fait flashé aléatoirement car il est défectueux
                        if ( Math.floor( Math.random() * params.defective ) === 1 )                            
                            flashLight( 9, model, false );                        
                        
                    }
                    
                } );
                
                if ( forceUpdate )
                    newmodel.getAnimCallback()( that.getScene(), newmodel );

                that.getScene().addToAnimCallbacks( newmodel );
                
            }
                
            if ( params.showCollider !== undefined && params.showCollider === true )
                 that.getScene().showCollider( true, collider );
            
        }
        
        if ( callback !== undefined )
            callback( newmodel );
        
    } );
    
    return model;
    
};



// Création d'une lumière de trafique
// Paramètres { showCollider: true/false, initialstate: 0-2, syncto: model, synctoinvert: true/false }
Objects3D.prototype.addTrafficLight = function ( position, rotation, params, callback ) {
    
    var that         = this;
    var adj_position = this.adjustPosition( position );
    var adj_rotation = this.adjustRotation90degree( rotation );

    var model = this.loadModel( 'trafficlight', 'trafficlight.json', 1 * that.getScale(), adj_position, adj_rotation, function( newmodel ) {

        // On doit cloner le material pour ne pas qu'il soit changé par un autre lumière de traffique clone
        // TODO: DEVRAIT CLONER TOUS LES MATERIELS DANS Model.clone()
        var material = newmodel.getChild( 'red' ).material.clone();
        newmodel.getChild( 'red' ).material = material;
        material     = newmodel.getChild( 'yellow' ).material.clone();
        newmodel.getChild( 'yellow' ).material = material;        
        material     = newmodel.getChild( 'green' ).material.clone();
        newmodel.getChild( 'green' ).material = material;  
        
        // On utilise se collider pour éviter de masqué le ciel étoilé
        var collider = that.getScene().addCollider( 3, new THREE.Vector3( adj_position.x, 0, adj_position.z ), new THREE.Vector3( 0, 0, 0 ), new THREE.Vector2( 0.015 * that.getScale(), 3 * that.getScale() ), 0, 0 );              
        
        if ( params !== undefined ) {

            if ( params.initialstate !== undefined ) {
                
                // Toutes les lumières sont a off
                newmodel.getChild( 'red' ).material.color       = new THREE.Color( 0x330000 );
                newmodel.getChild( 'yellow' ).material.color    = new THREE.Color( 0x333300 );
                newmodel.getChild( 'green' ).material.color     = new THREE.Color( 0x003300 );
                newmodel.getChild( 'red' ).material.emissive    = new THREE.Color( 0x000000 );
                newmodel.getChild( 'yellow' ).material.emissive = new THREE.Color( 0x000000 );
                newmodel.getChild( 'green' ).material.emissive  = new THREE.Color( 0x000000 );
                
                switch ( params.initialstate ) {
                    
                    case 0  : // red
                              newmodel.getChild( 'red' ).material.emissive = new THREE.Color( 0xFF0000 );
                              break;
                    case 1  : // yellow
                              newmodel.getChild( 'yellow' ).material.emissive = new THREE.Color( 0xFFFF00 );
                              break;
                    case 2  : // green
                    default :
                              newmodel.getChild( 'green' ).material.emissive = new THREE.Color( 0x00FF00 );
                              break;
                    
                }                
            
                // Configuration du callback d'animation
                
                var state        = params.initialstate;
                var timer        = (new Date()).getTime();
                var redtime      = 7;
                var yellowtime   = 2;
                var greentime    = 5;
                var syncto;
                var synctoinvert = false;
                
                // Configure les données d'animation de l'objet
                newmodel.getAnimCallbackData().state = state;
                
                // Distance (x ou y, pas distance réel pour fin d'optimisation) maximum pour que les animations soient actives
                if ( params.maxanimdist !== undefined )  
                    newmodel.getAnimCallbackData().maxanimdist = params.maxanimdist;
                else
                    newmodel.getAnimCallbackData().maxanimdist = that.getScene().getConfig( 'camera_culling' ) + ( 3.3 * that.getScale() ); // Le offset de 3.3 sert à la syncro des lumières au coins de rue
                
                newmodel.getAnimCallbackData().forceanim = false;                
                
                if ( params.redtime !== undefined )
                    redtime = params.redtime;
                
                if ( params.yellowtime !== undefined )
                    redtime = params.yellowtime;

                if ( params.greentime !== undefined )
                    redtime = params.greentime;  

                if ( params.syncto !== undefined )
                    syncto = params.syncto;

                if ( params.synctoinvert !== undefined && params.synctoinvert === true )
                    synctoinvert = true;                 
                
                // Ajoute le callback d'animation de la lumières de trafique dans la liste des callbacks d'animations de la scène                
                newmodel.setAnimCallback( function( scene, model ) {

                    var curtimer = ( (new Date()).getTime() - timer ) / 1000;
                    
                    // Syncronise la lumière avec une autre au besoin
                    if ( syncto !== undefined ) {

                        var redcol   = yellowcol = greencol = new THREE.Color( 0x000000 );
                        var needsync = false;
                        
                        // Syncronisation non inversée
                        if ( synctoinvert === undefined || !synctoinvert ) {
                            
                            if ( syncto.getChild( 'red' ).material.emissive.getHex() === 0xFF0000 && state !== 0 ) {
                                
                                redcol    = new THREE.Color( 0xFF0000 );
                                state     = 0;
                                needsync  = true;
                                
                            }
                            
                            if ( syncto.getChild( 'yellow' ).material.emissive.getHex() === 0xFFFF00 && state !== 1 ) {
                                
                                yellowcol = new THREE.Color( 0xFFFF00 );
                                state     = 1;
                                needsync  = true;
                                
                            } 

                            if ( syncto.getChild( 'green' ).material.emissive.getHex() === 0x00FF00 && state !== 2 ) {
                                
                                greencol  = new THREE.Color( 0x00FF00 );
                                state     = 2;
                                needsync  = true;
                                
                            }                               
                            
                        } else if ( synctoinvert !== undefined && synctoinvert ) { // Syncronisation inversée
                                                    
                            if ( syncto.getChild( 'green' ).material.emissive.getHex() === 0x00FF00 && state !== 0 ) {
                                
                                redcol   = new THREE.Color( 0xFF0000 );
                                state    = 0;
                                needsync = true;
                                
                            }  

                            if ( syncto.getChild( 'yellow' ).material.emissive.getHex() === 0xFFFF00 && state !== 0 ) {
                                
                                redcol   = new THREE.Color( 0xFF0000 );
                                state    = 0;
                                needsync = true;
                                
                            }                             
                            
                        }
                        
                        if ( needsync ) {
                            
                            model.getChild( 'red' ).material.emissive    = redcol;
                            model.getChild( 'yellow' ).material.emissive = yellowcol;
                            model.getChild( 'green' ).material.emissive  = greencol;
                            model.getAnimCallbackData().state            = state;
                            timer = (new Date()).getTime();
                            return;                            
                            
                        }
                        
                    }
                    
                    switch ( state ) {
                        
                        case 0  : // red
                                  if ( curtimer >= redtime ) {
                                      
                                    model.getChild( 'red' ).material.emissive   = new THREE.Color( 0x000000 );
                                    model.getChild( 'green' ).material.emissive = new THREE.Color( 0x00FF00 );
                                    timer = (new Date()).getTime();
                                    state = 2;
                                    
                                  }
                                  
                                  break;
                        case 1  : // yellow
                                  if ( curtimer >= yellowtime ) {
                                      
                                    model.getChild( 'yellow' ).material.emissive   = new THREE.Color( 0x000000 );
                                    model.getChild( 'red' ).material.emissive = new THREE.Color( 0xFF0000 );
                                    timer = (new Date()).getTime();
                                    state = 0;
                                    
                                  }
                                  
                                  break;
                        case 2  : // green
                                  if ( curtimer >= greentime ) {
                                      
                                    model.getChild( 'green' ).material.emissive   = new THREE.Color( 0x000000 );
                                    model.getChild( 'yellow' ).material.emissive = new THREE.Color( 0xFFFF00 );
                                    timer = (new Date()).getTime();
                                    state = 1;
                                    
                                  }
                                  
                                  break;                        
                        
                    }
                    
                    model.getAnimCallbackData().state = state;
                    
                } );                
                
            }

            that.getScene().addToAnimCallbacks( newmodel );                            
                
            if ( params.showCollider !== undefined && params.showCollider === true )
                 that.getScene().showCollider( true, collider );
                 // newmodel.showCollider( true );
            
        }
        
        if ( callback !== undefined )
            callback( newmodel );
        
    } );
    
    return model;
    
};



// Création de 4 lumières de trafique syncronisées aux 4 coins
// Paramètres { showCollider: true/false }
Objects3D.prototype.addTrafficLightsSet = function ( position, rotation, params, callback ) {
    
    var that         = this;
    var lights       = new Array( 4 );
    var adj_position = this.adjustPosition( position );
    var adj_rotation = this.adjustRotation90degree( rotation );
    var offset       = 3.3 * this.getScale();
    var showCollider = undefined;
    var forward      = 0.9 * this.getScale();    
    
    if ( params !== undefined )
        showCollider = params.showCollider;

    lights[0] = this.add( ID.TRAFFIC_LIGHT, { x:adj_position.x - offset, z:adj_position.z - offset - forward }, 2, { showCollider: showCollider, initialstate: 2 }, function( modellight1 ) {
        
        modellight1.getAnimCallbackData().syncto       = undefined;
        modellight1.getAnimCallbackData().synctoinvert = false;
        modellight1.getAnimCallbackData().set          = true;
        modellight1.getAnimCallbackData().mainlight    = modellight1;
        modellight1.getAnimCallbackData().setmembers   = [ modellight1 ];
        
        lights[1] = that.add( ID.TRAFFIC_LIGHT, { x:adj_position.x + offset, z:adj_position.z + offset + forward }, 0, { showCollider: showCollider, initialstate: 2, syncto: modellight1, synctoinvert: false }, function( modellight2 ) {                                    

            modellight2.getAnimCallbackData().syncto       = modellight1;
            modellight2.getAnimCallbackData().synctoinvert = false;
            modellight2.getAnimCallbackData().set          = true;
            modellight2.getAnimCallbackData().mainlight    = modellight1;
            modellight1.getAnimCallbackData().setmembers.push( modellight2 );
            modellight2.getAnimCallbackData().setmembers   = modellight1.getAnimCallbackData().setmembers;
            
            lights[2] = that.add( ID.TRAFFIC_LIGHT, { x:adj_position.x - offset - forward, z:adj_position.z + offset }, 3, { showCollider: showCollider, initialstate: 2, syncto: modellight1, synctoinvert: true }, function( modellight3 ) {
        
                modellight3.getAnimCallbackData().syncto       = modellight1;
                modellight3.getAnimCallbackData().synctoinvert = true;
                modellight3.getAnimCallbackData().set          = true;
                modellight3.getAnimCallbackData().mainlight    = modellight1;
                modellight1.getAnimCallbackData().setmembers.push( modellight3 );
                modellight3.getAnimCallbackData().setmembers   = modellight1.getAnimCallbackData().setmembers;
                
                lights[3] = that.add( ID.TRAFFIC_LIGHT, { x:adj_position.x + offset + forward, z:adj_position.z - offset }, 1, { showCollider: showCollider, initialstate: 2, syncto: modellight3, synctoinvert: false }, function( modellight4 ) {
                
                    modellight4.getAnimCallbackData().syncto       = modellight3;
                    modellight4.getAnimCallbackData().synctoinvert = false;
                    modellight4.getAnimCallbackData().set          = true;
                    modellight4.getAnimCallbackData().mainlight    = modellight1;
                    modellight1.getAnimCallbackData().setmembers.push( modellight4 );
                    modellight4.getAnimCallbackData().setmembers   = modellight1.getAnimCallbackData().setmembers;               
                
                } );
                
            } );
        
        } );
    
    } );

    return lights;
    
};



// Création de 4 stops aux 4 coins
// Paramètres { showCollider: true/false, direction: 0-2 }
Objects3D.prototype.addStopsSet = function ( position, rotation, params, callback ) {
    
    var that         = this;
    var stops        = new Array( 4 );
    var adj_position = this.adjustPosition( position );
    var adj_rotation = this.adjustRotation90degree( rotation );
    var offset       = 3.3 * this.getScale();
    var showCollider = undefined;
    var forward      = 0.9 * this.getScale();
    
    if ( params !== undefined )
        showCollider = params.showCollider;
   
    if ( params !== undefined && params.direction !== undefined && params.direction === 1 ) { // Horizontale
        
        stops[0] = this.add( ID.STOP, { x:adj_position.x - offset, z:adj_position.z - offset - forward }, 2, { showCollider: showCollider } ); 
        stops[1] = this.add( ID.STOP, { x:adj_position.x + offset, z:adj_position.z + offset + forward }, 0, { showCollider: showCollider } );  
        
    } else if ( params !== undefined && params.direction !== undefined && params.direction === 2 ) { // Verticale
        
        stops[0] = this.add( ID.STOP, { x:adj_position.x - offset - forward, z:adj_position.z + offset }, 3, { showCollider: showCollider } );                                       
        stops[1] = this.add( ID.STOP, { x:adj_position.x + offset + forward, z:adj_position.z - offset }, 1, { showCollider: showCollider } );
        
    } else { // Les 2 directions
    
        stops[0] = this.add( ID.STOP, { x:adj_position.x - offset, z:adj_position.z - offset - forward }, 2, { showCollider: showCollider } ); 
        stops[1] = this.add( ID.STOP, { x:adj_position.x + offset, z:adj_position.z + offset + forward }, 0, { showCollider: showCollider } );                                              
        stops[2] = this.add( ID.STOP, { x:adj_position.x - offset - forward, z:adj_position.z + offset }, 3, { showCollider: showCollider } );                                       
        stops[3] = this.add( ID.STOP, { x:adj_position.x + offset + forward, z:adj_position.z - offset }, 1, { showCollider: showCollider } );    
    
    }

    return stops;
    
};



// Création d'un cone
// Paramètres { showCollider: true/false }
Objects3D.prototype.addCone = function ( position, rotation, params, callback ) {

    var that         = this;
    var adj_position = this.adjustPosition( position );
    var adj_rotation = new THREE.Vector3 ( 0, 0, 0 );
    
    if ( rotation !== undefined )
        adj_rotation.y = rotation;
    
    var model = this.loadModel( 'cone', 'cone.json', 1.12 * that.getScale(), adj_position, adj_rotation, function( newmodel ) {    
                
        that.getScene().addModel( newmodel );         
        
        newmodel.addCollider( 4, 100, 1, 0.3, new THREE.Vector3( 0, 0.6, 0.6 ) ); 
        
        var timeout      = 5000; // Temps en ms pour stopper les mouvement du cone
        var startTimeout = ( new Date() ).getTime();
        
        // Distance (x ou y, pas distance réel pour fin d'optimisation) maximum pour que les animations soient actives
        if ( params !== undefined && params.maxanimdist !== undefined )  
             newmodel.getAnimCallbackData().maxanimdist = params.maxanimdist;
        else
             newmodel.getAnimCallbackData().maxanimdist = 10;
                
        newmodel.getAnimCallbackData().forceanim = false;        
        
        // Ajoute le callback d'animation pour éviter le problème des cônes qui bougent sans arrêt               
        newmodel.setAnimCallback( function( scene, model ) {
            
            if ( ( new Date() ).getTime() - startTimeout > timeout ) {
                
                var object = model.getCollider().getModel();
                
                // TODO: TEST POUR REGLER LES PROBLEMES DE MOUVEMENT DES CONES, PEUT-ETRE INUTILE!
                object.__dirtyRotation = true; 
                object.__dirtyPosition = true; 
            
                // object.setAngularFactor(   new THREE.Vector3( 0, 0, 0 ) );
                object.setLinearVelocity(  new THREE.Vector3( 0, 0, 0 ) );
                object.setAngularVelocity( new THREE.Vector3( 0, 0, 0 ) );
                
                startTimeout = ( new Date() ).getTime();
                
            }
            
        } );
        
        that.getScene().addToAnimCallbacks( newmodel );
        
        if ( params !== undefined && params.showCollider !== undefined && params.showCollider === true )
             newmodel.showCollider( true );        
        
        if ( callback !== undefined )
            callback( newmodel );        
        
    } );
    
    return model;   
    
};



// Création d'une poubelle
// Paramètres { showCollider: true/false }
Objects3D.prototype.addTrashCan = function ( position, rotation, params, callback ) {
    var that         = this;
    var adj_position = this.adjustPosition( position );
    var adj_rotation = new THREE.Vector3 ( 0, 0, 0 );
    
    if ( rotation !== undefined )
        adj_rotation = rotation;
    
    var model = this.loadModel( 'trash', 'trash.json', 1 * that.getScale(), adj_position, adj_rotation, function( newmodel ) {
                
        that.getScene().addModel( newmodel );         
        
        newmodel.addCollider( 3, 1000, 1, 0.4 ); // Cylinder collider
        newmodel.setAngularDamping( new THREE.Vector3( 0.004, 0.004, 0.004 ) );
        
        if ( params !== undefined && params.showCollider !== undefined && params.showCollider === true )
             newmodel.showCollider( true );         
        
        if ( callback !== undefined )
            callback( newmodel );        
        
    } );
    
    return model;   
    
};



// Création d'un ballon
// Paramètres { showCollider: true/false }
Objects3D.prototype.addBall = function ( position, rotation, params, callback ) {
    
    var that = this;
    var adj_position;

    if ( position.x !== undefined && position.y !== undefined && position.z !== undefined ) // THREE.Vector3
        adj_position = position
    else // Position à 2 axes
        adj_position = this.adjustPosition( position );
        
    var model = this.loadModel( 'ball', 'ball.json', 1.3 * that.getScale(), adj_position, new THREE.Vector3 ( 0, 0, 0 ), function( newmodel ) {
                
        that.getScene().addModel( newmodel );         
        
        newmodel.addCollider( 2, 1000, 1, 6, new THREE.Vector3( 0.2, 0.2, 0.2 ) ); // Sphere collider
        newmodel.setAngularDamping( new THREE.Vector3( 0.004, 0.004, 0.004 ) ); 
        
        if ( params !== undefined && params.showCollider !== undefined && params.showCollider === true )
             newmodel.showCollider( true );         
        
        if ( callback !== undefined )
            callback( newmodel );        
        
    } );
    
    return model;
    
};



// Création d'un arbre
// Paramètres { showCollider: true/false, type: number, scale: number, random: true/false, color_var: number }
Objects3D.prototype.addTree = function ( position, rotation, params, callback ) {
    
    var that         = this;
    var scale        = 1.12;
    var adj_rotation = new THREE.Vector3( 0, 0, 0 );    
    var adj_position = this.adjustPosition( position );
    var model;
    var models = [ [ 'tree_a', 'tree_a.json' ], [ 'tree_b', 'tree_b.json' ] ];
    
    // Randomness
    var color_var     = 0;
    var min_color_var = 0;
    var max_color_var = 0.2;
    var min_scale     = 0.3;
    var max_scale     = 1.12;
    var min_rotation  = 0;
    var max_rotation  = 0.04;    
            
    if ( params !== undefined ) {

        if ( params.type !== undefined )
            model = type;
        else
            model = Math.floor( Math.random() * models.length );
        
        if ( params.random !== undefined && params.random === false ) {
            
            // Ne pas redimentionner ou incliner les arbres
            max_scale     = 0;
            max_rotation  = 0;
            max_color_var = 0;
            
        }
        
        if ( params.scale !== undefined )
            scale = params.scale;
        
        if ( params.color_var !== undefined )
            color_var = params.color_var;
    
    } else 
        model = Math.floor( Math.random() * models.length );
    
    if ( max_scale !== 0 )
        scale = ( Math.random() * max_scale + min_scale ) * this.getScale();
    
    if ( max_rotation !== 0 )
        adj_rotation = new THREE.Vector3( -1 * Math.floor( Math.random() * 2 ) * Math.random() * max_rotation + min_rotation, 0, -1 * Math.floor( Math.random() * 2 ) * Math.random() * max_rotation + min_rotation );    
  
    if ( max_color_var !== 0 )
        color_var = ( -1 * Math.floor( Math.random() * 2 ) * Math.random() * max_color_var + min_color_var );
    
    var tree = this.loadModel( models[model][0], models[model][1], scale * that.getScale(), adj_position, adj_rotation, function( newmodel ) {
                
        that.getScene().addModel( newmodel ); 

        // Change la teinte des feuilles de l'arbre
        if ( max_color_var !== 0 ) {
            
            var m = newmodel.getChild( 'leaves' ).material;
            var h = m.color.getHSL().h;
            var s = m.color.getHSL().s;
            var l = m.color.getHSL().l;
            
            m = new THREE.MeshPhongMaterial( { color : m.color.setHSL( h, s, Math.min( 1, Math.max( 0, l + color_var ) ) ), transparent : false } );
        
        }
        
        that.getScene().addCollider( 3, newmodel.getPosition(), new THREE.Vector3( 0, 0, 0 ), new THREE.Vector2( 0.01 * scale, 3 * scale ), 0, 0 );  // Valeur normale 0.05 de largeur
        
        if ( params !== undefined && params.showCollider !== undefined && params.showCollider === true )
             newmodel.showCollider( true );         
        
        if ( callback !== undefined )
            callback( newmodel );        
        
    } );
    
    return tree;
    
};



// Création d'une forêt
// Paramètres { showCollider: true/false, min: number, max: number, num: number, units: number }
Objects3D.prototype.addForest = function ( position, rotation, params, callback ) {
  
  var min = 5;
  var max = 10;
  var arbres;
  
  if ( params !== undefined && params.min !== undefined )
      min = params.min;

  if ( params !== undefined && params.max !== undefined )
      max = params.max;   
  
  if ( params !== undefined && params.num !== undefined )
      arbres = new Array( params.num );     
  else
      arbres = new Array( Math.floor( Math.random() * max ) + min );

  for ( var i = 0; i < arbres.length; ++i ) { 
  
      var newX  = ( Math.random() < .5 ? -1 : 1 ) * Math.random() * params.units / 2 + position.x * params.units;
      var newY  = ( Math.random() < .5 ? -1 : 1 ) * Math.random() * params.units / 2 + position.y * params.units;
      arbres[i] = this.getScene().add( ID.TREE, { x: newX, y: newY } );
    
  }
  
  return arbres;
  
};



// Création d'une boite
// Paramètres { showCollider: true/false }
Objects3D.prototype.addBox = function ( position, rotation, params, callback ) {
    
    var that         = this;
    var adj_position = this.adjustPosition( position );
    var adj_rotation = this.adjustRotation( rotation );
        
    var model = this.loadModel( 'cardboard', 'cardboard.json', 0.2 * that.getScale(), adj_position, adj_rotation, function( newmodel ) {
                
        that.getScene().addModel( newmodel );         
        
        newmodel.addCollider( 0, undefined, 0.9, 0.2 );
        
        if ( params !== undefined && params.showCollider !== undefined && params.showCollider === true )
             newmodel.showCollider( true );         
        
        if ( callback !== undefined )
            callback( newmodel );        
        
    } );
    
    return model;
    
};



// Création d'un stop
// Paramètres { showCollider: true/false }
Objects3D.prototype.addStop = function ( position, rotation, params, callback ) {
    
    var that         = this;
    var adj_position = this.adjustPosition( position );
    var adj_rotation = this.adjustRotation( rotation );
    
    var model = this.loadModel( 'stop', 'stop.json', 1 * that.getScale(), adj_position, adj_rotation, function( newmodel ) {
                
        that.getScene().addModel( newmodel );         
                
        // newmodel.addCollider( 0, 0, 0, 0 );
        
        // On utilise se collider pour éviter de masqué le ciel étoilé
        var collider = that.getScene().addCollider( 3, new THREE.Vector3( adj_position.x, 0, adj_position.z ), new THREE.Vector3( 0, 0, 0 ), new THREE.Vector2( 0.01 * that.getScale(), 2 * that.getScale() ), 0, 0 );
                        
        if ( params !== undefined && params.showCollider !== undefined && params.showCollider === true )
             that.getScene().showCollider( true, collider );
             // newmodel.showCollider( true );         
        
        if ( callback !== undefined )
            callback( newmodel );        
        
    } );
    
    return model;
    
};

// Création d'une table
// Paramètres { showCollider: true/false, color: color }
Objects3D.prototype.addTable = function ( position, rotation, params, callback ) {
    
    var that         = this;
    var adj_position = this.adjustPosition( position );
    var adj_rotation = this.adjustRotation( rotation );
    
    var model = this.loadModel( 'table', 'table.json', 1.7 * that.getScale(), adj_position, adj_rotation, function( newmodel ) {
                
        that.getScene().addModel( newmodel );         
        
        newmodel.addCollider( 0, 0, 0, 0 );
        
        if ( params !== undefined && params.color !== undefined )
            newmodel.tint( params.color, 'table' );
        
        if ( params !== undefined && params.showCollider !== undefined && params.showCollider === true )
             newmodel.showCollider( true );         
        
        if ( callback !== undefined )
            callback( newmodel );        
        
    } );
    
    return model;
    
};



// Création d'un poteau de téléphone
// Paramètres { showCollider: true/false, color: color }
Objects3D.prototype.addTelephonePole = function ( position, rotation, params, callback ) {
    
    var that         = this;
    var adj_position = this.adjustPosition( position );
    var adj_rotation = this.adjustRotation( rotation );
    
    var model = this.loadModel( 'telephonepole', 'telephonepole.json', 1 * that.getScale(), adj_position, adj_rotation, function( newmodel ) {
                
        that.getScene().addModel( newmodel );         
        
        that.getScene().addCollider( 3, new THREE.Vector3( adj_position.x, 0, adj_position.z ), new THREE.Vector3( 0, 0, 0 ), new THREE.Vector2( 0.05 * that.getScale(), 6.7 * that.getScale() ), 0, 0 );
        
        if ( params !== undefined && params.color !== undefined )
            newmodel.tint( params.color, 'poteau' );
        
        if ( params !== undefined && params.showCollider !== undefined && params.showCollider === true )
             newmodel.showCollider( true );         
        
        if ( callback !== undefined )
            callback( newmodel );        
        
    } );
    
    return model;
    
};



// Création de fils électriques
// Paramètres {}
Objects3D.prototype.addElectricWires = function ( position, rotation, params, callback ) {
    
    var that         = this;
    var adj_position = this.adjustPosition( position );
    var adj_rotation = this.adjustRotation( rotation );
    
    var model = this.loadModel( 'electricityline', 'electricityline.json', 1 * that.getScale(), adj_position, adj_rotation, function( newmodel ) {
                
        that.getScene().addModel( newmodel );                 
        
        if ( callback !== undefined )
            callback( newmodel );        
        
    } );
    
    return model;
    
};



// Création d'une ligne électrique
// Paramètres { showCollider: true/false, size: number, distance: number, variation: number, wires: true/false, lights: true/false,
//              defectiveness: 0.0-1.0, defectiveness_min: number, defectiveness_max: number, all_lights: true/false, wire_scale: number }
Objects3D.prototype.addElectricLine = function ( position, rotation, params, callback ) {
    
    var that           = this;
    var adj_position   = this.adjustPosition( position );
    var adj_rotation   = rotation === undefined ? 0 : rotation;
    var showcollider   = false;
    var elements       = {
        
        poles: undefined,
        wires: undefined
        
    };
    
    var poles_distance           = 4 * that.getScale();
    var poles_dist_var           = 0;
    var poles_lights             = true;
    var poles_all_lights         = false;    
    var poles_wires              = true;
    var line_size                = 3 * that.getScale();
    var light_color              = 0xFFFF88;
    var lights_defectiveness     = 0.2;
    var lights_defectiveness_min = 200;
    var lights_defectiveness_max = 300;
    var street_x_offset          = 3.3  * that.getScale();
    var street_z_offset          = 3.3  * that.getScale();
    var wires_offset             = 0.05 * that.getScale();
    var poles_darkness           = 0.2;
    
    // TODO: DEFRAIT FAIRE UN CALCUL ICI POUR LES VALEURS NON PAR DEFAUT!
    var wire_scale               = 1.34 * that.getScale(); // Valeur si poles_distance = 4, line_size = 3 et edge_offset
    
    if ( params !== undefined ) {
        
        if ( params.showCollider !== undefined )
            showcollider = params.showCollider;  
        
        if ( params.distance !== undefined )
            poles_distance = params.distance; 
        
        if ( params.variation !== undefined )
            poles_dist_var = params.variation; 
        
        if ( params.wires !== undefined )
            poles_wires = params.wires;  

        if ( params.size !== undefined )
            line_size = params.size;

        if ( params.lights !== undefined )
            poles_lights = params.lights; 

        if ( params.light_color !== undefined )
            light_color = params.light_color; 

        if ( params.defectiveness !== undefined )
            lights_defectiveness = params.defectiveness;  

        if ( params.defectiveness_min !== undefined )
            lights_defectiveness_min = params.defectiveness_min;  
        
        if ( params.defectiveness_max !== undefined )
            lights_defectiveness_max = params.defectiveness_max;         

        if ( params.all_lights !== undefined )
            poles_all_lights = params.all_lights;   

        if ( params.wire_scale !== undefined )
            wire_scale = params.wire_scale;         
        
    }
    
    elements.poles = new Array( line_size );
    elements.wires = new Array( line_size - 1 );
    
    var xstart, zstart, x_offset, z_offset, s_x_offset, s_z_offset, wires_x_offset, wires_z_offset, wires_rotation, direction;
    x_offset      = z_offset      = s_x_offset = s_z_offset = wires_x_offset = wires_z_offset = wires_rotation = direction = 0;
    
    var light = false;
    
    switch ( adj_rotation ) {

        case ID.NORTH : xstart         = adj_position.x;
                        zstart         = adj_position.z - ( line_size - 1 ) * poles_distance / 2;
                        z_offset       = poles_distance;
                        s_x_offset     = -street_x_offset;
                        direction      = 0;
                        wires_z_offset = poles_distance / 2;                        
                        wires_x_offset = wires_offset;
                        wires_rotation = Math.PI / 2;
                        break; 
                        
        case ID.WEST  : xstart         = adj_position.x + ( line_size - 1 ) * poles_distance / 2;
                        zstart         = adj_position.z;
                        x_offset       = -poles_distance;
                        s_z_offset     = street_z_offset;                        
                        direction      = 1;
                        wires_z_offset = -wires_offset;
                        wires_x_offset = -( poles_distance / 2 );
                        wires_rotation = 0;                        
                        break; 
                        
        case ID.SOUTH : xstart         = adj_position.x;
                        zstart         = adj_position.z + ( line_size - 1 ) * poles_distance / 2;
                        z_offset       = -poles_distance;
                        s_x_offset     = street_x_offset;
                        direction      = 2;
                        wires_z_offset = -( poles_distance / 2 );                        
                        wires_x_offset = -wires_offset;
                        wires_rotation = -( Math.PI / 2 );                        
                        break;
                        
        case ID.EAST  : 
        default       :        
                        xstart         = adj_position.x - ( line_size - 1 ) * poles_distance / 2;
                        zstart         = adj_position.z;
                        x_offset       = poles_distance;
                        s_z_offset     = -street_z_offset;                        
                        direction      = 3;
                        wires_z_offset = wires_offset;                        
                        wires_x_offset = poles_distance / 2;
                        wires_rotation = 2 * Math.PI;                        
                        break;
                        
    }          
    
    // Poteaux ou lumières de rue
    for ( var i = 0; i < line_size; ++i ) {
        
        if ( light || poles_all_lights ) {
            
            var params_lights = { showCollider: showcollider, light: true, castshadow: false, color: light_color };
            
            // Lumière défectueuse?
            if ( Math.random() < lights_defectiveness ) 
                params_lights.defective = Math.random() * lights_defectiveness_max + lights_defectiveness_min;            
             
            elements.poles[i] = this.addStreetLight(   new THREE.Vector3( xstart + s_x_offset + i * x_offset, adj_position.y, zstart + s_z_offset + i * z_offset ), direction, params_lights, function( pole ) {

                var hsl = pole.getChild( 'poteau' ).material.color.getHSL();
                hsl.l   = hsl.l + ( ( -1 * Math.floor( Math.random() * 2 ) ) * ( Math.random() * poles_darkness ) );
                pole.getChild( 'poteau' ).material.color.setHSL( hsl.h, hsl.s, Math.min( 1, Math.max( 0, hsl.l ) ) );
                
            } );            
            
        } else 
            elements.poles[i] = this.addTelephonePole( new THREE.Vector3( xstart + s_x_offset + i * x_offset, adj_position.y, zstart + s_z_offset + i * z_offset ), direction, { showCollider: showcollider }, function( pole ) {
                
                var hsl = pole.getChild( 'poteau' ).material.color.getHSL();
                hsl.l   = hsl.l + ( ( -1 * Math.floor( Math.random() * 2 ) ) * ( Math.random() * poles_darkness ) );
                pole.getChild( 'poteau' ).material.color.setHSL( hsl.h, hsl.s, Math.min( 1, Math.max( 0, hsl.l ) ) );
                
            } );   
        
        if ( poles_lights )
            light = !light;
        
    }

        
    // Fils électriques    
    if ( poles_wires ) {

        for ( var i = 0; i < ( line_size - 1 ); ++i ) {
        
            // Le dernier va appeler le callback;
            elements.wires[i] = this.addElectricWires( new THREE.Vector3( xstart + s_x_offset + wires_x_offset + i * x_offset, adj_position.y, zstart + s_z_offset + wires_z_offset + i * z_offset ), new THREE.Vector3( 0, wires_rotation, 0 ), { showCollider: showcollider }, function( wire ) {

                wire.getModel().scale.set( wire_scale, 1, 1 ); 
                
                if ( i === ( line_size - 2 ) && callback !== undefined ) 
                    callback( elements );
                
            } );        
        
        }    
        
    }
    
    return elements;
    
};



// Création d'une bascule
// Paramètres { showCollider: true/false, color1: color, color2: color, color3: color, color4: color }
Objects3D.prototype.addBascule = function ( position, rotation, params, callback ) {
    
    var that         = this;
    var adj_position = this.adjustPosition( position );
    var adj_rotation = this.adjustRotation( rotation );

    var model = this.loadModel( 'bascules', 'bascules.json', 1 * that.getScale(), adj_position, adj_rotation, function( newmodel ) {
                
        that.getScene().addModel( newmodel );         
        
        newmodel.addCollider( 0, 0, 0, 0 );
        
        if ( params !== undefined ) {
            
            if ( params.color1 !== undefined )
                newmodel.tint( params.color1, 'bascule1' );
            
            if ( params.color2 !== undefined )
                newmodel.tint( params.color2, 'bascule2' );
            
            if ( params.color3 !== undefined )
                newmodel.tint( params.color3, 'bascule3' );
            
            if ( params.color4 !== undefined ) {
                
                newmodel.tint( params.color4, 'poutre1' );
                newmodel.tint( params.color4, 'poutre2' );
            }
            
            if ( params.showCollider !== undefined && params.showCollider === true )
                newmodel.showCollider( true );         
        }
        
        if ( callback !== undefined )
            callback( newmodel );        
        
    } );
    
    return model;
    
};



// Création d'une barrière
// Paramètres { showCollider: true/false }
Objects3D.prototype.addBarrier = function ( position, rotation, params, callback ) {
    
    var that         = this;
    var adj_position = this.adjustPosition( position );
    var adj_rotation = this.adjustRotation( rotation );

    var model = this.loadModel( 'construction', 'construction.json', 1 * that.getScale(), adj_position, adj_rotation, function( newmodel ) {
                
        that.getScene().addModel( newmodel );         
        
        newmodel.setRotation( 0, 0, 0 );
        newmodel.addCollider( 0, 2000, 1, 0.1 );        
        newmodel.setRotation( adj_rotation.x, adj_rotation.y, adj_rotation.z );

        if ( params !== undefined && params.showCollider !== undefined && params.showCollider === true )
             newmodel.showCollider( true );         
        
        if ( callback !== undefined )
            callback( newmodel );        
        
    } );
    
    return model;
    
};



// Création d'une clôture
// Paramètres { showCollider: true/false, height: 0.0-1.0, door: true/false, scale: scale, type: type, color: color }
Objects3D.prototype.addFences = function ( position, rotation, params, callback ) {
    
    if ( params === undefined )
        params = {};    
    
    var that         = this;
    var adj_position = this.adjustPosition( position );
    var adj_rotation = this.adjustRotation( rotation ); 
    var scale        = params.scale !== undefined ? params.scale : 1; 
    var type         = params.type  !== undefined ? params.type  : 0; 
    var name;
    var namedoor;
    var file;     
    var filedoor;
    
    
    switch ( type ) {
    
        case 0 : name     = 'fence';
                 namedoor = 'fence_door_b';
                 file     = 'fence.json';     
                 filedoor = 'fence_door_b.json';
                 break;
                 
        case 1 : name     = 'fence';
                 namedoor = 'fence_door';        
                 file     = 'fence.json';     
                 filedoor = 'fence_door.json';
                 break;  

        case 2 : name     = 'fence_c';
                 namedoor = 'fence_c_door';        
                 file     = 'fence_c.json';     
                 filedoor = 'fence_c_door.json';
                 break;                  
                 
    }              
                
    if ( params !== undefined && params.door !== undefined && params.door === true ) {
       
        name = namedoor;        
        file = filedoor;
        
    }

    var model = this.loadModel( name, file, 1.12 * that.getScale() * scale, adj_position, adj_rotation, function( newmodel ) {
        
        // Hauteur de la clôture 0.0 - 1.0
        if ( params !== undefined && params.height !== undefined ) {
        
            var fence_height  = new THREE.Box3().setFromObject( newmodel.getModel() ).max.y;
            var wanted_height = fence_height * Math.min( 1, Math.max( 0, params.height ) );
            var pos           = newmodel.getPosition();
            
            newmodel.setPosition( pos.x, -( fence_height - wanted_height ), pos.z );
        
        }        
                
        that.getScene().addModel( newmodel );  

        // Teinte de la clôture
        if ( params !== undefined && params.color !== undefined ) {
            
            newmodel.tint( params.color, 'left' );
            newmodel.tint( params.color, 'right' );
            newmodel.tint( params.color, 'top' );
            newmodel.tint( params.color, 'center' );
            newmodel.tint( params.color, 'center.001' );
            newmodel.tint( params.color, 'top.001' );
            
        }
        
        if ( params !== undefined && params.door !== undefined && params.door === false ) {
        
            // TODO: POUR FIXER LE BUG DE ROTATION DU MODEL FIX TEMPORAIRE
            newmodel.setRotation( 0, 0, 0 );        
            newmodel.addCollider( 0, 0, 0, 0 );
            newmodel.setRotation( adj_rotation.x, adj_rotation.y, adj_rotation.z );
            
        } else { // Collider de la porte
        
            var fencebox    = new THREE.Box3().setFromObject( newmodel.getModel() ).max;            
            var colrotation = new THREE.Vector3( 0, adj_rotation.y - Math.PI/2, 0 );
            var xoffset, zoffset, colliderz, colliderx;
           
            switch ( Math.round( adj_rotation.y ) ) {
               
               // 0
               case 0 : 
              
               // 2
               case Math.round( Math.PI ) :                  
               
                         zoffset     = 0;
                         xoffset     = 1.04 * that.getScale() * scale;                
                         colliderz   = 0.01;
                         colrotation = new THREE.Vector3( 0, adj_rotation.y, 0 );
                         colliderx   = 3.36 * that.getScale() * scale * 0.39;
                         break;
                         
              // 3              
              case Math.round( Math.PI / 2 ) :  

                         zoffset     = 1.04 * that.getScale() * scale;
                         xoffset     = 0;                 
                         colliderz   = 3.36 * that.getScale() * scale * 0.39; 
                         colliderx   = 0.01;
                         colrotation = new THREE.Vector3( 0, adj_rotation.y - Math.PI/2, 0 );
                         break;

               // 1
               case Math.round( 3 * Math.PI / 2 ) :  
                         zoffset     = 1.04 * that.getScale() * scale;
                         xoffset     = 0;                 
                         colliderz   = 3.36 * that.getScale() * scale * 0.39; 
                         colliderx   = 0.01;
                         colrotation = new THREE.Vector3( 0, adj_rotation.y - Math.PI/2, 0 );
                         break;               
                         
            }

            var collider1 = that.getScene().addCollider( 0, new THREE.Vector3( adj_position.x + xoffset, adj_position.y + fencebox.y / 2, adj_position.z + zoffset ), colrotation, { x: colliderx, y: fencebox.y, z: colliderz }, 0, 0 );
            var collider2 = that.getScene().addCollider( 0, new THREE.Vector3( adj_position.x - xoffset, adj_position.y + fencebox.y / 2, adj_position.z - zoffset ), colrotation, { x: colliderx, y: fencebox.y, z: colliderz }, 0, 0 );
                       
            if ( params !== undefined && params.showCollider !== undefined && params.showCollider === true ) {
                
                that.getScene().showCollider( true, collider1 );          
                that.getScene().showCollider( true, collider2 ); 
                
            }            
            
        }
        
        if ( params !== undefined && params.showCollider !== undefined && params.showCollider === true )
             newmodel.showCollider( true );         
        
        if ( callback !== undefined )
            callback( newmodel );        
        
    } );
    
    return model;
    
};



// Création d'un mur clôture
// Paramètres { showCollider: true/false, height: 0.0-1.0, size: number, door: true/false, door_offset: number, scale: scale, type: type, params.color }
Objects3D.prototype.addFencesWall = function ( position, rotation, params, callback ) {

    // On doit au moins fournir length
    if ( params === undefined || params.size === undefined )
        params = { size: 3 };
    
    if ( rotation === undefined )
        rotation = 0;
    
    // Seulement les rotations en angles de 90 degrés sont supportées
    if ( rotation !== ID.NORTH && rotation !== ID.SOUTH && rotation !== ID.WEST && rotation !== ID.EAST )
        return undefined;
    
    var that         = this;
    var adj_position = this.adjustPosition( position );
    var adj_rotation = this.adjustRotation90degree( rotation );        
    var elements     = new Array ( params.size );
    var height       = params.height      !== undefined ? params.height      : 1;
    var scale        = params.scale       !== undefined ? params.scale       : 1; 
    var door_offset  = params.door_offset !== undefined ? params.door_offset : 0;
    var offset       = 3.36 * that.getScale() * scale;  
    var x_offset     = 0;
    var z_offset     = 0;    
    var x_start      = 0;
    var z_start      = 0;
    var showcollider = params.showCollider !== undefined ? params.showCollider : false;
    
    switch ( rotation ) {
        
        case ID.NORTH : 
        case ID.SOUTH :        
                        x_start  = -( params.size * offset / 2 ) + ( offset / 2 );
                        x_offset = offset;
                        break;                        
        case ID.WEST  :
        case ID.EAST  : z_start  = -( params.size * offset / 2 ) + ( offset / 2 );
                        z_offset = offset;
                        break;                        
                        
    }
    
    for ( var i = 0; i < params.size; ++i ) {
        
        var pos  = new THREE.Vector3( adj_position.x + x_start + x_offset * i, adj_position.y, adj_position.z + z_start + z_offset * i );
        var door = false;
        
        if ( params.door !== undefined && params.door && door_offset === i )
            door = true;                
                
        elements[i] = this.addFences( pos, adj_rotation, { showCollider: showcollider, height: height, door: door, scale: params.scale, type: params.type, color: params.color }, function( fence ) {
            
            // Après l'ajout du dernier élément on appel le callback
            if ( i === params.size - 1 ) {
                
                if ( callback !== undefined )
                    callback( elements ); 
                
            }
                            
        } );
        
    }
    
    return elements;
    
};



// Création d'un boite clôture
// Paramètres { showCollider: true/false, height: 0.0-1.0, size_x: number, size_z: number, door: true/false, door_position: 0-3, door_offset: number, scale: scale, type: type, params.color }
Objects3D.prototype.addFencesBox = function ( position, rotation, params, callback ) {

    var that         = this;
    var adj_position = this.adjustPosition( position ); 
    var sizez        = params.size_z        !== undefined ? params.size_z        : 1;  
    var sizex        = params.size_x        !== undefined ? params.size_x        : 1;      
    var scale        = params.scale         !== undefined ? params.scale         : 1;
    var door_pos     = params.door_position !== undefined ? params.door_position : 0;  
    var dodoor       = params.door          !== undefined ? params.door          : true;  
    var sides        = new Array( 4 );
    var offset       = 3.36 * that.getScale() * scale;    
    var x_offset;
    var z_offset;
    var size;
    
    // Ajutement de la position de la porte pour utiliser le même standard que Terrain.js
    // TODO: Standardiser les directions
    switch ( door_pos ) {

        case 0 : door_pos = 0; break;
        case 1 : door_pos = 3; break;
        case 2 : door_pos = 2; break;
        case 3 : door_pos = 1; break;        
        
    }

    if ( params === undefined )
        params = {};
    
    for ( var i = 0; i < 4; ++i ) {
        
        switch ( i ) {
        
            case 0 : z_offset = -( sizez * offset / 2 ); x_offset = 0; size = sizex; break;
            case 1 : x_offset = -( sizex * offset / 2 ); z_offset = 0; size = sizez; break;            
            case 2 : z_offset =  ( sizez * offset / 2 ); x_offset = 0; size = sizex; break;                                                  
            case 3 : x_offset =  ( sizex * offset / 2 ); z_offset = 0; size = sizez; break;                      
                        
        }
        
        //var pos  = new THREE.Vector3( adj_position.x, adj_position.y, adj_position.z );
        var pos  = new THREE.Vector3( adj_position.x + x_offset, adj_position.y, adj_position.z + z_offset );
        var door = false;
        
        if ( dodoor && params.door !== undefined && params.door && door_pos === i )
            door = true;
        
        sides[i] = this.addFencesWall( pos, i, { showCollider: params.showCollider, size: size, height: params.height, door: door, door_offset: params.door_offset, scale: scale, type: params.type, color: params.color }, function( wall ) {
            
            // Après l'ajout du dernier mur on appel le callback
            if ( i === 3 ) {
                
                if ( callback !== undefined )
                    callback( sides ); 
                
            }
                            
        } );      
        
    }
    
    return sides;
    
}    



// Création d'un parc
// Paramètres { showCollider: true/false, size_x: number, size_z: number, scale: scale, fences: true/false, fences_type: number, fences_height: 0.0-1.0, 
//              fences_door: true/false, fences_door_position: 0-3, fences_door_offset: number, trees_num: number, balls_num: number,
//              tables_num: number, bascules_num: number, fences_color : color }
Objects3D.prototype.addParc = function ( position, rotation, params, callback ) {
 
    var that                 = this;
    var adj_position         = this.adjustPosition( position ); 
    
    if ( params === undefined )
        params = {};
    
    // Valeurs par défaut
    var size_x               = params.size_x               !== undefined ? params.size_x               : 2; 
    var size_z               = params.size_z               !== undefined ? params.size_z               : 2;  
    var scale                = params.scale                !== undefined ? params.scale                : 1;     
    var fences               = params.fences               !== undefined ? params.fences               : true; 
    var fences_type          = params.fences_type          !== undefined ? params.fences_type          : 0; 
    var fences_height        = params.fences_height        !== undefined ? params.fences_height        : 1; 
    var fences_door          = params.fences_door          !== undefined ? params.fences_door          : true; 
    var fences_door_position = params.fences_door_position !== undefined ? params.fences_door_position : 0; 
    var fences_door_offset   = params.fences_door_offset   !== undefined ? params.fences_door_offset   : 0; 
    var fences_height        = params.fences_height        !== undefined ? params.fences_height        : 1;  
    var parkrotation         = rotation                    !== undefined ? rotation                    : 0;
    var balls_num            = params.balls_num            !== undefined ? params.balls_num            : 1;    
    var trees_num            = params.trees_num            !== undefined ? params.trees_num            : 2;    
    var tables_num           = tables_num                  !== undefined ? tables_num                  : 2;
    var bascules_num         = bascules_num                !== undefined ? bascules_num                : 2;    
    var limits               = 2 * that.getScale() * scale; 
    var placedobjectsnum     = 4; // trees, balls, tables, bascules 
    var xsize                = size_x * 3.36 * that.getScale() * scale / 2;  
    var zsize                = size_z * 3.36 * that.getScale() * scale / 2;       
    var objects              = new Array();
    
    if ( fences ) {
        
        objects.push( this.addFencesBox( position, rotation, { showCollider: params.showCollider, height: fences_height, size_x: size_x, size_z: size_z, scale: scale, door: fences_door, door_position: fences_door_position, door_offset: fences_door_offset, type: fences_type, color: params.fences_color }, function( sides ) {

        } ) );
        
    }
    
    if ( tables_num !== 0 || trees_num !== 0 || balls_num !== 0 && bascules_num !== 0 ) {
        
        for ( x = adj_position.x - xsize + limits; x < adj_position.x + xsize - limits; x = x + limits ) {
           
             for ( z = adj_position.z - zsize + limits ; z < adj_position.z + zsize - limits; z = z + limits ) {
                 
                 var placed = false;
                 
                 do {
                     
                        var random_object = Math.floor( Math.random() * ( placedobjectsnum + 1 ) );

                        switch ( random_object ) {
                     
                            case 0 : // Table
                                     if ( tables_num !== 0 ) {

                                         objects.push( this.addTable( { x: x, z: z }, new THREE.Vector3( 0, Math.random() * 2 * Math.PI, 0 ), {
                                             
                                             color: new THREE.Color( Math.max( col_max, Math.random() * col_max + col_min ), Math.max( col_max, Math.random() * col_max + col_min ), Math.max( col_max, Math.random() * col_max + col_min ) ).getHex()
                                             
                                         } ) ); 
                                         tables_num--;
                                         placed = true;
                                        
                                     }                              
                                     break;
                              
                            case 1 : // Tree
                                     if ( trees_num !== 0 && this.getScene().getConfig( 'cityDecoration' ) ) {
                                         
                                         objects.push( this.addTree( { x: x, z: z }, new THREE.Vector3( 0, Math.random() * 2 * Math.PI, 0 ), {} ) ); 
                                         trees_num--;
                                         placed = true;
                                        
                                     }                                
                                     break;

                            case 2 : // Ball
                                     if ( balls_num !== 0 ) {
                                         
                                         objects.push( this.addBall( { x: x, z: z }, new THREE.Vector3( 0, Math.random() * 2 * Math.PI, 0 ), {} ) ); 
                                         balls_num--;
                                         placed = true;
                                        
                                     }                                
                                     break;  
                                     
                            case 3 : // Bascules
                                     if ( bascules_num !== 0 ) {
                                         
                                         var col_min = 44;
                                         var col_max = 255;
                                         objects.push( this.addBascule( { x: x, z: z }, new THREE.Vector3( 0, Math.random() * 2 * Math.PI, 0 ), {
                                                                                          
                                             color1: new THREE.Color( Math.max( col_max, Math.random() * col_max + col_min ), Math.max( col_max, Math.random() * col_max + col_min ), Math.max( col_max, Math.random() * col_max + col_min ) ).getHex(),
                                             color2: new THREE.Color( Math.max( col_max, Math.random() * col_max + col_min ), Math.max( col_max, Math.random() * col_max + col_min ), Math.max( col_max, Math.random() * col_max + col_min ) ).getHex(),
                                             color3: new THREE.Color( Math.max( col_max, Math.random() * col_max + col_min ), Math.max( col_max, Math.random() * col_max + col_min ), Math.max( col_max, Math.random() * col_max + col_min ) ).getHex(), 
                                             color4: new THREE.Color( Math.max( col_max, Math.random() * col_max + col_min ), Math.max( col_max, Math.random() * col_max + col_min ), Math.max( col_max, Math.random() * col_max + col_min ) ).getHex(),
                                             
                                         } ) ); 
                                         bascules_num--;
                                         placed = true;
                                        
                                     }                                
                                     break;                                       
                              
                        }
                        
                 } while ( !placed && ( tables_num !== 0 && trees_num !== 0 && balls_num !== 0 && bascules_num !== 0 ) );
                 
             }
            
        }                                    
       
    }
    
    return objects;
    
}




// Création d'un parc de construction
// Paramètres { showCollider: true/false, size_x: number, size_z: number, scale: scale, fences: true/false, fences_type: number, fences_height: 0.0-1.0, 
//              fences_door: true/false, fences_door_position: 0-3, fences_door_offset: number, barriers_num: number, cones_num: number,
//              boxes_num: number, fences_color : color }
Objects3D.prototype.addConstructionParc = function ( position, rotation, params, callback ) {
 
    var that                 = this;
    var adj_position         = this.adjustPosition( position ); 
    
    if ( params === undefined )
        params = {};
    
    // Valeurs par défaut
    var size_x               = params.size_x               !== undefined ? params.size_x               : 2; 
    var size_z               = params.size_z               !== undefined ? params.size_z               : 2;  
    var scale                = params.scale                !== undefined ? params.scale                : 1;     
    var fences               = params.fences               !== undefined ? params.fences               : true; 
    var fences_type          = params.fences_type          !== undefined ? params.fences_type          : 0; 
    var fences_height        = params.fences_height        !== undefined ? params.fences_height        : 1; 
    var fences_door          = params.fences_door          !== undefined ? params.fences_door          : true; 
    var fences_door_position = params.fences_door_position !== undefined ? params.fences_door_position : 0; 
    var fences_door_offset   = params.fences_door_offset   !== undefined ? params.fences_door_offset   : 0; 
    var fences_height        = params.fences_height        !== undefined ? params.fences_height        : 1;  
    var parkrotation         = rotation                    !== undefined ? rotation                    : 0;
    var cones_num            = params.cones_num            !== undefined ? params.cones_num            : 30;    
    var barriers_num         = params.barriers_num         !== undefined ? params.barriers_num         : 30; 
    var boxes_num            = params.boxes_num            !== undefined ? params.boxes_num            : 30;  
    var building             = building                    !== undefined ? building                    : true;      
    var limits               = 2 * that.getScale() * scale; 
    var placedobjectsnum     = 3; // cones, barriers, boxes 
    var xsize                = size_x * 3.36 * that.getScale() * scale / 2;  
    var zsize                = size_z * 3.36 * that.getScale() * scale / 2;       
    var objects              = new Array();
    var entrance_position;
    
    if ( fences ) {
        
        objects.push( this.addFencesBox( position, rotation, { showCollider: params.showCollider, height: fences_height, size_x: size_x, size_z: size_z, scale: scale, door: fences_door, door_position: fences_door_position, door_offset: fences_door_offset, type: fences_type, color: params.fences_color }, function( sides ) {

        } ) );
        
    }
        
    if ( building ) {
        
        objects.push( this.addBuilding( adj_position, rotation, { size: new THREE.Vector3( size_x, Math.floor( Math.random() * 8 ) + 2, size_z ), scale : scale * 0.9 * this.getScale(), type: ID.TYPE_HOUSE_CONSTRUCT }, function( building ) {

            
        } ) );

    }    
    
    var entrance_distance = 0.5 * that.getScale() * scale; 
    
    switch ( fences_door_position ) {
        
        case 0 : entrance_position = new THREE.Vector3( adj_position.x + ( ( -1 * Math.floor( Math.random() * 2 ) ) * Math.random() * xsize ), 0, adj_position.z - entrance_distance - zsize ); break;
        case 1 : entrance_position = new THREE.Vector3( adj_position.x + entrance_distance + xsize, 0, adj_position.z + ( ( -1 * Math.floor( Math.random() * 2 ) ) * Math.random() * zsize ) ); break;
        case 2 : entrance_position = new THREE.Vector3( adj_position.x + ( ( -1 * Math.floor( Math.random() * 2 ) ) * Math.random() * xsize ), 0, adj_position.z + entrance_distance + zsize ); break;
        case 3 : entrance_position = new THREE.Vector3( adj_position.x - entrance_distance - xsize, 0, adj_position.z + ( ( -1 * Math.floor( Math.random() * 2 ) ) * Math.random() * zsize ) ); break;
        
    }
    
    if ( cones_num !== 0 || barriers_num !== 0 || boxes_num !== 0  ) {
        
        for ( x = adj_position.x - xsize + limits; x < adj_position.x + xsize - limits; x = x + limits ) {
           
             for ( z = adj_position.z - zsize + limits ; z < adj_position.z + zsize - limits; z = z + limits ) {
                 
                 var placed = false;
                 
                 do {                     
                        var random_object = Math.floor( Math.random() * ( placedobjectsnum + 1 ) );

                        switch ( random_object ) {
                     
                            case 0 : // Cone
                                     if ( cones_num !== 0 ) {

                                         if ( Math.random() < 0.5 )                                      
                                            objects.push( this.addCone( { x: x, z: z }, Math.random() * 2 * Math.PI, {

                                            } ) );
                                         else ( Math.random() < 0.5 )                                      
                                            objects.push( this.addCone( { x: entrance_position.x, z: entrance_position.z }, Math.random() * 2 * Math.PI, {

                                            } ) );
                                            
                                         cones_num--;
                                         placed = true;
                                        
                                     }                              
                                     break;
                              
                            case 1 : // Barrier
                                     if ( barriers_num !== 0 ) {
                                         
                                         if ( Math.random() < 0.5 ) 
                                            objects.push( this.addBarrier( { x: x, z: z }, new THREE.Vector3( 0, Math.random() * 2 * Math.PI, 0 ), {} ) ); 
                                         else
                                            objects.push( this.addBarrier( { x: entrance_position.x, z: entrance_position.z }, new THREE.Vector3( 0, Math.random() * 2 * Math.PI, 0 ), {} ) ); 
                                        
                                         barriers_num--;
                                         placed = true;
                                        
                                     }                                
                                     break;
                                     
                            case 2 : // Boxes
                                     if ( boxes_num !== 0 ) {
                                         
                                         if ( Math.random() < 0.5 ) 
                                            objects.push( this.addBox( { x: x, z: z }, new THREE.Vector3( 0, Math.random() * 2 * Math.PI, 0 ), {} ) ); 
                                         else
                                            objects.push( this.addBox( { x: entrance_position.x, z: entrance_position.z }, new THREE.Vector3( 0, Math.random() * 2 * Math.PI, 0 ), {} ) ); 
                                         boxes_num--;
                                         placed = true;
                                        
                                     }                                
                                     break;                                     
                              
                        }
                        
                 } while ( !placed && ( cones_num !== 0 && barriers_num !== 0 && boxes_num !== 0 ) );
                 
             }
            
        }                                    
       
    }
    
    return objects;
    
}




















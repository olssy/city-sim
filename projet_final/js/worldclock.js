/* INF5071 - Infographie - Travail Pratique 2 - UQAM A2015
 *
 *   Etudiant   : Stephane Olssen
 *   Code perm  : OLSS14037300
 *   Courriel   : olssen.stephane@courrier.uqam.ca
 *   
 *   Etudiant   : Thomas Robert de Massy
 *   Code perm  : ROBT24057409
 *   Courriel   : robert_de_massy.thomas@courrier.uqam.ca     
 *
 *   Professeur : Alexandre Blondin Massé
 *
 *
 *   durationInSec - La duree d'un journee de 2h dans la simulation en secondes reeles
 *   dayStart - Le leve du jour
 *   nightStart - Le debut de la nuit
 */
var WorldClock = function(durationInSec, dayStart, nightStart) {

    this.clock = new THREE.Clock(); // Horloge THREE

    // duree d'une journee 24h
    this.fullDayDuration = (typeof durationInSec === 'undefined') ? 120 : durationInSec;
    // debut de la journee - leve du soleil    
    this.dayStart = (typeof dayStart === 'undefined') ? (this.fullDayDuration / 4) / (this.fullDayDuration / 24) : dayStart / 24 * this.fullDayDuration;
    // debut de la nuit    
    this.nightStart = (typeof nightStart === 'undefined') ? (this.fullDayDuration / 4 * 3) / (this.fullDayDuration / 24) : nightStart / 24 * this.fullDayDuration;
    // Calcule des duree et des differentes periode de la journee
    this.dayDuration = this.nightStart - this.dayStart;
    this.nightDuration = this.dayStart + (this.dayDuration - this.dayStart);
    this.dawnStart = (this.dayDuration - this.dayStart) / 2;
    this.morningStart = this.dayStart;
    this.highNoonStart = ((1 / 2 * this.dayDuration) + this.dayStart);
    this.afterNoonStart = ((1 / 4 * this.dayDuration) + this.highNoonStart);
    this.eveningStart = (((5 / 12) * this.dayDuration) + this.highNoonStart);
    this.nightTimeStart = this.nightStart;

    this.dawnDuration = this.morningStart - this.dawnStart;
    this.morningDuration = this.highNoonStart - this.morningStart;
    this.highNoonDuration = this.afterNoonStart - this.highNoonStart;
    this.afterNoonDuration = this.eveningStart - this.afterNoonStart;
    this.eveningDuration = this.nightTimeStart - this.eveningStart;
    this.nightTimeDuration = (this.fullDayDuration - this.nightTimeStart) + (this.dawnStart)


    this.elapsedTime = this.clock.getElapsedTime(); // temps passee depuis le lancement de l'application

    // L'heure sur un horloge 24h de la simulation
    this.time = ((this.elapsedTime + (this.fullDayDuration / 2)) % this.fullDayDuration) / this.fullDayDuration * 24;

    this.deltaStart; // debut d'un delta de temps

    /*
    / Fonction pour debutter un delta
    */
    this.startDelta = function() {
            this.deltaStart = this.elapsedTime;
        }
        /*
        / Fonction qui retourne le temps sepuis l'appel a startDelta
        */
    this.getDelta = function() {
        return this.elapsedTime - this.deltaStart;
    }

    /*
     * Fonction pour mettre a jour l'heure
     */
    this.update = function() {
        this.elapsedTime = this.clock.getElapsedTime();
        this.time = ((this.elapsedTime + (this.fullDayDuration / 2)) % this.fullDayDuration) / this.fullDayDuration * 24;
    };
    /*
     * Fonction qui retournent la duree d'une journee
     */
    this.getFullDayDuration = function() {
        return this.fullDayDuration;
    };
    /*
     * Fonction pour definir la duree d'une journee
     */
    this.setFullDayDuration = function(dd) {
        this.dayDuration = dd;
    };

    /*
     * Fonction pour reinitialiser l'heure
     */
    this.restart = function() {
        this.clock.stop();
        this.clock.start();
    };

    /*
     * Fonction qui retourne depuis quand l'application a ete lance
     */
    this.getElapsedTime = function() {

        return this.clock.getElapsedTime();

    };

    /*
     * Fonction qui retourrne l'heure dans la simulation
     */
    this.getTime = function() {
        return this.time;
    };

    // l'horloge, cacher au depart
    this.debugText = document.createElement('div');
    this.debugText.setAttribute("id", "clock");
    this.debugText.style.opacity = .6;
    this.debugText.style.position = 'absolute';
    this.debugText.style.width = 80 + "px";
    this.debugText.style.height = 25 + "px";
    this.debugText.style.fontSize = 20 + "px";
    this.debugText.style.backgroundColor = "transparent";
    this.debugText.style.color = "white";
    this.debugText.style.visibility = "hidden";
    this.debugText.innerHTML = this.time.toFixed(2);
    this.debugText.style.top = 70 + 'px';
    this.debugText.style.right = 20 + 'px';
    document.body.appendChild(this.debugText);

    /*
     * Fonction pour afficher l'heure
     */
    this.showClock = function(isVisible) {
        if (isVisible) {
            this.debugText.style.visibility = "visible";
        } else {
            this.debugText.style.visibility = "hidden";
        }
    };

    /*
     * Ensemble de fonction pour identifier differentes periode de la journee et
     * de recuperer la pourcentage passee d'une periode.
     */
    this.getFullDayPercentage = function() {
        this.time / this.getDayDuration()
    };
    this.isDay = function() {
        return (this.time >= this.dayStart && this.time <= this.nightStart);
    };
    this.isNight = function() {
        ret = this.time < this.dayStart || this.time > this.nightStart;
        return ret;
    };
    this.isDawn = function() {
        if (this.time >= this.dawnStart && this.time < this.dayStart) {
            return true;
        } else {
            return false;
        }
    };
    this.isMorning = function() {
            if (this.time >= this.dayStart && this.time < this.highNoonStart) {
                return true;
            } else {
                return false;
            }
        },
        this.isHighNoon = function() {
            if (this.time >= this.highNoonStart && this.time < this.afterNoonStart) {
                return true;
            } else {
                return false;
            }
        };
    this.isAfterNoon = function() {
        if (this.time >= this.afterNoonStart && this.time < this.eveningStart) {
            return true;
        } else {
            return false;
        }
    };
    this.isEvening = function() {
        if (this.time >= this.eveningStart && this.time < this.nightStart) {
            return true;
        } else {
            return false;
        }
    };
    this.isNightTime = function() {
        if ((this.time >= this.nightStart) ||
            (this.time < this.dawnStart)) {
            return true;
        } else {
            return false;
        }
    };
    this.getPercentageOfDawn = function() {
        if (this.isDawn()) {
            return (this.time - this.dawnStart) / this.dawnDuration;
        } else {
            return 0;
        }
    };
    this.getPercentageOfMorning = function() {
        if (this.isMorning()) {
            return (this.time - this.morningStart) / this.morningDuration;
        } else {
            return 1;
        }
    };
    this.getPercentageOfHighNoon = function() {
        if (this.isHighNoon()) {
            return (this.time - this.highNoonStart) / this.highNoonDuration;
        } else {
            return 0;
        }
    };
    this.getPercentageOfAfterNoon = function() {
        if (this.isAfterNoon()) {
            return (this.time - this.afterNoonStart) / this.afterNoonDuration;
        } else {
            return 0;
        }
    };
    this.getPercentageOfEvening = function() {
        if (this.isEvening()) {
            return (this.time - this.eveningStart) / this.eveningDuration;
        } else {
            return 0;
        }
    };
    this.getPercentageOfNightTime = function() {
        if (this.isNightTime()) {
            if (this.time > this.nightTimeStart) {
                return (this.time - this.nightTimeStart) / this.nightTimeDuration;
            } else { // apres minuit
                return (this.time + (this.fullDayDuration - this.nightTimeStart)) / this.nightTimeDuration;
            }
        } else {
            return 0;
        }
    };
    this.getPercentageOfDay = function() {
        if (this.isNight()) {
            return 1;
        };
        return (this.time - this.dayStart) / this.dayDuration;
    };
    this.getPercentageOfNight = function() {

        if (this.isDay()) {
            return 1;
        };

        var nightTime;

        // minuit a 7am; deuxieme moitie de nuit
        if (this.time < this.dayStart) {
            nightTime = (this.time + this.dayStart);
        } else {
            nightTime = this.time - this.dayDuration - this.dayStart;
        }
        return (nightTime / this.nightDuration);
    };
    this.getPercentageOfAM = function() {

        // Si avant la moitie de la journee alors
        if (this.time < 12) {

            return (this.time / this.dayDuration);
        } else { // Sinon on est dans le PM

            return 0;
        }
    };
    this.getPercentageOfPM = function() {

        // Si apres la moitie de la journee
        if (this.time >= 12) {

            return ((this.time - 12) / this.dayDuration);
        } else { // Sinon on est dans le AM

            return 0;
        }

    };
    this.isAM = function() {

        if (this.time < 12) {

            return true;
        } else {

            return false;
        }
    };
    this.isPM = function() {

        if (this.time >= 12) {

            return true;
        } else {

            return false;
        }
    };
    this.minTommss = function() {        
        this.update();
        var time = this.time;
        var sign = time < 0 ? "-" : "";
        var min  = Math.floor(Math.abs(time))
        var sec  = Math.floor((Math.abs(time) * 60) % 60);
        
        return sign + (min < 10 ? "0" : "") + min + ":" + (sec < 10 ? "0" : "") + sec;
    };    
    
};

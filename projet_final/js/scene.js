/* INF5071 - Infographie - Travail Pratique 2 - UQAM A2015
 *
 *   Etudiant   : Stephane Olssen
 *   Code perm  : OLSS14037300
 *   Courriel   : olssen.stephane@courrier.uqam.ca
 *   
 *   Etudiant   : Thomas Robert de Massy
 *   Code perm  : ROBT24057409
 *   Courriel   : robert_de_massy.thomas@courrier.uqam.ca     
 *
 *   Professeur : Alexandre Blondin Mass?
 
 
 *         render()
 *         updateStats()
 *         setSize( width, height )
 *         create()
 *         addModel( Model )
 *   bool  isInScene( Model )
 *   Model findModelByName( name )
 *         removeModel( Model )
 *         removeModelByName( name )
 *         clear()
 *         reset()
 *         modelsListCleanup()
 *         removeBuilding( Building )
 *         removeBuildingByName( name )
 *         removeAllBuildings()
 *         buildingsListCleanup()
 *         setCameraPos( x, y, z )
 *         setCameraDir( x, y, z )
 *         createTerrain( length, width, proportionBuildings, tailleBlocs, buildings )
 *         createSky()
 *         calculateAngularDampings()
 *   Model addCollider( type, position, rotation, size, friction, restitution )
 *         showCollider( mode, collider )
 *         preloadModels( callback )
 *          
 *   Getters/Setters
 *
 *   ATTENTION! être prudent en utilisant les setters, car vous pouvez briser la structure de la scène
 *             
 *                 setCamera( camera )       
 *   object        getCamera()        
 *                 setCameraCtrler( object )      
 *   object        getCameraCtrler()      
 *                 setRenderer( object )    
 *   object        getRenderer()
 *                 setScene( object )
 *   object        getScene()          
 *                 setTerrain( Terrain )  
 *   Terrain       getTerrain()           
 *                 setWidth( number )     
 *   number        getWidth()        
 *                 setHeight( number )      
 *   number        getHeight()   
 *                 setBackground( color )     
 *   color         getBackground()  
 *                 setMainLight( object )
 *   object        getMainLight()
 *                 setAmbientLight( object )     
 *   object        getAmbientLight() 
 *                 setRenderCallback( callback )     
 *   function      getRenderCallback() 
 *                 addToModels( Model )      
 *                 setModels( Model[] )   
 *   Model[]       getModels()  
 *                 addBuilding( Building ) 
 *                 setBuildings( Building[] )    
 *   Building[]    getBuildings()      
 *                 bool getShadowsEnabled() 
 *                 setShadowsEnabled( bool ) 
 *   number        getCameraHeight() 
 *                 setCameraHeight( number )  
 *                 setStats( bool )  
 *   bool          getStats()        
 *                 setShowStats( bool )   
 *   bool          getShowStats()
 *                 addCollider( type, position, rotation, size, friction, restitution )
 *                 showCollider( mode, collider ) 
 *                 addToAnimCallbacks( Model )        
 *                 setAnimCallbacks( Model[] )     
 *   Model[]       getAnimCallbacks()
 *   color         rgbToInt( r, g, b )
 *          
 *
 */

 
 
var Scene = function( mycanvas, color ) {

    // Membres publiques
    
    // Membres privés
    
    var configs = {
  
        antialias          : true,
        camera_culling     : 30,
        camera_far         : 200,
        cast_shadows       : true,
        dynamic_lights     : true,
        animate_car        : false,
        details            : 3,
        v_size             : 28,
        h_size             : 28,
        v_num              : 16,
        h_num              : 16,
        buildings_percent  : 34,
        houses_percent     : 33,
        public_percent     : 33,
        day_duration       : 24,
        lightsToStopsProp  : .5,
        fallenTrashProb    : .1,
        cityDecoration     : true,
        hasDowntown        : false,
        ambientLightMax    : .6,
        ambientLightMin    : .4,
        mainLightMax       : 1,
        mainLightMin       : 0
    };    
      
    var that           = this;
    var scene;
    var camera;        
    var canvas;
    var terrain;
    var sky;
    var renderer;
    var canvasWidth;
    var canvasHeight;
    var background;
    var renderCallback;
    var shadowsenabled;
    var objects;
    var models          = new Array();
    var buildings       = new Array(); 
    var animcallbacks   = new Array();     
    var cameraCtlrer;
    var clock           = new WorldClock(configs.day_duration);
    var cameraHeight;
    var stats;
    var showsStats      = false;
    var render          = true;
    
    /** "Constructeur"
     * 
     */		
    ( function () {               

        objects        = new Objects3D( that );
        canvas         = document.getElementById( mycanvas );
        background     = color !== undefined ? color : 0x000000;
        shadowsenabled = false;  // Aucune ombre par défaut
        cameraHeight   = 0.3;    // Hauteur de la caméra
        
    } )();
    
    // Getters/Setters
    this.getConfig          = function ( c )    { return configs[c]; }; 
    this.setConfig          = function ( c, v ) { configs[c] = v; }; 
    this.getConfigs         = function ()       { return configs; };     
    this.getCamera          = function ()       { return camera; }; 
    
    
    this.setCamera          = function ( cam )  { camera = cam; };        
    this.getCamera          = function ()       { return camera; };        
    this.setCameraCtrler    = function ( cc )   { cameraCtrler = cc; };        
    this.getCameraCtrler    = function ()       { return cameraCtrler; };        
    this.setRenderer        = function ( rend ) { renderer = rend; };    
    this.getRenderer        = function ()       { return renderer; };
    this.setScene           = function ( scn )  { scene = scn; };  
    this.getScene           = function ()       { return scene; };            
    this.setTerrain         = function ( ter )  { terrain = ter; };  
    this.getTerrain         = function ()       { return terrain; };            
    this.setSky             = function ( s )    { sky = s; };  
    this.getSky             = function ()       { return sky; };            
    this.setWidth           = function ( w )    { canvasWidth = w; };      
    this.getWidth           = function ()       { return canvasWidth; };        
    this.setHeight          = function ( h )    { canvasHeight = h; };      
    this.getHeight          = function ()       { return canvasHeight; };   
    this.setBackground      = function ( c )    { background = c; };     
    this.getBackground      = function ()       { return background; };  
    this.setRender          = function ( r )    { render = r; };    
    this.getRender          = function ()       { return render; };    
    this.setRenderCallback  = function ( rc )   { renderCallback = rc; };     
    this.getRenderCallback  = function ()       { return renderCallback; }; 
    this.addToModels        = function ( m )    { models.push( m ); };        
    this.setModels          = function ( m )    { models = m; };     
    this.getModels          = function ()       { return models; }; 
    this.getObjects         = function ()       { return objects; }; 
    this.clearObjects       = function ()       { objects = new Objects3D( that ); };     
    this.addBuilding        = function ( b )    { buildings.push( b ); }; 
    this.setBuildings       = function ( b )    { buildings = b; };     
    this.getBuildings       = function ()       { return buildings };     
    this.getShadowsEnabled  = function ()       { return shadowsenabled; };
    this.setShadowsEnabled  = function ( se )   { shadowsenabled = se; };
    this.getCameraHeight    = function ()       { return cameraHeight; };
    this.setCameraHeight    = function ( ch )   { cameraHeight = ch; };
    this.setStats           = function ( s )    { stats = s; };
    this.getStats           = function ()       { return stats; };    
    this.setShowStats       = function ( ss )   { showsStats = ss; };
    this.getShowStats       = function ()       { return showsStats; };
    this.getClock           = function ()       { return clock; };
    this.setClock           = function (c)      { clock = c; };
    this.newClock           = function (t)      { clock = new WorldClock(t) };
    this.addToAnimCallbacks = function ( m )    { animcallbacks.push( m ); };        
    this.setAnimCallbacks   = function ( a )    { animcallbacks = a; };     
    this.getAnimCallbacks   = function ()       { return animcallbacks; };

    
    
    this.render = function () {
        
        if ( !that.getRender() )
            return;
        
        // Calcul du culling de la caméra
        that.calculateCameraCulling();
                
        that.getClock().update();
        
        document.getElementById('clock').innerHTML = that.getClock().minTommss() + 'h';//that.getClock().getTime().toFixed(2);	        

        var terrain = that.getTerrain();
        var scene   = that.getScene();
        var sky     = that.getSky();
        var clock   = that.getClock();

        sky.render();
        scene.simulate();
        
        // Executes les callbacks d'animations des Models dans la liste des callbacks d'animations
        that.processAnimCallbacks();
    
        if ( that.getRenderCallback() !== undefined )
            that.getRenderCallback()();
        
        that.getRenderer().render( that.getScene(), that.getCamera() );        
        
        // Affiche les statistiques
        that.updateStats();  

        // Calcul des dampings de rotation sur les modèles de la scène
        that.calculateAngularDampings();
         
        requestAnimationFrame( that.render );
    
    };    
        
    return this;
    
};



Scene.prototype.calculateCameraCulling = function () {
    
    var frustum_check = false;
    var camera        = this.getCamera();
    var frustum       = new THREE.Frustum();
    var models        = this.getModels();
    var objects;
    
    if ( frustum_check ) {
        
        camera.updateMatrix();      // make sure camera's local matrix is updated
        camera.updateMatrixWorld(); // make sure camera's world matrix is updated
        camera.matrixWorldInverse.getInverse( camera.matrixWorld );  
    
        frustum.setFromMatrix( new THREE.Matrix4().multiplyMatrices( camera.projectionMatrix, camera.matrixWorldInverse ) );
        
    }

    for ( var i = 0; i < models.length; ++i ) {
        
        // TODO: BUG DES LUMIÈRES DE RUE, FIX TEMPORAIRE, VOIR AUSSI LA FONCTION processAnimCallbacks()
        if ( models[i].getName().indexOf("streetlight") === 0 )
            continue;
        
        // Il n'y a pas de culling sur le gazon
        if ( models[i].getName() === 'floor' )
            continue;
        
        objects     = models[i].getChildren();
        var visible = true;
        models[i].getModel().visible = true;      
        
        // Pour l'instant le collider est visible
        if ( models[i].getCollider() !== undefined )
            models[i].getCollider().getModel().visible = true;    
        
        if ( ( Math.max( Math.abs( models[i].getPosition().x - this.getCameraCtrler().position.x ), 
                         Math.abs( models[i].getPosition().z - this.getCameraCtrler().position.z ) ) > this.getConfig( 'camera_culling' ) ) ) {

            visible = false;
            models[i].getModel().visible = false;
            
            // Le collider aussi est invisible
            if ( models[i].getCollider() !== undefined && models[i].getCollider().getModel() !== undefined )
                models[i].getCollider().getModel().visible = false;
            
        }
                
        if ( objects === undefined || ( visible && models[i].isVisible() ) || ( models[i].getCollider() !== undefined && models[i].getCollider().isVisible() ) )
            continue;
        
        models[i].setVisible( visible );
        
        for ( var j = 0; j < objects.length; ++j ) {
        
            // Seulement les objets dans la vue de la caméra sont visible
            if ( frustum_check && visible === true ) {                
                        
                objects[j].updateMatrix();      // make sure plane's local matrix is updated
                objects[j].updateMatrixWorld(); // make sure plane's world matrix is updated 

                objects[j].visible = frustum.intersectsObject( objects[j] ); 
                visible            = objects[j].visible;
                models[i].setVisible( objects[j].visible );                
            
            }
            
            if ( visible === true ) {
                
                 // Les murs sont invisible à cause de l'optimisation des buildings
                 if ( objects[j].name !== 'mur' ) {
                    
                    models[i].getModel().visible = true;                    
                    objects[j].visible           = true;
                    models[i].setVisible( true );                    
                    
                    // Collider aussi visible
                    if ( models[i].getCollider() !== undefined && models[i].getCollider().getModel() !== undefined )
                        models[i].getCollider().getModel().visible = true;                    
                    
                 } else
                    objects[j].visible = false; 
                
            } else 
                 objects[j].visible = false;
        
        }
        
    }

};



// TODO: NE MARCHE PAS SUPER BIEN, MARCHE EN PARTIE POUR LE BALLON, MAIS PAS POUR LES CONES
Scene.prototype.calculateAngularDampings = function () {
    
    var models = this.getModels();
    
    // TODO: DEVRAIT JUSTE REGARDER LES COLLIDERS ICI EN UTILISANT getColliders()    
    for ( var i = 0; i < models.length; i++ ) {
        
        if ( models[i] !== undefined && models[i].getCollider() !== undefined && models[i].getCollider().getModel() !== undefined 
          && models[i].getAngularDamping().x !== 0 && models[i].getAngularDamping().y !== 0 && models[i].getAngularDamping().z !== 0 ) {
        
            var collider = models[i].getCollider();
            var velocity = collider.getModel().getLinearVelocity(); 
            var angVel   = collider.getModel().getAngularVelocity();            
            var angFact  = models[i].getAngularFactor();
            var angDamp  = models[i].getAngularDamping();
            
            if ( velocity === undefined || angFact === undefined || angDamp === undefined )
                continue;            
           
            if ( ( angFact.x !== 0 || angFact.y !== 0 || angFact.z !== 0 ) && ( angVel.x !== 0 || angVel.y !== 0 || angVel.z !== 0 ) ) {
            
                models[i].setAngularFactor( new THREE.Vector3( Math.max( 0, angFact.x - angDamp.x ), Math.max( 0, angFact.y - angDamp.y ), Math.max( 0, angFact.z - angDamp.z ) ) );            
            
                collider.getModel().setAngularFactor( models[i].getAngularFactor() );
                
                angFact = models[i].getAngularFactor();
                
                if ( ( angFact.x === 0 || angFact.y === 0 || angFact.z === 0 ) ) {
                
                    models[i].setAngularFactor( new THREE.Vector3( 1, 1, 1 ) );
                    collider.getModel().setAngularFactor( new THREE.Vector3( 0, 0, 0 ) );
                    collider.getModel().setLinearVelocity( new THREE.Vector3( 0, 0, 0 ) );
                    collider.getModel().setAngularVelocity( new THREE.Vector3( 0, 0, 0 ) );
                    
                }
                
                collider.getModel().updateMatrixWorld( true );
                
             } else {
                
                models[i].setAngularFactor( new THREE.Vector3( 1, 1, 1 ) );
                
             }
            
        }
        
    }

};



Scene.prototype.updateStats = function () {
    
    // Affiche les statistiques
    if ( this.getShowStats() ) {
        
        // Est-ce que la library Stats est pr?sente?
        if ( Stats !== undefined ) {
            
            if ( this.getStats() === undefined ) {
        
                var stats = new Stats();
                stats.domElement.style.position = 'absolute';
                stats.domElement.style.top      = '20px';
                stats.domElement.style.right    = '20px';
                stats.domElement.style.zIndex   = 100;
                stats.domElement.style.opacity  = 0.3;
                document.body.appendChild( stats.domElement );
        
                this.setStats( stats );                    
                this.getStats().update();
    
            } else {
                
                this.getStats( stats ).domElement.style.visibility = 'visible';
                this.getStats().update();
                
            }
                
        }
               
    } else if ( this.getStats() !== undefined ) {
        
        // Rend les stats invisibles
        this.getStats( stats ).domElement.style.visibility = 'hidden';
        
    }

};



Scene.prototype.setSize = function ( width, height ) {
    
    this.setWidth( width );
    this.setHeight( height );
        
};



Scene.prototype.create = function () {

    this.getClock().showClock( true );

    // Initialisation du canvas
    this.setRenderer( new THREE.WebGLRenderer( {antialias: this.getConfig( 'antialias' ), alpha: false } ) );

    this.getRenderer().setClearColor( this.getBackground(), 1 );        
    
    this.getRenderer().setSize( this.getWidth(), this.getHeight() );
        
    document.body.appendChild( this.getRenderer().domElement );

    // Initialisation de la scène et de la camera
    this.setScene( new Physijs.Scene( { fixedTimeStep: 1 / 120 } ) );
    this.setCamera( new THREE.PerspectiveCamera( 60, this.getWidth() / this.getHeight(), 0.01, this.getConfig( 'camera_far' ) ) ); 
    this.getScene().setGravity( new THREE.Vector3( 0, -30, 0 ) );

    var materialRed     = Physijs.createMaterial( new THREE.MeshPhongMaterial( { color: 0xff0000, shininess: 100.0 } ), 0, 0);
    materialRed.visible = false; // Le disque de collision doit être invisible
    
    this.setCameraCtrler( new Physijs.CylinderMesh( new THREE.CylinderGeometry( 0.3, 0.3, 0.85 ), materialRed, 1000 ) );
    
    this.getScene().add( this.getCameraCtrler() );
    
    this.getCamera().position.set( 0, this.getCameraHeight(), 0 );
    
    this.getCameraCtrler().add( this.getCamera() );
    
    // Pour éviter que le cube de la physique de la caméra bouge
    this.getCameraCtrler().setAngularFactor( new THREE.Vector3( 0, 0, 0 ) );
    
    if ( this.getShadowsEnabled() ) {
               
        this.getRenderer().shadowMap.enabled  = true;   
        this.getRenderer().shadowMap.type     = THREE.PCFSoftShadowMap; 

    }
                    
    return this.getScene();
    
};



/*
 * Ajoute un objet à la scene et retourn son Model
 *
 */
Scene.prototype.add = function ( id, position, rotation, params, callback ) {

    return this.getObjects().add( id, position, rotation, params, callback );
    
};



Scene.prototype.addModel = function ( model, scale ) {
    
    var scene = this.getScene();
                    
    // Ombres
    model.getModel().castShadow    = true;
    model.getModel().receiveShadow = true;

    if ( model.getCollider() !== undefined )
        scene.add( model.getCollider().getModel() );
    else
        scene.add( model.getModel() );
           
    model.setScene( this );    
    
    if ( scale === undefined || scale !== false )
        model.scale( model.getScale() );
    
    this.addToModels( model );           
    
};



Scene.prototype.isInScene = function ( themodel ) {
    
    var models = this.getModels();
    
    for ( var i = 0; i < models.length; ++i ) {
        
        if ( models[i] === themodel )
            return true;
        
    }

    return false; // Not found    
    
};



Scene.prototype.findModelByName = function ( name ) {
    
    var models = this.getModels();
    
    for ( var i = 0; i < models.length; ++i ) {
        
        if ( models[i] !== undefined && models[i].getName() === name ) {
        
            return models[i];         
            
        }
        
    }  

    return undefined; // Not found
    
};



Scene.prototype.preloadModels = function ( callback ) {
    
    var obj3D = this.getObjects();
    
    // Buildings
    obj3D.preloadBuilding( ID.TYPE_SKYSCRAPER_A,                   function() { 
    obj3D.preloadBuilding( ID.TYPE_SKYSCRAPER_B,                   function() { 
    obj3D.preloadBuilding( ID.TYPE_HOUSE_A1,                       function() { 
    obj3D.preloadBuilding( ID.TYPE_HOUSE_A2,                       function() {   
    obj3D.preloadBuilding( ID.TYPE_HOUSE_B1,                       function() {  
    obj3D.preloadBuilding( ID.TYPE_HOUSE_C1,                       function() {   
    obj3D.preloadBuilding( ID.TYPE_HOUSE_A_GARAGE,                 function() {     
    obj3D.preloadBuilding( ID.TYPE_HOUSE_CONSTRUCT,                function() { 
    
    // Objects
    obj3D.preloadModel( 'fence',           'fence.json',           function() {
    obj3D.preloadModel( 'fence_c',         'fence_c.json',         function() {
    obj3D.preloadModel( 'fence_c_door',    'fence_c_door.json',    function() {
    obj3D.preloadModel( 'fence_door',      'fence_door.json',      function() {        
    obj3D.preloadModel( 'fence_door_b',    'fence_door_b.json',    function() {       
    obj3D.preloadModel( 'stop',            'stop.json',            function() {
    obj3D.preloadModel( 'ball',            'ball.json',            function() {
    obj3D.preloadModel( 'trash',           'trash.json',           function() {
    obj3D.preloadModel( 'trafficlight',    'trafficlight.json',    function() {
    obj3D.preloadModel( 'streetlight',     'streetlight.json',     function() {
    obj3D.preloadModel( 'cone',            'cone.json',            function() {
    obj3D.preloadModel( 'construction',    'construction.json',    function() {
    obj3D.preloadModel( 'telephonepole',   'telephonepole.json',   function() {
    obj3D.preloadModel( 'electricityline', 'electricityline.json', function() {
    obj3D.preloadModel( 'table',           'table.json',           function() {
    obj3D.preloadModel( 'bascules',        'bascules.json',        function() {
    obj3D.preloadModel( 'cardboard',       'cardboard.json',       function() {
    obj3D.preloadModel( 'tree_a',          'tree_a.json',          function() {    
    obj3D.preloadModel( 'tree_b',          'tree_b.json',          function() {
        
        // Appel le callback une fois terminé
        if ( callback !== undefined )
            callback();
        
    } ); } ); } ); } ); } ); } ); } ); } ); } ); } ); } ); } ); } ); } ); } ); } ); } ); } ); } ); } ); } ); } ); } ); } ); } ); } ); } );
     
};



Scene.prototype.removeModel = function ( themodel ) {
    
    var scene  = this.getScene();
    var models = this.getModels();
    
    this.setRender( false );
    
    for ( var i = 0; i < models.length; ++i ) {
        
        if ( models[i] === themodel ) {
        
            // On doit aussi enlever le collider si il existe            
            if ( models[i].getCollider() !== undefined ) {
                
                scene.remove( models[i].getCollider() );
                models[i].setCollider( undefined );
                
            }
            
            scene.remove( models[i].getModel() );
            models[i].setModel( undefined );
            models[i] = undefined;
                    
            break;            
            
        }
        
    }

    this.modelsListCleanup();    
    
    // TODO: Enlever le callback d'animation ici
    
    this.setRender( true );
    
};



Scene.prototype.removeModelByName = function ( name ) {
    
    var scene  = this.getScene();
    var models = this.getModels();
    
    this.setRender( false );    
    
    for ( var i = 0; i < models.length; ++i ) {
        
        if ( models[i] !== undefined && models[i].getName() === name ) {
            
            // On doit aussi enlever le collider si il existe            
            if ( models[i].getCollider() !== undefined ) {
                
                scene.remove( models[i].getCollider() );
                models[i].setCollider( undefined );
                
            }            
        
            scene.remove( models[i].getModel() );
            models[i].setModel( undefined );
            models[i] = undefined;           
            
        }
        
    }  

    this.modelsListCleanup();
    
    // TODO: Enlever le callback d'animation ici
    
    this.setRender( true );    
    
};



Scene.prototype.clear = function () {
    
    var scene  = this.getScene();
    var models = this.getModels();

    this.setRender( false );
        
    // Vide la liste des callbacks d'animations
    this.setAnimCallbacks( new Array() );
    
    for ( var i = 0; i < models.length; ++i ) {
        
        if ( models[i] !== undefined ) {
            
            // On doit aussi enlever le collider si il existe            
            if ( models[i].getCollider() !== undefined ) {

                scene.remove( models[i].getCollider() );
                models[i].setCollider( undefined );
                
            }            
        
            // TODO: Pour régler le bug
            if ( models[i].getModel() !== undefined )
                scene.remove( models[i].getModel() );
            
            models[i].setModel( undefined );
            models[i] = undefined;           
            
        }
        
    }  

    // Efface la liste des mod?les (ainsi que les buildings)
    this.setModels( new Array() );
    this.setBuildings( new Array() );
    
    // TODO: Temporaire pour fixer bug, on vide la liste des preloads!
    this.clearObjects()
    
    this.setRender( true );    
    
};



Scene.prototype.modelsListCleanup = function () {
    
    var models      = this.getModels();
    var cleanmodels = new Array();
    
    for ( var i = 0; i < models.length; ++i ) {
        
        if ( models[i] !== undefined )
            cleanmodels.push( models[i] );
        
    } 
    
    this.setModels( cleanmodels );
    
};




Scene.prototype.removeBuilding = function ( building ) {
    
    var scene     = this.getScene();
    var buildings = this.getBuildings();
        
    this.setRender( false );    
    
    for ( var i = 0; i < buildings.length; ++i ) {
        
        if ( buildings[i] === building ) {
        
            building.remove();
            buildings[i] = undefined;           
            
        }
        
    }  

    this.buildingsListCleanup();
    
    // TODO: Enlever le callback d'animation ici
    
    this.setRender( true );    
    
};



Scene.prototype.removeBuildingByName = function ( name ) {
    
    var scene     = this.getScene();
    var buildings = this.getBuildings();

    this.setRender( false );    
    
    for ( var i = 0; i < buildings.length; ++i ) {
        
        if ( buildings[i] !== undefined && buildings[i].getName() === name ) {
        
            buildings[i].remove();
            buildings[i] = undefined;           
            
        }
        
    }  

    this.buildingListCleanup();
    
    // TODO: Enlever le callback d'animation ici
    
    this.setRender( true );    
    
};



Scene.prototype.removeAllBuildings = function () {
    
    var scene  = this.getScene();
    var buildings = this.getBuildings();
    
    // TODO: Enlever le callback d'animation ici
    
    this.setRender( false );    
    
    for ( var i = 0; i < buildings.length; ++i ) {
        
        if ( buildings[i] !== undefined ) {
        
            buildings[i].remove();
            buildings[i] = undefined;           
            
        }
        
    }  

    // Efface la liste des buildings
    this.setBuildings( new Array() );
    
    // TODO: Enlever le callback d'animation ici
    
    this.setRender( true );    
    
};



Scene.prototype.buildingsListCleanup = function () {
    
    var buildings      = this.getBuildings();
    var cleanbuildings = new Array();
    
    for ( var i = 0; i < buildings.length; ++i ) {
        
        if ( buildings[i] !== undefined )
            cleanbuildings.push( buildings[i] );
        
    } 
    
    this.setBuildings( cleanbuildings );
    
};



Scene.prototype.setCameraPos = function ( x, y, z ) {
    
    var cameraCtrler = this.getCameraCtrler();
    
    cameraCtrler.__dirtyRotation = true; 
    cameraCtrler.__dirtyPosition = true; 
    
    cameraCtrler.position.x = x;
    cameraCtrler.position.y = y;
    cameraCtrler.position.z = z;     
    
};



Scene.prototype.setCameraDir = function ( x, y, z ) {
    
    var camera = this.getCamera();
    
    // TODO: Devrait vraiment changer la direction de la cam?ra, pour l'instant c'est comme ?a ne attendant
    //       pour faire marcher le code.
    camera.lookAt( new THREE.Vector3( 0, this.getCameraHeight(), -0.1 ) ); 
    
};



Scene.prototype.createTerrain = function (terrainDimensions, proportionBuildings, blocDimensions, dayInSeconds) {
 
    var terrain = this.getTerrain();
    var clock   = this.getClock();

    clock = new WorldClock( dayInSeconds );
    this.setClock( clock );

    terrain = new Terrain( terrainDimensions, this, proportionBuildings, blocDimensions );
    
    this.setTerrain( terrain );
    
    terrain.render();
    
    return terrain;
  
};



Scene.prototype.createSky = function() {
    
     var sky = this.getSky();
     sky     = new Sky( this );
     
     this.setSky( sky );

     return sky;
     
};



Scene.prototype.reset = function() {

    var scene = this.getScene();

    // Détruit tous les objets de la scène
    this.clear();
    
    // Détruit ce qui reste
    scene.remove( this.getCamera() );
    scene.remove( this.getCameraCtrler() );
    
    this.setCamera( undefined );
    this.setScene( undefined );
    this.setCameraCtrler( undefined );
    this.setRenderer( undefined );
    this.setTerrain( undefined );
    
};



// TODO: Pour l'instant seulement les capsules colliders ne fonctionne pas
Scene.prototype.addCollider = function ( type, position, rotation, size, friction, restitution ) {    

    if ( Physijs === undefined || this.getScene() === undefined )
        return;      
        
    var geometry;
    var collider;          
    var material;
    var mass         = 0;    
    var material     = Physijs.createMaterial( new THREE.MeshBasicMaterial( { color: 0xFF0000, transparent: true, opacity: 0 } ), friction, restitution );
    var colliderObj  = new Model( "scence_collider" );      
    
    switch ( type ) {
        
        // Box collider
        case 0 : geometry = new THREE.CubeGeometry( size.x, size.y, size.z );
                 collider = new Physijs.BoxMesh( geometry, material, mass );
                 break;
                 
        // Plane collider
        case 1 : geometry = new THREE.PlaneGeometry( boundingbox.size().x, boundingbox.size().y ); 
                 collider = new Physijs.PlaneMesh( geometry, material, mass );
                 break;

        // Sphere collider
        case 2 : geometry = new THREE.SphereGeometry( size, 32, 32 );
                 collider = new Physijs.SphereMesh( geometry, material, mass );
                 break;

        // Cylinder collider
        case 3 : geometry = new THREE.CylinderGeometry( size.x, size.x, size.y, 32 );                
                 collider = new Physijs.CylinderMesh( geometry, material, mass );
                 break;  
                 
        // Cone collider        
        case 4 : geometry = new THREE.CylinderGeometry( 0, size.x, size.y, 32 ); 
                 collider = new Physijs.ConeMesh( geometry, material, mass );
                 break;                  

        // Capsule collider
        //case 5 : collider = new Physijs.CapsuleMesh( geometry, material, mass );
        //         break;  

        // Box collider by default
        default: geometry = new THREE.CubeGeometry( size.x, size.y, size.z );
                 collider = new Physijs.BoxMesh( geometry, material, mass );
                 break;       
                 
    }
    
    collider.__dirtyPosition = true;
    collider.__dirtyRotation = true;    

    collider.position.set( position.x, position.y, position.z );
    collider.rotation.set( rotation.x, rotation.y, rotation.z );        
    
    // Pour éviter que le collider bouge quand on le place
    // TODO: Pas certain que c'est utile
    collider.setAngularFactor( new THREE.Vector3( 0, 0, 0 ) );        
    
    colliderObj.setModel( collider );
    
    this.addModel( colliderObj );
    
    // Le collider ne projète pas d'ombres
    collider.castShadow    = false;
    collider.receiveShadow = false;
    
    collider.visible = false;
    
    return colliderObj;
    
};



Scene.prototype.showCollider = function ( mode, collider ) {  

    if ( collider !== undefined && collider.getModel() !== undefined ) {
        
        if ( mode === true ) {
           
            collider.getModel().visible = true;          
            collider.getModel().material.opacity = 0.08;
            
        } else {
            
            collider.getModel().visible = false;
            collider.getModel().material.opacity = 0.0;
            
        }
        
    }
    
};



Scene.prototype.processAnimCallbacks = function ( mode, collider ) { 

    var animModels = this.getAnimCallbacks();
        
    for ( var i = 0; i < animModels.length; ++i ) {
        
        // TODO: BUG DES LUMIÈRES DE RUE, FIX TEMPORAIRE, VOIR AUSSI LA FONCTION calculateCameraCulling()
        //       ce fix n'est pas obligatoire, mais règle le problème des lumières qui reste allumées
        var bypass = animModels[i].getName().indexOf("streetlight") === 0 ? true : false;
        
        if ( bypass || ( animModels[i].isVisible() && animModels[i].isLoaded() && animModels[i].getAnimCallback() !== undefined ) ) {
        
            // On ne process l'animation que si le controlleur de caméra n'est pas plus loin
            // que la distance maximum pour animer l'objet pour optimiser de rendu.
            // On ne calcul pas la distance réel pour éviter le sqrt pour optimiser mais la distance max sur le x et z
            if ( bypass 
            ||   animModels[i].getAnimCallbackData().maxanimdist === undefined 
            || ( animModels[i].getAnimCallbackData().forceanim   !== undefined && animModels[i].getAnimCallbackData().forceanim === true )
            || ( animModels[i].getAnimCallbackData().maxanimdist !== undefined
            && ( Math.max( Math.abs( animModels[i].getPosition().x - this.getCameraCtrler().position.x ), 
                           Math.abs( animModels[i].getPosition().z - this.getCameraCtrler().position.z ) ) <= animModels[i].getAnimCallbackData().maxanimdist ) ) ) {
                
                animModels[i].getAnimCallback()( this, animModels[i] );
                
            }
        
        }
        
    }
    
};



Scene.prototype.rgbToInt= function( r, g, b ) {

    return ( ( r << 16 ) + ( g << 8 ) + b );
};

/* INF5071 - Infographie - Travail Pratique 2 - UQAM A2015
 *
 *   Etudiant   : Stephane Olssen
 *   Code perm  : OLSS14037300
 *   Courriel   : olssen.stephane@courrier.uqam.ca
 *   
 *   Etudiant   : Thomas Robert de Massy
 *   Code perm  : ROBT24057409
 *   Courriel   : robert_de_massy.thomas@courrier.uqam.ca     
 *
 *   Professeur : Alexandre Blondin Massé
 */
 
var Move = function( thescene, pos, dir ) {

    // Membres publiques   
    this.tween;
        
    // Membres privés
    var that           = this;
    var scene;
    var moving;
    var position;
    var direction;
    var cameraheight   = 0.3;    
    var forwardPressed = false;
    var backPressed    = false;
    var cameraLook     = false; 
    
    /** "Constructeur"
     * 
     */		
    ( function () {
        
        scene    = thescene;
        moving   = false;
        
        if ( pos !== undefined ) {
            
            position  = pos;
            direction = dir;
            scene.setCameraPos( pos.x, pos.y + cameraheight, pos.z );
            scene.setCameraDir( dir.x, dir.y + cameraheight, dir.z );              
            
        }
        
    } )();
    
    
    
    // Getters/Setters
    this.setScene        = function ( s )  { scene = s; };        
    this.getScene        = function ()     { return scene; };        
    this.getCamera       = function ()     { if ( scene !== undefined ) return scene.getCamera(); return undefined; }; 
    this.getCameraCtrler = function ()     { if ( scene !== undefined ) return scene.getCameraCtrler(); return undefined; }; 
    this.setMoving       = function ( m )  { moving = m; };        
    this.getMoving       = function ()     { return moving; };     
    
    
    
    // Clavier
    document.onkeydown = function( e ) {
           
         
        switch ( e.keyCode ) {
        
            case 37: // Left arrow (déplacement à gauche)
                resetCamera();            
                tweenRotation( 2 * Math.PI );             
                break;
                
            case 39: // Right arrow (déplacement à droite)
                resetCamera();            
                tweenRotation( -2 * Math.PI );                
                break;
                
            case 38: // Up arrow (déplacement en avant)
                resetCamera();            
                tweenMove( 5 );
                forwardPressed = true;                
                break;
                
            case 40: // Down arrow (déplacement en arrière)
                resetCamera();            
                tweenMove( -5 );
                backPressed = true;
                break;
                
            case 33: // Page up (regarde en haut)          
                tweenLook( 1 );
                cameraLook = true;
                break;
                
            case 34: // Page down (regarde en bas)
                tweenLook( -1 );
                cameraLook = true;
                break;
                
        }
        
    };
    
    
    
    document.onkeyup = function( e ) {
        
        switch ( e.keyCode ) {
            
            case 38 : forwardPressed = false;
                      break;
                
            case 40 : backPressed = false;
                      break;
                      
        }
        
        that.getCameraCtrler().setLinearVelocity( { z: 0, y: 0, x: 0 } );  
        
        if ( that.tween !== undefined ) {
            
            that.tween.stop();
            that.tween = undefined;
            
        }
        
        if ( forwardPressed ) 
            tweenMove( 5 );
        
        if ( backPressed ) 
            tweenMove( -5 );        
        
    };
       
       
       
    var resetCamera = function() {

        if ( cameraLook === true ) {
            
            var needRotate = false;
            
            if ( that.getCamera().getWorldDirection().z  > 0 )
                needRotate = true;
            
            that.getCamera().lookAt( new THREE.Vector3( that.getCamera().getWorldDirection().x, that.getScene().getCameraHeight(), -Math.abs( that.getCamera().getWorldDirection().z ) ) ); 
            cameraLook = false;
            
            if ( needRotate )
                that.getCamera().rotation.y = Math.PI - that.getCamera().rotation.y;
            
        }
        
    };    

    
    
    // Tweening mouvement avant/arrière
    var tweenMove = function ( deltaZ ) {
        
        that.setMoving( true );                
        
        var dir = new THREE.Vector3( 0, 0, 0 );
           
        dir.x = that.getCamera().getWorldDirection().x;
        dir.y = that.getCamera().getWorldDirection().y;
        dir.z = that.getCamera().getWorldDirection().z;
           
        dir.setLength( deltaZ );           
           
        that.getCameraCtrler().setLinearVelocity( dir );        
        
    };

    
    
    // Tweening du look UP/DOWN
    var tweenLook = function ( deltaX ) {
        
        that.setMoving( true );

        ( function() {
            
            var dir = new THREE.Vector3( 0, 0, 0 );
           
            dir.x = that.getCamera().getWorldDirection().x;
            dir.y = that.getCamera().getWorldDirection().y + that.getScene().getCameraHeight();
            dir.z = that.getCamera().getWorldDirection().z; 
            
            dir.normalize();
            
            var params = { x : dir.x, y: dir.y, z: dir.z };
            
            if ( that.tween !== undefined )
                return;
                
            that.tween = new TWEEN.Tween( params ).to( { x: dir.x, y: dir.y + deltaX, z: dir.z }, 300 ).onUpdate( function () {           
                
                that.getCamera().lookAt( params );
                
            } ).onComplete( function () {
              
                that.tween = undefined;
                
            } ).start();
            
        } ).call( this );
        
    }; 
    
    

    // Tweening rotation gauche/droite
    var tweenRotation = function ( deltaY ) {
        
        that.setMoving( true );

        ( function() {
            
            var params = { y: that.getCamera().rotation.y };
            
            if ( that.tween !== undefined )
                 return;
             
            that.tween = new TWEEN.Tween( params ).to( { y: that.getCamera().rotation.y + deltaY }, 3000 ).onUpdate( function () {
                
                that.getCamera().__dirtyRotation = true; 
                that.getCamera().__dirtyPosition = true;                
                that.getCamera().rotation.y      = params.y;

            } ).onComplete( function () {
              
                that.tween = undefined;
                
            } ).start();
            
        } ).call( this );
        
    };   
    
    return this;
    
};


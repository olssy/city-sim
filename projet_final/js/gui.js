/* INF5071 - Infographie - Travail Pratique 2 - UQAM A2015
 *
 *   Etudiant   : Stephane Olssen
 *   Code perm  : OLSS14037300
 *   Courriel   : olssen.stephane@courrier.uqam.ca
 *   
 *   Etudiant   : Thomas Robert de Massy
 *   Code perm  : ROBT24057409
 *   Courriel   : robert_de_massy.thomas@courrier.uqam.ca     
 *
 *   Professeur : Alexandre Blondin Massé
 */

function gui_init(that) {

    that = this;

    // les tabs des parametres/options
    $("#tabs").tabs();

    // Slider taille ville
    $("#tailleVilleH_title").html("De la ville(horizontale) : " + that.theScene.getConfig("h_size"));
    $("#tailleVilleH").slider({
        "min": 12,
        "max": 240,
        "step": 4,
        value: that.theScene.getConfig("h_size"),
        slide: function(event, ui) {
            $("#tailleVilleH_title").html("De la ville(horizontale) : " + ui.value);
            that.theScene.setConfig("h_size", ui.value);
        }
    });


    $("#tailleVilleV_title").html("De la ville (verticale) : " + that.theScene.getConfig("v_size"));
    $("#tailleVilleV").slider({
        "min": 12,
        "max": 240,
        "step": 4,
        value: that.theScene.getConfig("v_size"),
        slide: function(event, ui) {
            $("#tailleVilleV_title").html("De la ville (verticale) : " + ui.value);
            that.theScene.setConfig("v_size", ui.value);
        }
    });


    // Slider taille bloc 
    $("#tailleBlocMinH_title").html("D'un bloc (horizontale) : " + that.theScene.getConfig("h_num"));
    $("#tailleBlocMinH").slider({
        "min": 4,
        "max": 32,
        "step": 4,
        value: that.theScene.getConfig("h_num"),
        slide: function(event, ui) {
            $("#tailleBlocMinH_title").html("D'un bloc (horizontale) : " + ui.value);
            that.theScene.setConfig("h_num", ui.value);
        }
    });

    $("#tailleBlocMinV_title").html("D'un bloc (verticale) : " + that.theScene.getConfig("v_num"));
    $("#tailleBlocMinV").slider({
        "min": 4,
        "max": 32,
        "step": 4,
        value: that.theScene.getConfig("v_num"),
        slide: function(event, ui) {
            $("#tailleBlocMinV_title").html("D'un bloc (verticale) : " + ui.value);
            that.theScene.setConfig("v_num", ui.value);
        }
    });


    // Slider jumelles pour les porportion de batisses 
    $("#propInd_title").html("Bâtisses industrielles (%) : " + that.theScene.getConfig("buildings_percent"));
    $("#propInd").slider({
        "min": 0,
        "max": 100,
        "step": 1,
        value: that.theScene.getConfig("buildings_percent"),
        slide: function(event, ui) {
            $("#propInd_title").html("Bâtisses industrielles (%) : " + ui.value);
            that.theScene.setConfig("buildings_percent", ui.value);
        }
    });
    $('#propInd').bind('slidechange', function(event, ui) {
        $("#propInd_title").html("Bâtisses industrielles (%) : " + $("#propInd").slider('value'));
        that.theScene.setConfig("buildings_percent", ui.value);
    });


    $("#propRes_title").html("Bâtisses residentielles (%) : " + that.theScene.getConfig("houses_percent"));
    $("#propRes").slider({
        "min": 0,
        "max": 100,
        "step": 1,
        value: that.theScene.getConfig("houses_percent"),
        slide: function(event, ui) {
            $("#propRes_title").html("Bâtisses residentielles (%) : " + ui.value);
            that.theScene.setConfig("houses_percent", ui.value);
        }
    });
    $('#propRes').bind('slidechange', function(event, ui) {
        $("#propRes_title").html("Bâtisses residentielles (%) : " + $("#propRes").slider('value'));
        that.theScene.setConfig("houses_percent", ui.value);
    });


    $("#propPub_title").html("Espaces publiques (%) : " + that.theScene.getConfig("public_percent"));
    $("#propPub").slider({
        "min": 0,
        "max": 100,
        "step": 1,
        value: that.theScene.getConfig("public_percent"),
        slide: function(event, ui) {
            $("#propPub_title").html("Espaces publiques (%) : " + ui.value);
            that.theScene.setConfig("public_percent", ui.value);
        }
    });
    $('#propPub').bind('slidechange', function(event, ui) {
        $("#propPub_title").html("Espaces publiques (%) : " + $("#propPub").slider('value'));
        that.theScene.setConfig("public_percent", ui.value);
    });

    // Lier les slider
    $('.propSlider').slider().linkedSliders();

    // slider vitesse d'une journee
    $("#timeSpeed_title").html("Vitesse d'un jour (sec) : " + that.theScene.getConfig("day_duration"));
    $("#timeSpeed").slider({
        "min": 24,
        "max": 300,
        "step": 1,
        value: that.theScene.getConfig("day_duration"),
        slide: function(event, ui) {
            $("#timeSpeed_title").html("Vitesse d'un jour (sec) : " + ui.value);
            that.theScene.setConfig("day_duration", ui.value);
        }
    });


    // bouton pour generer une nouvelle ville
    $("#generateButton")
        .button()
        .click(function(event) {

            // prendre les bonnes valeurs de la configuration
            var terrainDimensions = [theScene.getConfig("h_size"), theScene.getConfig("v_size")];
            var buildingProportions = [theScene.getConfig("buildings_percent"),
                theScene.getConfig("houses_percent"),
                theScene.getConfig("public_percent"), 0
            ];
            var blocDimensions = [theScene.getConfig("h_num"), theScene.getConfig("v_num")];
            var dayInSeconds = theScene.getConfig("day_duration");

            theScene.clear(); // detruit les modele dans l'ancien terrain

            theScene.getClock().restart(); // creer une nouvelle horloge pour s'assurer que l'option day_duration est pris en compte

            // Préload les modèles en mémoire
            theScene.preloadModels(function() {

                theScene.getClock().setFullDayDuration(timeSpeed); // desuet

                // creer le nouveau terrain
                terrain = theScene.createTerrain(terrainDimensions, buildingProportions, blocDimensions, dayInSeconds);

                // Position de départ 
                theScene.getCameraCtrler().__dirtyPosition = true;
                theScene.getCameraCtrler().position.set(0, 1, 0);


            });
            // deuxieme fois pour contrer un bug dans ammo.js
            theScene.getCameraCtrler().position.set(0, 1, 0);

        });

    // options pour la generation

    // option centre ville
    $("#hasDowntown").prop("checked", that.theScene.getConfig("hasDowntown"));
    $("#hasDowntown")
        .bind('change', function() {
            that.theScene.setConfig("hasDowntown", !that.theScene.getConfig("hasDowntown"));
        });

    // option decor de la ville
    $("#cityDecoration").prop("checked", that.theScene.getConfig("cityDecoration"));
    $("#cityDecoration")
        .bind('change', function() {
            that.theScene.setConfig("cityDecoration", !that.theScene.getConfig("cityDecoration"));
        });

    // options dynamiques

    // option ombrages
    $("#ombrages").prop("checked", that.theScene.getConfig("cast_shadows"));
    $("#ombrages")
        .bind('change', function() {
            that.theScene.setConfig("cast_shadows", !that.theScene.getConfig("cast_shadows"));
            theScene.setShadowsEnabled(!theScene.getShadowsEnabled());
            theScene.getSky().setShadows(that.theScene.getConfig("cast_shadows"));
        });

    // option eclairage dynamique
    $("#dynamicLighting").prop("checked", that.theScene.getConfig("dynamic_lights"));
    $("#dynamicLighting")
        .bind('change', function() {
            theScene.setConfig("dynamic_lights", !theScene.getConfig("dynamic_lights"));
        });
}

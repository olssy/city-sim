/* INF5071 - Infographie - Travail Pratique 2 - UQAM A2015
 *
 *   Etudiant   : Stephane Olssen
 *   Code perm  : OLSS14037300
 *   Courriel   : olssen.stephane@courrier.uqam.ca
 *   
 *   Etudiant   : Thomas Robert de Massy
 *   Code perm  : ROBT24057409
 *   Courriel   : robert_de_massy.thomas@courrier.uqam.ca     
 *
 *   Professeur : Alexandre Blondin Massé
 */

// Seul objet global 
var theScene;

$( document ).ready( function() {

    Physijs.scripts.worker = 'js/three/physijs_worker.js';
    Physijs.scripts.ammo   = 'ammo.js';

    // Creation de la scene
    theScene = new Scene( "canvas", 0x000000 );
    theScene.setSize( $( window ).width() - 285, $( window ).height() - 30 );
    
    gui_init(this);
    
    // Préload les modèles en mémoire
    theScene.preloadModels(  function() {
    
    // Affiche les statistiques
    theScene.setShowStats( true );
    
    // ATTENTION : Ombres, commenter ou mettre a false si demande trop de puissance
    theScene.setShadowsEnabled( theScene.getConfig( 'cast_shadows' ) );
    
    // Creation de la scène principale
    var scene = theScene.create();
    
    // OMBRAGEFIX A DECOMMENTER QUAND VA ETRE DANS SKY 
    var sky = theScene.createSky();         

    // Créer le terrain
    var terrainDimensions    = [ theScene.getConfig("h_size"), theScene.getConfig("v_size") ];
    var buildingProportions  = [ theScene.getConfig("buildings_percent"), theScene.getConfig("houses_percent"), theScene.getConfig("public_percent"), 0 ];
    var blocDimensions       = [ theScene.getConfig("h_num"), theScene.getConfig("v_num") ];
    var dayInSeconds         = theScene.getConfig("day_duration");
    
    var terrain = theScene.createTerrain( terrainDimensions, buildingProportions, blocDimensions, dayInSeconds );

    // Positionnement de la camera
    theScene.setCameraPos( 0, 0.5, 0);

    // Mouvements    
    var move = new Move( theScene );
    
    // Le callback pour l'animation du render
    theScene.setRenderCallback( function() {

       // Pour les mouvements de caméra       
       TWEEN.update();       
        
    } );    
   
    // Débute le rendering the la scène.
    theScene.render();    
    
    } ); // Fin préload modèles
    
    Number.prototype.clamp = function( min, max ) {
       
        return Math.min( Math.max( this, min ), max );
    };
            
} );

( jQuery );   

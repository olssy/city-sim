/* INF5071 - Infographie - Travail Pratique 2 - UQAM A2015
 *
 *   Etudiant   : Stephane Olssen
 *   Code perm  : OLSS14037300
 *   Courriel   : olssen.stephane@courrier.uqam.ca
 *   
 *   Etudiant   : Thomas Robert de Massy
 *   Code perm  : ROBT24057409
 *   Courriel   : robert_de_massy.thomas@courrier.uqam.ca     
 *
 *   Professeur : Alexandre Blondin Massé
 *
 *   scene - La scene
 *
 */

var Sky = function(scene) {

    this.uniforms; // les variables uniform du shader du ciel 
    this.scene = scene;

    this.sky; // modele objet3d du ciel de jour
    this.skyMesh; // maille THREE.js du ciel de jour 
    this.sun; // le soleil
    this.nightSky; // modele objet3d du ciel de nuit

    this.mainLight; // La lumiere principale qui eclaire comme un soleil

    /*
     * La lumiere ambiante du monde
     */
    this.ambientLightColor = new THREE.Color(0xbbbbbb);
    this.ambientLight = new THREE.AmbientLight(this.ambientLightColor);
    this.ambientLightMaxIntensity = this.scene.getConfig("ambientLightMax");
    this.ambientLightMinIntensity = this.scene.getConfig("ambientLightMin");
    this.ambientLight.color.setHSL(this.ambientLight.color.getHSL().h, this.ambientLight.color.getHSL().s, this.ambientLightMaxIntensity);
    this.scene.getScene().add(this.ambientLight);

    this.ambientLightColor = this.ambientLight.color;
    this.ambientLightHSL = this.ambientLightColor.getHSL();

    /*
     * Variables pour le shader du ciel de jour
     * inspirer de imrdoob.github.io/three.js/examples/webgl_lights_hemisphere.html
     */
    var vertexShader = document.getElementById('vertexShader').textContent;
    var fragmentShader = document.getElementById('fragmentShader').textContent;
    var topColor = new THREE.Color(0x0077ff)
    var bottomColor = new THREE.Color(0xff0000)

    this.uniforms = {
        topColor: {
            type: "c",
            value: topColor
        },
        bottomColor: {
            type: "c",
            value: bottomColor
        },
        offset: {
            type: "f",
            value: 1000
        },
        exponent: {
            type: "f",
            value: 0.6
        },
        time: {
            type: "f",
            value: 1 //this.scene.getClock().getTime()/24 
        }
    };

    // Geometrie et materiel pour le ciel de jour
    var skyGeo = new THREE.SphereGeometry(196, 16, 16);
    var skyMat = new THREE.ShaderMaterial({
        vertexShader: vertexShader,
        fragmentShader: fragmentShader,
        uniforms: this.uniforms,
        side: THREE.BackSide
    });

    // l<objet THREE du ciel de jour
    this.skyMesh = new THREE.Mesh(skyGeo, skyMat);

    // texture du ciel de nui
    var texture = THREE.ImageUtils.loadTexture("assets/textures/ciel-nuit-1024.png");
    texture.wrapS = THREE.RepeatWrapping;
    texture.wrapT = THREE.RepeatWrapping;
    texture.repeat.set(2, 2);

    // objet THREE du ciel de nuit
    this.nightSky = new THREE.Mesh(
        new THREE.SphereGeometry(195, 16, 16),
        new THREE.MeshBasicMaterial({
            map: texture,
            side: THREE.BackSide
        })
    );
    this.nightSky.material.opacity = 0;
    this.scene.getScene().add(this.nightSky);
    // rotation du ciel de nuit pour mettre le croisement des textures sur le cote
    this.nightSky.rotation.z = -Math.PI / 2;

    this.nightSky.material.transparent = true; // permet la transparence du ciel de nuit

    // le soleil
    var sunGeo = new THREE.SphereGeometry(6, 24, 24);
    var sunMat = new THREE.MeshBasicMaterial({
        color: 0xffff00
    });
    this.sun = new THREE.Mesh(sunGeo, sunMat);
    this.sun.position.set(0, 194, 0);

    // ajuste la lumiere du soleil
    this.mainLightColor = new THREE.Color(0xddaaaa);
    this.mainLightMax = new THREE.Color(0xffdddd);
    this.mainLightMin = new THREE.Color(0x000000);
    this.mainLight = new THREE.DirectionalLight(this.mainLightColor, 1);
    this.mainLight.position.set(0, 50, 0);
    this.mainLightMaxIntensity = 1;
    this.mainLight.target = this.scene.getCameraCtrler();

    // ajoute la lumiere au soleil
    this.scene.getScene().add(this.mainLight);
    this.sun.add(this.mainLight);

    // ajoute le soleil et la lumeiere au ciel de jour
    this.skyMesh.add(this.sun);
    this.skyMesh.add(this.mainLight);
    this.sky = new Model("sky");
    this.sky.setModel(this.skyMesh);
    this.scene.getScene().add(this.skyMesh);


    /*
    / Function pour allumer/fermer les ombrages creer par le soleil
    / shadows: booleen pour savoir si on render des ombre ou pas
    */
    this.setShadows = function(shadows) {

        if (shadows) {

            // Shadows on
            this.mainLight.castShadow = true;
            this.mainLight.shadowCameraFar = 50;
            this.mainLight.shadowDarkness = 0.8;
            this.mainLight.shadowMapSoft = true;
            this.mainLight.shadowCameraNear = -50; //0.01;
            this.mainLight.shadowCameraFov = 50;
            this.mainLight.shadowMapBias = 0.0039;
            this.mainLight.shadowMapDarkness = 0.8;
            this.mainLight.shadowMapWidth = 2048;
            this.mainLight.shadowMapHeight = 2048;

            var d = 50;

            this.mainLight.shadowCameraRight = d;
            this.mainLight.shadowCameraLeft = -d;
            this.mainLight.shadowCameraTop = d;
            this.mainLight.shadowCameraBottom = -d;
        } else {

            this.mainLight.castShadow = false;
        }
    };

    // mettre ou enlever les ombres depandant de la configuration
    this.setShadows(this.scene.getConfig("cast_shadows"));

    // rotation pour desaligner la rotation du soleil avec les rues
    this.skyMesh.rotation.z = .2;
    this.nightSky.rotation.z = .2;

    /*
     * Fonction qui dessine le ciel de jour avec un shader.
     * Le shader crer un degrader entre 2 couleurs
     */
    this.renderSkyShader = function() {

        // le couleur du haut et du bas de la degrader
        bottomColorHSL = this.uniforms.bottomColor.value.getHSL();
        topColorHSL = this.uniforms.topColor.value.getHSL();

        if (this.scene.getClock().isDawn()) { // l'aube

            this.uniforms.offset.value = .5; // offset du degrader pour un lever du soleil

        } else if (this.scene.getClock().isMorning()) { // matin

            if (this.scene.getClock().getPercentageOfMorning() < .5) { // premier moitie du matin

                // de 0 a 1000
                this.uniforms.offset.value = 1000 * this.scene.getClock().getPercentageOfMorning() * 2;
            } else {

                this.uniforms.offset.value = 1000;
            }
        } else if (this.scene.getClock().isEvening()) { // soiree
            // de 100 a -50
            this.uniforms.offset.value = 100 - (this.scene.getClock().getPercentageOfEvening() * 150);

        } else if (this.scene.getClock().isNightTime()) { // nuit
            this.uniforms.offset.value = 0;

        } else {
            this.uniforms.offset.value = 1000;

        }
    };

    /*
     * Fonction qui dessine(render) le ciel
     */
    this.renderSky = function() {

        var sunScale = 1; // echelle du soleil

        if (this.scene.getClock().isNightTime()) { // la nuit
            // enleve la transparence du ciel de nuit la nuit
            this.nightSky.material.transparent = false;

        } else if (this.scene.getClock().isDawn()) { // l'aube

            sunScale = 1;

            this.nightSky.material.transparent = true; // allume la transparence
            // modifie la transparence depandant on est rendu ou dans l'aube
            this.nightSky.material.opacity = 1 - this.scene.getClock().getPercentageOfDawn();

        } else if (this.scene.getClock().isEvening()) { // soiree

            // modifie la transparence du ciel de nuit par rapport ou en est rendu dans la soiree
            this.nightSky.material.opacity = this.scene.getClock().getPercentageOfEvening();

            // grossi le soleil pour le coucher du soleil
            sunScale = (1 + this.scene.getClock().getPercentageOfEvening()).clamp(1, 1.5);

        } else if (this.scene.getClock().isMorning) {

            this.nightSky.material.transparent = true;
            this.nightSky.material.opacity = 0;
            // raptisse le soleil le matin
            sunScale = (1.5 - (this.scene.getClock().getPercentageOfMorning())).clamp(1, 1.5);
        } else {
            // le restant du temps assur que le ciel de nuit est cache
            this.nightSky.material.transparent = true;
            this.nightSky.material.opacity = 0
        }
        // change l'echelle du soleil
        this.sun.scale.set(sunScale, sunScale, sunScale);
        // dessine le ciel de jour
        this.renderSkyShader();
    };

    /*
     * Fonction qui effectue la rotation du ciel de jour et de nuit
     */
    this.rotateSky = function() {
        // rotation du ciel
        this.skyMesh.rotation.x = Math.PI + (this.scene.getClock().getTime() * (1 / 24) * 2 * Math.PI);
        // rotation du ciel de nuit
        this.nightSky.rotation.x = Math.PI + (this.scene.getClock().getTime() * (1 / 24) * 2 * Math.PI);
    };

    /*
     * Fonction qui bouge les cieux avec la camera
     */
    this.moveSky = function() {
        // bouge le ciel avec la camera
        this.skyMesh.position.copy(this.scene.getCameraCtrler().position);
        this.nightSky.position.copy(this.scene.getCameraCtrler().position);
    };

    /*
     * Fonction qui change l'eclairage du soleil avec l'heure du jour
     */
    this.renderMainLight = function() {

        if (this.scene.getClock().isDawn()) { // aube
            // soleil devient plus intense
            this.mainLight.intensity = (1 - this.scene.getClock().getPercentageOfDawn()).clamp(0, this.mainLightMaxIntensity);
        } else if (this.scene.getClock().isEvening()) { // soiree
            // soleil devient moins intense
            this.mainLight.intensity = (1 - this.scene.getClock().getPercentageOfEvening()) * this.mainLightMaxIntensity;
        } else if (this.scene.getClock().isNightTime()) { // la nuit
            // plus de lumiere de soleil la nuit
            this.mainLight.intensity = 0;
        } else {
            // en tout autre temps on eclaire au maximum
            this.mainLight.intensity = this.mainLightMaxIntensity;
        }
    };

    /*
     * Fonction qui change l'eclairage ambiante selon la periode du jour
     */
    this.renderAmbientLight = function() {

        var luminosity;

        if (this.scene.getClock().isDawn()) { // l'aube
            // augemente la luminosite
            luminosity = (this.ambientLightMaxIntensity * (this.scene.getClock().getPercentageOfDawn())).clamp(this.ambientLightMinIntensity, this.ambientLightMaxIntensity);
        } else if (this.scene.getClock().isEvening()) { // soiree
            // diminue la luminosite
            luminosity = (this.ambientLightMaxIntensity * (1 - this.scene.getClock().getPercentageOfEvening())).clamp(this.ambientLightMinIntensity, this.ambientLightMaxIntensity);
        } else if (this.scene.getClock().isNightTime()) { // la nuit
            // luminosite minimum
            luminosity = this.ambientLightMinIntensity;
        } else {
            // luminosite maximum
            luminosity = this.ambientLightMaxIntensity;
        }

        // applique la nouvelle luminosite
        this.ambientLight.color.setHSL(this.ambientLightHSL.h,
            this.ambientLightHSL.s,
            luminosity);
    };

    /*
     * Fonction principale pour dessiner(render) le ciel
     */
    this.render = function() {

        this.rotateSky();
        this.moveSky();
        this.renderSky();

        // Si la configuration demande l'eclairage dynamique alors
        if (this.scene.getConfig("dynamic_lights") === true) {

            this.renderMainLight();
            this.renderAmbientLight();

        } else { // sinon on enleve le soleile et on augmente la lumiere ambiante au maximum

            this.mainLight.intensity = 0;
            this.ambientLight.color = new THREE.Color(0xffffff);

        }
    };
};
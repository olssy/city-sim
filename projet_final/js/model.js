/* INF5071 - Infographie - Travail Pratique 2 - UQAM A2015
 *
 *   Etudiant   : Stephane Olssen
 *   Code perm  : OLSS14037300
 *   Courriel   : olssen.stephane@courrier.uqam.ca
 *   
 *   Etudiant   : Thomas Robert de Massy
 *   Code perm  : ROBT24057409
 *   Courriel   : robert_de_massy.thomas@courrier.uqam.ca     
 *
 *   Professeur : Alexandre Blondin Massé
 *   
 *    
 *                 load( callback )
 *                 loadComplex( callback )
 *                 applyColor( name, color )
 *                 tint( color, tintedpart ) 
 *                 applyTexture( name, texture ) 
 *   object        getChild( name )
 *   object[]      getChildren()
 *   material[]    getMaterials()
 *                 addCollider( type, mass, friction, restitution, [angularFactor] )
 *                 addColliderTo( model, scene, name, type, mass, friction, restitution )
 *                 addCustomCollider( type, position, rotation, size, mass, friction, restitution, [angularFactor] )
 *   bool          hasCollider()
 *                 removeCollider() 
 *                 showCollider( bool )
 *   bool          isLoaded()
 *                 scale( scale )
 *                 setPosition( x, y, z )
 *   THREE.Vector3 getPosition()
 *                 setRotation( x, y, z )
 *   THREE.Vector3 getRotation()
 *                 setPositionInsideCollider( x, y, z )
 *   THREE.Vector3 getPositionInsideCollider()
 *                 setRotationInsideCollider( x, y, z )
 *   THREE.Vector3 getRotationInsideCollider() 
 *                 remove()
 *                 clone()
 *                 addTo( scene ) 
 *
 *   Getters/Setters
 *
 *   ATTENTION! ?tre prudent en utilisant les setters, car vous pouvez briser la structure du mod?le et/ou de la sc?ne
 *   
 *                 setName( string )      
 *   string        getName()      
 *                 setHRef( string )     
 *   string        getHRef()  
 *                 setModel( object )      
 *   object        getModel()   
 *                 setComplex( bool )     
 *   bool          getComplex()
 *                 setDeferedData( object )       
 *   object        getDeferedData()
 *                 setScale( number )      
 *   number        getscale() 
 *                 setScene( number )       
 *   object        getScene()    
 *                 setCollider( model )        
 *   Model         getCollider() 
 *                 setAngularDamping = function ( THREE.Vector3 )
 *   THREE.Vector3 getAngularDamping = function ()
 *                 setAngularFactor  = function ( THREE.Vector3 )
 *   THREE.Vector3 getAngularFactor  = function ()    
 *     
 */
 
 
 
var Model = function( model_name, model_href ) {

    // Membres publiques        
    
    // Membres privés
    
    // IMPORTANT! Ne pas oublier d'updater clone() quand quelque chose est ajout? ici
      
    var that = this;
    var name;
    var href;
    var model;
    var complex;
    var defered_data;
    var scale;
    var scene;
    var collider;
    var angularDamping;
    var angularFactor;
    var visible;
    var animCallback;
    var animCallbackData = {};
    
    /** "Constructeur"
     * 
     */		
    ( function () {

        name           = model_name;
        href           = model_href;
        complex        = false;
        scale          = 1.0;
        visible        = true;
        angularDamping = new THREE.Vector3( 0, 0, 0 );
        angularFactor  = new THREE.Vector3( 1, 1, 1 ); 
        
    } )();
    
    // Getters/Setters
    this.setName             = function ( n )  { name = n; };        
    this.getName             = function ()     { return name; };       
    this.setHRef             = function ( h )  { href = h; };        
    this.getHRef             = function ()     { return href; };   
    this.setModel            = function ( m )  { model = m; };        
    this.getModel            = function ()     { return model; };   
    this.setComplex          = function ( c )  { complex = c; };        
    this.getComplex          = function ()     { return complex; }; 
    this.setDeferedData      = function ( dd )  { defered_data = dd; };        
    this.getDeferedData      = function ()     { return defered_data; };
    this.setScale            = function ( s )  { scale = s; };        
    this.getScale            = function ()     { return scale; };  
    this.setScene            = function ( s )  { scene = s; };        
    this.getScene            = function ()     { return scene; };     
    this.setCollider         = function ( c )  { collider = c; };        
    this.getCollider         = function ()     { return collider; }; 
    this.setAngularDamping   = function ( ad ) { angularDamping = ad; }
    this.getAngularDamping   = function ()     { return angularDamping; } 
    this.setAngularFactor    = function ( af ) { angularFactor = af; }
    this.getAngularFactor    = function ()     { return angularFactor; } 
    this.setAnimCallback     = function ( cb ) { animCallback = cb; };     
    this.getAnimCallback     = function ()     { return animCallback; };      
    this.setAnimCallbackData = function ( cd ) { animCallbackData = cd; };     
    this.getAnimCallbackData = function ()     { return animCallbackData; };
    this.setVisible          = function ( v )  { visible = v; };
    this.isVisible           = function ()     { return visible; };
        
    return this;
    
};



Model.prototype.load = function ( callback ) {

    var loader = new THREE.JSONLoader();
    var that   = this;
    
    this.setComplex( false );
    
    // Load le modèle
    loader.load( this.getHRef(), function ( geometry, materials ) {
             
            var model = new THREE.Mesh( geometry, materials );
            
            // Shadows
            model.castShadow    = true;
            model.receiveShadow = true;

            that.setModel( model );
                                    
            callback( that ); // Appel au callback un fois load?
            
        },
        function ( event ) {},
        function ( error ) { console.error( "error loading 3D model!" ) }
                                                                  
    );
    
};



Model.prototype.loadComplex = function ( callback ) {

    var loader = new THREE.ObjectLoader();
    var that   = this;
    
    this.setComplex( true );
    
    // Load le modèle complexe
    loader.load( this.getHRef(), function ( object ) {  
    
            that.setModel( object );
                                    
            // Ombrages et textures par défaut                                   
            object.traverse( function( child ) {
                
                if ( child instanceof THREE.Mesh ) {
                    
                    // Shadows
                    child.castShadow    = true;
                    child.receiveShadow = true;
                    
                    // Transparent doit être false pour ne pas être caché par les objets transparents, sauf pour les clotures
                    if ( child.name !== 'fence' && child.name !== 'fence_back' && child.name !== 'windows_buidingb' )
                        child.material.transparent = false;
                    else
                        child.material.transparent = true;
                    
                    // Optimisation, ne marche pas avec tous les objets (walking, poubelle, stop, boites electriques)
                    if ( child.name !== 'walking' && child.name !== 'construction_mur' && child.name !== 'wood' && child.name !== 'trash' && child.name !== 'panneau' && child.name !== 'boite1' && child.name !== 'boite2' )
                        child.material.side = THREE.SingleSide;
                    else
                        child.material.side = THREE.DoubleSide;
                    
                }
                                        
            } );
                                    
            callback( that ); // Appel au callback un fois load?
                                    
        },
        function ( event ) {},
        function ( error ) { console.error( "error loading 3D complex model!" ) }
                                 
    );
    
};



Model.prototype.applyColor = function ( name, color ) {

    if ( this.getModel() === undefined )
        return;

    if ( this.getComplex() ) {

        this.getModel().traverse( function( child ) {
                                        
            if ( child instanceof THREE.Mesh ) {
                
                if ( child.name === name )
                    child.material = new THREE.MeshPhongMaterial( {color: color} ); // MeshPhongMaterial MeshLambertMaterial MeshBasicMaterial
                    
            }
                                        
        } );       
      
    } else { // Objet non complexe
        
       if ( color !== undefined ) 
           this.getModel().material = new THREE.MeshPhongMaterial( {color: color} );
       else
           this.getModel().material = new THREE.MeshPhongMaterial( {color: name} );           
       
    }
    
};



Model.prototype.tint = function ( color, tintedpart ) {

        if ( tintedpart === undefined || this.getModel() === undefined )
            return;

        if ( this.getComplex() ) {
            
            if ( this.getModel().name === tintedpart ) {
                
                this.getModel().material = new THREE.MeshPhongMaterial( {
                        
                                color       : color,
                                map         : child.material.map,
                                transparent : false
                        
                } );
                
            } else {
            
                this.getModel().traverse( function( child ) {
                        
                    if ( child instanceof THREE.Mesh ) {
                    
                        if ( child.name === tintedpart ) {
                                                        
                            child.material = new THREE.MeshPhongMaterial( {
                        
                                color       : color,
                                map         : child.material.map,
                                transparent : false
                        
                            } );
                    
                        }
                    
                    }
                                        
                } );
                
            }
            
        } else { // objet non complexe
        
            this.getModel().material = new THREE.MeshPhongMaterial( {
                        
                color       : color,
                map         : this.getModel().material.map,
                transparent : false
                        
            } );
        
        }
    
};



Model.prototype.applyTexture = function ( name, texture ) {
   
    if ( this.getModel() === undefined )
        return;
    
    var that = this;
    
    // Seulement un argument?    
    if ( texture === undefined ) 
        texture = name;        
    
    var loader = new THREE.TextureLoader();
    var mat    = new THREE.MeshPhongMaterial(); // MeshPhongMaterial MeshLambertMaterial MeshBasicMaterial
    
    if ( that.getComplex() ) {

        that.getModel().traverse( function( child ) {
                                        
            if ( child instanceof THREE.Mesh ) {
                
                if ( child.name === name ) {
                        
                    loader.load( texture, function( tex ) {

                        mat.map         = tex;
                        mat.transparent = false;

                        that.getModel().material = mat;

                    } );
            

                }

            }
                                        
        } );       
      
    } else { // Objet non complexe

        loader.load( texture, function( tex ) {

            mat.map         = tex;
            mat.transparent = false;

            that.getModel().material = mat;

        } );

    }
    
};
 


Model.prototype.getChild = function ( name ) {
   
    if ( this.getModel() === undefined )
        return;
    
    if ( !this.getComplex() )
        return undefined;    

    var children = this.getModel().children;
 
    for ( index in children ) {
        
        if ( children[index] instanceof THREE.Mesh ) {
            
            if ( children[index].name === name )
                return children[index];

        }
        
    }
    
    return undefined; // Not found
    
};



Model.prototype.getChildren = function () {
   
    if ( this.getModel() === undefined )
        return;
    
    if ( !this.getComplex() )
        return undefined;    

    return this.getModel().children;
    
};



Model.prototype.getMaterials = function () {
   
    if ( this.getModel() === undefined )
        return;
    
    if ( !this.getComplex() )
        return this.getModel().material;    

    var children  = this.getModel().children;
    var materials = new Array();
    
    if ( this.getModel().material !== undefined )
        materials.push( this.getModel().material );
 
    for ( index in children ) {
        
        if ( children[index] instanceof THREE.Mesh )            
            materials.push( children[index].material );        
        
    }
    
    return materials;
    
};



// TODO: Pour l'instant seulement les capsules collider ne fonctionnent pas
Model.prototype.addCollider = function ( ptype, pmass, pfriction, prestitution, angularFactor ) {    
    
    var model = this.getModel();
        
    if ( model === undefined || Physijs === undefined || this.getScene() === undefined )
        return;  
    
    // On ne peut pas ajouter de collider si l'objet a un parent
    if ( model.parent !== this.getScene().getScene() && model.parent !== undefined && model.parent !== null ) {
        
        console.warn( "Cannot apply a collider to a child object!" );
        return;
        
    }
        
    var geometry;
    var collider;    
    var type         = ptype        !== undefined ? ptype        : 0; // 0 : Box collider
    var mass         = pmass;//        !== undefined ? pmass        : 1;
    var friction     = pfriction    !== undefined ? pfriction    : 0;
    var restitution  = prestitution !== undefined ? prestitution : 0;  
    var material     = Physijs.createMaterial( new THREE.MeshBasicMaterial( { color: 0xFF0000, transparent: true, opacity: 0 } ), friction, restitution );
    var colliderObj  = new Model( "collider_" + this.getName() );        
    var boundingbox  = new THREE.Box3().setFromObject( model );

    geometry = new THREE.CubeGeometry( boundingbox.size().x, boundingbox.size().y, boundingbox.size().z );
    
    switch ( type ) {
        
        // Box collider
        case 0 : geometry = new THREE.CubeGeometry( boundingbox.size().x, boundingbox.size().y, boundingbox.size().z );
                 collider = new Physijs.BoxMesh( geometry, material, mass );
                 break;
                 
        // Plane collider
        case 1 : geometry = new THREE.PlaneGeometry( boundingbox.size().x, boundingbox.size().y ); 
                 collider = new Physijs.PlaneMesh( geometry, material, mass );
                 break;

        // Sphere collider
        case 2 : geometry = new THREE.SphereGeometry( Math.max( Math.max( boundingbox.size().x, boundingbox.size().y ), boundingbox.size().z ) / 2, 32, 32 );
                 collider = new Physijs.SphereMesh( geometry, material, mass );
                 break;

        // Cylinder collider
        case 3 : geometry = new THREE.CylinderGeometry( Math.max( boundingbox.size().x, boundingbox.size().z ) / 2, Math.max( boundingbox.size().x, boundingbox.size().z ) / 2, boundingbox.size().y, 32 ); 
                 collider = new Physijs.CylinderMesh( geometry, material, mass );
                 break;  
        
        // Cone collider        
        case 4 : geometry = new THREE.CylinderGeometry( 0, Math.max( boundingbox.size().x, boundingbox.size().z ) / 2, boundingbox.size().y, 32 ); 
                 collider = new Physijs.ConeMesh( geometry, material, mass );
                 break;  

        // Box collider by default
        default: geometry = new THREE.CubeGeometry( boundingbox.size().x, boundingbox.size().y, boundingbox.size().z );
                 collider = new Physijs.BoxMesh( geometry, material, mass );
                 break;        
                 
    }
    
    model.updateMatrixWorld( true );     
    
    collider.position.set( model.position.x, model.position.y + boundingbox.size().y / 2, model.position.z );
    collider.rotation.set( model.rotation.x, model.rotation.y, model.rotation.z );
    
    // TODO: peut-être modifier hauteur
    
    model.position.set( 0, -(boundingbox.size().y / 2), 0 );
    model.rotation.set( 0, 0, 0 );
                
    // L'objet devient un enfant du collider  
    collider.add( model );       
    
    colliderObj.setModel( collider );
    colliderObj.setScene( this.getScene() );    
    
    this.setCollider( colliderObj );        
    
    // Seulement ajouté le collider à la scène quand l'objet est déjà ajouté à la scène
    if ( this.getScene() !== undefined )
        this.getScene().addModel( colliderObj );
    
    if ( angularFactor !== undefined )
        collider.setAngularFactor( angularFactor );
    else
        collider.setAngularFactor( new THREE.Vector3( 1, 1, 1 ) );     
    
    // Le collider ne proj?te pas d'ombres
    collider.castShadow    = false;
    collider.receiveShadow = false;
    
};



// TODO: Ne peut pas être utiliser avec plus d'un collider! N'est pas terminé, quelques bugs
// TODO: Pour l'instant seulement les capsules colliders ne fonctionne pas
Model.prototype.addCustomCollider = function ( type, position, rotation, size, mass, friction, restitution, angularFactor  ) {    

    var model = this.getModel();
        
    if ( model === undefined || Physijs === undefined || this.getScene() === undefined )
        return;    
        
   // var mass = 0;
    var geometry;
    var collider;          
    var material;  
    var material     = Physijs.createMaterial( new THREE.MeshBasicMaterial( { color: 0xFF0000, transparent: true, opacity: 0 } ), friction, restitution );
    var colliderObj  = new Model( "model_collider" );      
    
    switch ( type ) {
        
        // Box collider
        case 0 : geometry = new THREE.CubeGeometry( size.x, size.y, size.z );
                 collider = new Physijs.BoxMesh( geometry, material, mass );
                 break;
                 
        // Plane collider
        case 1 : geometry = new THREE.PlaneGeometry( boundingbox.size().x, boundingbox.size().y ); 
                 collider = new Physijs.PlaneMesh( geometry, material, mass );
                 break;

        // Sphere collider
        case 2 : geometry = new THREE.SphereGeometry( size, 32, 32 );
                 collider = new Physijs.SphereMesh( geometry, material, mass );
                 break;

        // Cylinder collider
        case 3 : geometry = new THREE.CylinderGeometry( size.x, size.x, size.y, 32 );                
                 collider = new Physijs.CylinderMesh( geometry, material, mass );
                 break;  
                 
        // Cone collider        
        case 4 : geometry = new THREE.CylinderGeometry( 0, size.x, size.y, 32 ); 
                 collider = new Physijs.ConeMesh( geometry, material, mass );
                 break;                  

        // Capsule collider
        //case 5 : collider = new Physijs.CapsuleMesh( geometry, material, mass );
        //         break;  

        // Box collider by default
        default: geometry = new THREE.CubeGeometry( size.x, size.y, size.z );
                 collider = new Physijs.BoxMesh( geometry, material, mass );
                 break;       
                 
    }

    collider.__dirtyPosition = true; 
    collider.__dirtyRotation = true; 
    
    collider.position.set( position.x, position.y, position.z );
    collider.rotation.set( rotation.x, rotation.y, rotation.z );  

    model.position.set( 0, -(size.y / 2), 0 );
    model.rotation.set( 0, 0, 0 );    

    if ( this.getCollider() !== undefined )
        this.getCollider().getModel().add( collider );    
    
    collider.add( model );    
    
    colliderObj.setModel( collider );
    
    this.getScene().addModel( colliderObj );
    
    if ( angularFactor !== undefined )
        collider.setAngularFactor( angularFactor );  
    else
        collider.setAngularFactor( new THREE.Vector3( 1, 1, 1 ) );  
    
    // Le collider ne projète pas d'ombres
    collider.castShadow    = false;
    collider.receiveShadow = false;
    
};



// Fait pour être utiliser comme fonction statique (new Model()).addCollider()
Model.prototype.addColliderTo = function ( pmodel, pscene, pname, ptype, pmass, pfriction, prestitution ) {  

    var model = new Model();
    
    model.setScene( pscene );
    model.setModel( pmodel );
    model.setName( pname );
    
    model.addCollider( ptype, pmass, pfriction, prestitution );
    
};



Model.prototype.hasCollider = function () {  

    if ( this.getCollider() !== undefined )
        return true;
    else
        return false;
    
};



Model.prototype.removeCollider = function () {  

    if ( this.getCollider() !== undefined ) { 
    
        this.getModel().parent = this.getCollider().getModel().parent;    
        this.getModel().setPosition( this.getPosition() );        
        this.getModel().setRotation( this.getRotation() );        
        this.getScene().getScene().remove( this.getCollider().getModel() );        
        this.setCollider( undefined );
        
    }
    
};



Model.prototype.showCollider = function ( mode ) {  

    if ( this.getCollider() ) {
        
        if ( mode === true )
            this.getCollider().getModel().material.opacity = 0.08;
        else    
            this.getCollider().getModel().material.opacity = 0.0;
        
    }
    
};



Model.prototype.isLoaded = function () {

    if ( this.getModel() === undefined ) 
        return false;    
    
    return true;
    
};



Model.prototype.scale = function ( scale ) {

    if ( this.getModel() !== undefined ) {
        
        this.setScale( scale );        
        this.getModel().scale.set( scale, scale, scale ); 
        
    }
    
    // Change aussi la taille du collider
    if ( this.getCollider() !== undefined ) {

        this.getCollider().scale( scale );
        
    }
    
};



Model.prototype.setPosition = function ( x, y, z ) {    
    
    // On doit changer la position du collider si il existe
    if ( this.getCollider() !== undefined ) {
        
        var collider = this.getCollider().getModel();
        
        if ( collider !== undefined ) {
            
            collider.__dirtyPosition = true;            
            collider.position.x      = x;
            collider.position.y      = y;
            collider.position.z      = z;
            
        }
        
    } else if ( this.getModel() !== undefined ) {
        
        var model = this.getModel();
        
        model.position.x = x;
        model.position.y = y;
        model.position.z = z;
        
    }
    
};



Model.prototype.setPositionInsideCollider = function ( x, y, z ) {

    if ( ( this.getCollider() !== undefined ) && ( this.getModel() !== undefined ) ) {
        
        var model = this.getModel();
        
        model.__dirtyPosition = true;   
        model.position.x      = x;
        model.position.y      = y;
        model.position.z      = z;
        
    }
    
};



Model.prototype.getPosition = function () {

    // Retourne la position du collider à la place si il existe
    if ( this.getCollider() !== undefined && this.getCollider().getModel() !== undefined && this.getCollider().getModel().position !== undefined )
        return new THREE.Vector3( this.getCollider().getModel().position.x, this.getCollider().getModel().position.y, this.getCollider().getModel().position.z );
        // TODO: VERY IMPORTANT!!! Ceci est un bug, un collider normal est placé comme Model et un collider custom est placé comme objet!!!!
        // return new THREE.Vector3( this.getCollider().position.x, this.getCollider().position.y, this.getCollider().position.z );
    else
        return new THREE.Vector3( this.getModel().position.x, this.getModel().position.y, this.getModel().position.z );
    
};



Model.prototype.getPositionInsideCollider = function () {

    if ( this.getCollider() !== undefined )
        return new THREE.Vector3( this.getModel().position.x, this.getModel().position.y, this.getModel().position.z );
    
    return undefined;
    
};



Model.prototype.setRotation = function ( x, y, z ) {

    // On doit changer la rotation du collider si il existe
    if ( this.getCollider() !== undefined ) {
        
        var collider = this.getCollider().getModel();
        
        if ( collider !== undefined ) {
            
            collider.__dirtyRotation = true;
            collider.rotation.x      = x;
            collider.rotation.y      = y;
            collider.rotation.z      = z;
            
        }
        
    } else if ( this.getModel() !== undefined ) {
        
        var model = this.getModel();
        
        model.rotation.x = x;
        model.rotation.y = y;
        model.rotation.z = z;
        
    }
    
};



Model.prototype.setRotationInsideCollider = function ( x, y, z ) {

    if ( ( this.getCollider() !== undefined ) && ( this.getModel() !== undefined ) ) {
        
        var model = this.getModel();
        
        model.__dirtyRotation = true;
        model.rotation.x      = x;
        model.rotation.y      = y;
        model.rotation.z      = z;
        
    }
    
};



Model.prototype.getRotation = function () {

    // Retourne la rotation du collider à la place si il existe
    if ( this.getCollider() !== undefined )
        return new THREE.Vector3( this.getCollider().rotation.x, this.getCollider().rotation.y, this.getCollider().rotation.z );
    else
        return new THREE.Vector3( this.getModel().rotation.x, this.getModel().rotation.y, this.getModel().rotation.z );
    
};



Model.prototype.getRotationInsideCollider = function () {

    if ( this.getCollider() !== undefined )
        return new THREE.Vector3( this.getModel().rotation.x, this.getModel().rotation.y, this.getModel().rotation.z );
    
    return undefined;
    
};



Model.prototype.clone = function ()  {
    
    var model = new Model( this.getName(), this.getHRef() );

    if ( this.getModel() !== undefined )
        model.setModel( this.getModel().clone() );
    else
        console.warn( 'Cloned object has no model to clone!' );
    
    // TODO: JE NE SUIS PAS CERTAIN QUE ÇA MACHE A FAIRE DES TESTS
    // Clone aussi les materials
    var children = this.getChildren();
    for ( var i = 0; i < children.length; ++i ) {
        
        children[i].material = children[i].material.clone();
        
        children[i].material.needsUpdate = true;
        
        // On doit changer le nom pour éviter que le maping soit modifier par un autre objet!
        // TODO: Faire un renommage plus intelligent ici!
        children[i].material.name = children[i].material.name + '_' + Math.floor( 1000000000 * Math.random() );
        
    }
    
    // Clone aussi le collider
    if ( this.getCollider() !== undefined )        
        model.setCollider( this.getCollider().clone() );
    
    model.setName( this.getName() );
    model.setComplex( this.getComplex() );
    model.setDeferedData( this.getDeferedData() );       
    model.setHRef( this.getModel() );
    model.setScale( this.getScale() );
    
    return model;
    
}; 



Model.prototype.remove = function () {
    
    if ( this.getScene() !== undefined )
        this.getScene().remove( this );
    
};



Model.prototype.addTo = function ( scene ) {
    
    if ( scene !== undefined )
        scene.addModel( this );
    
};


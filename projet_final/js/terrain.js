/* INF5071 - Infographie - Travail Pratique 2 - UQAM A2015
 *
 *   Etudiant   : Stephane Olssen
 *   Code perm  : OLSS14037300
 *   Courriel   : olssen.stephane@courrier.uqam.ca
 *   
 *   Etudiant   : Thomas Robert de Massy
 *   Code perm  : ROBT24057409
 *   Courriel   : robert_de_massy.thomas@courrier.uqam.ca     
 *
 *   Professeur : Alexandre Blondin Massé
 *
 *   Chaque unite de Three.js est egale a 8 metres
 *
 *   length: longueur du terrain en metres
 *   width: largeur du terrain en metres
 *   scene: la scene Three.js
 *   proportionBuildings: Array contenant les proportions de chaque 
 *                      type de batisse[industriel, residentiel, parc, vide]
 *   blocDimensions: Array contenant la longeur et largeur d'un bloc en metres
 *
 */

var Terrain = function(terrainDimensions, scene, proportionBuildings, blocDimensions) {

    this.worldLength = this.worldWidth = 2000; // taille du monde ou le terrain se situe
    this.objects = []; // Array contenant tout les objects faisant parti du terrain
    this.unitsPerLot = 8; // unités three.js par lot de terrain
    this.metersPerLot = 8; // Nombree de metres par lot
    this.plane; // le plan qui modelise le terrain
    this.terrains = []; // Array contenant tous les plans du terrain

    this.blocks = []; // Array contenant le point en haut a gaiuche et en bas a droite de chaque bloc
    this.zoneTypeMap = []; // Array contenant les types d'edifices a placer

    this.buildingNumber = 0; // Le nombre de batisses industrielles et residentielles
    
    var td = [terrainDimensions[0] / this.metersPerLot,    terrainDimensions[1] / this.metersPerLot];
    var bd = [blocDimensions[0]    / this.metersPerLot,    blocDimensions[1]    / this.metersPerLot];   
    
    // dimension du terrain en metres (avec ajustement si il manque de la place)
    if (td[0] % (bd[0] + 1) === 1)
        this.length = td[0] * this.metersPerLot;
    else
        this.length = ( Math.floor(td[0] / (bd[0] + 1)) * (bd[0] + 1) + (td[0] % (bd[0] + 1)) + (bd[0] + 1) - (td[0] % (bd[0] + 1)) + 1 ) * this.metersPerLot;

    if (td[1] % (bd[1] + 1) === 1)
        this.width = td[1] * this.metersPerLot;
    else
        this.width = ( Math.floor(td[1] / (bd[1] + 1)) * (bd[1] + 1) + (td[1] % (bd[1] + 1)) + (bd[1] + 1) - (td[1] % (bd[1] + 1)) + 1 ) * this.metersPerLot;

    // dimension interne en bloc
    this.lUnit = this.length / this.metersPerLot;
    this.wUnit = this.width / this.metersPerLot;

    // la scene three.js
    this.scene = scene.getScene();
    this.sceneObj = scene;


    // les ratios des differentes zones en pourcentage
    this.ratioZones = {
        industrial: 33.3,
        residential: 33.3,
        park: 33.4,
        empty: 0.0
    };

    this.map = []; // carte interne du terrain diviser par rapport a l'echelle

    this.tailleBlocH = blocDimensions[0] / this.metersPerLot; // taille d'un bloc
    this.tailleBlocV = blocDimensions[1] / this.metersPerLot; // taille d'un bloc

    this.nbBlocs = 0; // nombre de blocs complet sur le terrain
    this.materials = []; // array contenant les materiaux [gazon, route horizontale, route verticale, intersection de route]

    this.calculateProportions(proportionBuildings);
    this.createMap();
    this.mapRoadsAndBlocks();
    this.calculateBuildingProportions();
    this.placeBuildings();
    this.createGround();
    this.createTerrain();

    // place les forets autour de la ville si dans la configuration
    if (this.sceneObj.getConfig("cityDecoration") === true) {
        this.placeForests();
    }
};

	 /*
	  * Fonction qui genere la carte interne
	  */
	 Terrain.prototype.createMap = function() {
        for (var i = 0; i < this.wUnit; i++) {
            this.map[i] = [];
            for (var j = 0; j < this.lUnit; j++) {
                this.map[i][j] = "grass"; // initialise chaque case pour du gazon
            }
        }
    },

    /*
     *  Calculer les differents pourcentages des batisses
     */
    Terrain.prototype.calculateProportions = function(values) {

        var total = values[0] + values[1] + values[2] + values[3];

        // calcule des valeurs de 0-1 pour les pourcentages
        this.ratioZones.industrial = values[0] / total;
        this.ratioZones.residential = values[1] / total;
        this.ratioZones.park = values[2] / total;
        this.ratioZones.grass = values[3] / total;
    },

    /*
     * Fonction qui place les routes sur la carte interne et calcule le nombre de bloc de batisses
     */
    Terrain.prototype.mapRoadsAndBlocks = function() {
        var nbBlocsHorz = 0;
        var nbBlocsVert = 0;
        // routes verticales
        for (var i = 0; i < this.wUnit; i++) {
            if (i % (this.tailleBlocV + 1) == 0) {
                if (i != 0) {
                    nbBlocsHorz++;
                }
                for (var j = 0; j < this.lUnit; j++) {
                    this.map[i][j] = "vRoad";
                }
            }
        }
        // routes horizontales
        for (var i = 0; i < this.lUnit; i++) {
            if (i % (this.tailleBlocH + 1) == 0) {
                if (i != 0) {
                    nbBlocsVert++;
                }
                for (var j = 0; j < this.wUnit; j++) {
                    if (this.map[j][i] == "vRoad") {
                        this.map[j][i] = "interRoad"; // intersection
                    } else {
                        this.map[j][i] = "hRoad";
                    }
                }
            }
        }
        this.nbBlocs = nbBlocsHorz * nbBlocsVert;
    },
    /*
     * Fonction pour le chargement de texures pour les modeles
     */
    Terrain.prototype.loadTexture = function(path, repeat) {

        var loader = new THREE.TextureLoader();

        texture = loader.load(
            path,
            // Callback lorsque la texture est chargee
            function(texture) {},
            // Callback lorque la te=xture est entrain de se faire charger
            function(xhr) {
                console.log((xhr.loaded / xhr.total * 100) + '% charge');
            },
            // Callback si erreur
            function(xhr) {
                console.log('Erreur lors du chargement de la texture de la route horizontale');
            }
        );
        // si on defini une repetition de texture le faire
        if (repeat != undefined) {
            texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
            texture.repeat.set(repeat[0], repeat[1]);
        }

        // Pour éliminer le BLUR avec les distances
        texture.minFilter = THREE.LinearMipMapLinearFilter;
        texture.anisotropy = 2;


        // retourne le meteriel physi.js construit avec la texture
        return new Physijs.createMaterial(new THREE.MeshPhongMaterial({
            map: texture
        }), 0.8, 0.1);
    },
    /*
     *  Fonction pour creer la terre
     */
    Terrain.prototype.createGround = function() {
        var plane = new THREE.PlaneGeometry(2000, 2000, 1, 1); // un plane de 2000x2000
        var grassMat = this.loadTexture('assets/textures/grass.png', [this.worldWidth, this.worldLength]); // gazon
        var floor = new Physijs.HeightfieldMesh(plane, grassMat, 0);
        floor.material.side = THREE.DoubleSide;
        floor.rotation.x = -Math.PI / 2; // l'aligner avec notre monde

        floor.position.x = 0;
        floor.position.y = -0.02; // le placer un peu en-dessous de la ville
        floor.position.z = 0;

        // recoit des ombrages
        floor.receiveShadow = true;

        this.objects.push(floor);

        // Ajoute le modèle à la liste des modèles de la scène
        var modelObj = new Model("floor");
        modelObj.setModel(floor);
        this.sceneObj.addModel(modelObj);

    },

    /*
    / Fonction que genere la ville
    */
    Terrain.prototype.createTerrain = function() {
        var plane; // un case de la ville
        var terrain; // le terrain de la ville
        // charge nos textures de sol
        var grassMat = this.loadTexture('assets/textures/grass.png', [10, 10]);
        var roadHorzMat = this.loadTexture('assets/textures/asphalt-lignes-h-trottoir-1024.png');
        var roadVertMat = this.loadTexture('assets/textures/asphalt-lignes-v-trottoir-1024.png');
        var roadInterMat = this.loadTexture('assets/textures/asphalt-lignes-inter-1024.png');
        // pour tous les cases horizontales
        for (var i = 0; i < this.wUnit; i++) {
            // pour tous les cases verticales
            for (var j = 0; j < this.lUnit; j++) {

                // creer un plane
                plane = new THREE.PlaneGeometry(this.unitsPerLot, this.unitsPerLot, 1, 1);
                // place le bon materiel dependant de la carte interne
                switch (this.map[i][j]) {
                    case "grass":
                        material = grassMat;
                        break;
                    case "vRoad":
                        material = roadVertMat;
                        // si decoration de ville est mis dans la configuration
                        if (this.sceneObj.getConfig("cityDecoration") === true) {
                            this.placeTreesByRoad(i, j, "vertical"); // place les arbres de bord de rue verticale

                            // Ajout des lignes électrique et lumières de rue
                            this.placeStreetPoles(i * this.unitsPerLot, j * this.unitsPerLot, i % 2, 0);

                        }
                        break;
                    case "hRoad":
                        material = roadHorzMat;
                        // si decoration de ville est mis dans la configuration
                        if (this.sceneObj.getConfig("cityDecoration") === true) {
                            this.placeTreesByRoad(i, j, "horizontal"); // place les arbres de bord de rue horizontale

                            // Ajout des lignes électrique et lumières de rue
                            this.placeStreetPoles(i * this.unitsPerLot, j * this.unitsPerLot, j % 2, 1);
                        }
                        break;
                    case "interRoad":
                        material = roadInterMat;
                        // si decoration de ville est mis dans la configuration
                        if (this.sceneObj.getConfig("cityDecoration") === true && i !== 0 && i !== this.wUnit - 1 && j !== 0 && j !== this.lUnit - 1 ) {
                            this.placeTrashCansByIntersection(i, j); // place les poubelles
                            this.placeTrafficLightsOrStopSignsByIntersection(i, j); // places les feux de corculaiton et les panneaux Stop
                        }
                        break
                    default:
                        null;
                }

                // Enlever la réflection specular sur le gazon
                if (material === grassMat)
                    material.shininess = 1;

                // creer la case
                terrain = new Physijs.HeightfieldMesh(plane, material, 0);

                // rotation pour aligner avec notre monde
                terrain.rotation.x = -Math.PI / 2;

                // placer au bon endroit
                terrain.position.x = i * this.unitsPerLot;
                terrain.position.y = 0 * this.unitsPerLot;
                terrain.position.z = j * this.unitsPerLot;

                terrain.material.side = THREE.DoubleSide;

                this.terrains.push(terrain);
            }
        }
    },
    /*
     * Fonction pour placer les Stops et les feux de circulation
     */
    Terrain.prototype.placeTrafficLightsOrStopSignsByIntersection = function(i, j) {
        // choisr un stop ou un feu depandant des proportion specifie dans la configuration
        var itemType = Math.random() < this.sceneObj.getConfig("lightsToStopsProp") ?
            ID.TRAFFIC_LIGHTS_SET : ID.STOP_4_DIRECTIONS;
        theScene.add(itemType, {
            x: i * this.unitsPerLot,
            y: j * this.unitsPerLot
        });

    },
    /*
     * Fonction pour placer des arbres sur le bord de la rue
     */
    Terrain.prototype.placeTreesByRoad = function(i, j, dir) {
        if (dir == "vertical") {
            theScene.add(ID.TREE, {
                x: i * this.unitsPerLot + (this.unitsPerLot / 2),
                y: j * this.unitsPerLot
            });
            theScene.add(ID.TREE, {
                x: i * this.unitsPerLot - (this.unitsPerLot / 2),
                y: j * this.unitsPerLot
            });
        } else { // horizontal
            theScene.add(ID.TREE, {
                x: i * this.unitsPerLot,
                y: j * this.unitsPerLot + (this.unitsPerLot / 2)
            });
            theScene.add(ID.TREE, {
                x: i * this.unitsPerLot,
                y: j * this.unitsPerLot - (this.unitsPerLot / 2)
            });

        }
    },
    /*
     * Fonction pour placer les poubelles aux coins de rue
     */
    Terrain.prototype.placeTrashCansByIntersection = function(i, j) {

        var trash = [];

        // ajoute les 4 poubelles et decider si elle est tombe par rapport a la probabilite dans la configuration
        trash[0] = theScene.add(ID.TRASH_CAN, {
                x: 4 + i * this.unitsPerLot,
                y: 3.5 + j * this.unitsPerLot
            },
            Math.random() < this.sceneObj.getConfig("fallenTrashProb") ?
            new THREE.Vector3(Math.PI / 2, 0, 0) : undefined);
        trash[1] = theScene.add(ID.TRASH_CAN, {
                x: 4 + i * this.unitsPerLot,
                y: -3.5 + j * this.unitsPerLot
            },
            Math.random() < this.sceneObj.getConfig("fallenTrashProb") ?
            new THREE.Vector3(Math.PI / 2, 0, 0) : undefined);
        trash[2] = theScene.add(ID.TRASH_CAN, {
                x: -4 + i * this.unitsPerLot,
                y: 3.5 + j * this.unitsPerLot
            },
            Math.random() < this.sceneObj.getConfig("fallenTrashProb") ?
            new THREE.Vector3(Math.PI / 2, 0, 0) : undefined);
        trash[3] = theScene.add(ID.TRASH_CAN, {
                x: -4 + i * this.unitsPerLot,
                y: -3.5 + j * this.unitsPerLot
            },
            Math.random() < this.sceneObj.getConfig("fallenTrashProb") ?
            new THREE.Vector3(Math.PI / 2, 0, 0) : undefined);

    },
    /*
     * Fonction pour dessiner(render) le terrain
     */
    Terrain.prototype.render = function() {
        for (i = 0; i < this.terrains.length; i++) {

            // recoit des ombrages
            this.terrains[i].receiveShadow = true;

            this.objects.push(this.terrains[i]);

            // Ajoute le modèle à la liste des modèles de la scène
            var modelObj = new Model("terrain_" + i);
            modelObj.setModel(this.terrains[i]);
            this.sceneObj.addModel(modelObj);

        }
    },
    /*
     * Fonction qui place les batisses
     */
    Terrain.prototype.placeBuildings = function() {

        var that = this;
        var direction; // direction de la batisse
        for (var i = 0; i < this.wUnit; i++) { // pour chaque case horizontale
            for (var j = 0; j < this.lUnit; j++) { // pour chaque case verticale
                if (this.map[i][j] == 'grass') { // si c'est du gazon c'est pas une rue donc contruisable
                    direction = this.getBuildingDirection(i, j); // calcule la direction du batisse
                    if (direction != 'none') {
                        var zoneType = this.zoneTypeMap.pop(); // type de zonnage
                        var type = 0;
                        switch (zoneType) {
                            case 'industrial': // choisir un batisse par rapport au zonage
                                type = ID.industrial[this.pickRandomPropertyType(ID.industrial)];
                                break;
                            case 'residential':
                                type = ID.residential[this.pickRandomPropertyType(ID.residential)];
                                break;
                            case 'park':
                                type = ID.publique[this.pickRandomPropertyType(ID.publique)];
                                break;
                            case 'empty':
                                type = -1;
                                break;
                            default:
                                break;
                        }
                        if (type != -1) {
                            var orientation;
                            // rotation dans la bonne direction
                            switch (direction) {

                                case 'up':
                                    orientation = 0;
                                    break;
                                case 'down':
                                    orientation = 2;
                                    break;
                                case 'right':
                                    orientation = 1;
                                    break;
                                case 'left':
                                    orientation = 3;
                                    break;
                                default:
                                    orientation = 0;
                                    break;
                            }
                            this.buildingNumber++;
                            // calcule la bonne hauteur des batisses
                            var max_height = 12;
                            var min_height = 3;
                            // mode centre ville pour des batisses industrielle en gratte-ciel
                            if (theScene.getConfig("hasDowntown")) {
                                max_height = Math.floor(this.calculateMaxHeight(i, j, max_height, type));
                                min_height = Math.floor(this.calculateMinHeight(i, j, min_height, max_height, type));
                            }
                            
                            // Les maisons normales n'on pas beaucoup d'étages
                            if ( type === ID.TYPE_HOUSE_A1 || type === ID.TYPE_HOUSE_B1 ) {
                                min_height = 1;
                                max_height = 3;
                            }                            

                            // si un parc
                            if (zoneType !== 'park') {

                                var building = theScene.add(ID.BUILDING, {
                                    x: (i * this.unitsPerLot),
                                    y: (j * this.unitsPerLot)
                                }, orientation, {

                                    type: type,
                                    size: new THREE.Vector3(2, Math.min(Math.floor(Math.random() * max_height) + min_height, max_height), 2),
                                    name: "building" + this.buildingNumber,
                                    scale: (0.14 * this.unitsPerLot),
                                    animateLights: true,
                                    animateLightsVariation: 100,
                                    color1: theScene.rgbToInt(200 + (Math.random() * 55), 200 + (Math.random() * 55), 200 + (Math.random() * 55)),
                                    color2: theScene.rgbToInt(150 + (Math.random() * 105), 150 + (Math.random() * 105), 150 + (Math.random() * 105)),
                                    color3: theScene.rgbToInt(150 + (Math.random() * 105), 150 + (Math.random() * 105), 150 + (Math.random() * 105)),
                                    color4: theScene.rgbToInt(150 + (Math.random() * 105), 150 + (Math.random() * 105), 150 + (Math.random() * 105)),
                                    color5: theScene.rgbToInt(200, 200, 150 + (Math.random() * 105))

                                }, function(newbuilding) {

                                    that.objects.push(newbuilding);

                                });

                            } else { // Zone publique

                                switch (type) {

                                    case ID.PARK:

                                        var col_min = 130;
                                        var col_max = 255;

                                        var public_site = theScene.add(ID.PARK, {
                                            x: (i * this.unitsPerLot),
                                            y: (j * this.unitsPerLot)
                                        }, 0, {

                                            showCollider: false,
                                            size_x: 2,
                                            size_z: 2,
                                            fences_height: Math.random() < 0.5 ? 0.5 : 1,
                                            fences_door: true,
                                            scale: (0.14 * this.unitsPerLot),
                                            fences_door_position: orientation,
                                            fences_color: new THREE.Color(Math.max(col_max, Math.random() * col_max + col_min), Math.max(col_max, Math.random() * col_max + col_min), Math.max(col_max, Math.random() * col_max + col_min)).getHex(),
                                            fences_type: 0

                                        }, function(elements) {

                                            that.objects.push(elements);

                                        });

                                        break;

                                    case ID.CONSTRUCTION_SITE:

                                        var public_site = theScene.add(ID.CONSTRUCTION_SITE, {
                                            x: (i * this.unitsPerLot),
                                            y: (j * this.unitsPerLot)
                                        }, 0, {

                                            showCollider: false,
                                            size_x: 2,
                                            size_z: 2,
                                            fences_height: 1,
                                            fences_door: true,
                                            scale: (0.14 * this.unitsPerLot),
                                            fences_door_position: orientation,
                                            fences_type: 2

                                        }, function(elements) {

                                            that.objects.push(elements);

                                        });

                                        break;

                                }


                            }
                        }
                    }
                }
            }
        }
    },
    /*
     * Fonction pour calculer l'hauteur maximum par rapport ou le batisse est dans la ville.
     * Plus la case est dans le millieu, plus l'edifice est haut
     */
    Terrain.prototype.calculateMaxHeight = function(i, j, max_height, type) {

        // fonction pour savoir si une valeur existe dans un objet
        isInObject = function(element, obj) {

            for (var k in obj) {
                if (obj[k] === element) {
                    return true; // trouve
                }
            }
            return false; // pas trouve
        }

        var weight; // poid de modification d'hauteur

        // residentielles
        if (isInObject(type, ID.residential) === true) {
            max_height = 3;
        }

        // les autres industrielles
        else if (isInObject(type, ID.industrial) === true) {
            max_height = 14;
            // Certains types de industrielles doivent avoir un minimum d'étages
            if (type === ID.TYPE_SKYSCRAPER_B) {

                max_height = 10;

            }
            if (i > this.map.length / 2) { // si deuxieme moite sur axe des x
                var weightX = this.map.length - i; // prendre difference
            } else {
                var weightX = i; // sinon prendre axe des x
            }
            weight = weightX / this.map.length;

            if (j > this.map[0].length / 2) { // si 2ieme moitie sur axe des y
                var weightY = +this.map[0].length - j; // prendre difference
            } else {
                var weightY = +j; // sinon prendre axe des y
            }
            weight = +weightY / this.map.length; // ajoute le poid des y

            weight = weight ^ 2; // augmenter par une puissance de 2

            //weight = weight/2;
            max_height = max_height + (max_height * weight);

            if (weight > .8 && Math.random() > .5) { // mettre quelques gratte ciel
                max_height = max_height * 4;
            }
        }
        return max_height;
    },
    /*
     * Fonction pour calculer l'hauteur minimum
     */
    Terrain.prototype.calculateMinHeight = function(i, j, min_height, max_height, type) {

        // fonction pour savoir si une valeur existe dans un objet
        isInObject = function(element, obj) {
            for (var k in obj) {
                if (obj[k] === element) {
                    return true; // trouve 
                }
            }
            return false; // pas trouve
        }

        // residentielles
        if (isInObject(type, ID.residential) === true) {
            // Les maisons normales n'on pas beaucoup d'étages
            if (type === ID.TYPE_HOUSE_B1) {
                min_height = 2;
            }
        }

        // les autres industrielles
        else if (isInObject(type, ID.industrial) !== true) {

            min_height = min_height * 3;

            // Certains types de industrielles doivent avoir un minimum d'étages
            if (type === ID.TYPE_SKYSCRAPER_B) {

                min_height = 3;

            }
        }
        if (min_height > max_height) { // assurer que l'hauteur minimumest inferieur au maximum
            min_height = max_height - 1;
        }
        return min_height;
    },
    /*
     * Fonction qui retourne la direction d'un batisse
     */
    Terrain.prototype.getBuildingDirection = function(x, y) {
        var u, d, l, r, ur, ul, dr, dl; // upper, down, left, right, etc.
        u = d = l = r = ur = ul = dr = dl = 'grass'; // mettre tous a du gazon

        // Trouver les 8 cases adjacent, si hors terrain alors traiter comme du gazon
        if (this.map[x - 1] != undefined) {
            l = this.map[x - 1][y]; // gauche
            if (this.map[x - 1][y - 1] != undefined) {
                ul = this.map[x - 1][y - 1]; // haut gauche
            }
            if (this.map[x - 1][y + 1] != undefined) {
                dl = this.map[x - 1][y + 1]; // bas gauche
            }
        }
        if (this.map[x][y + 1] != undefined) {
            d = this.map[x][y + 1]; // bas
        }
        if (this.map[x][y - 1] != undefined) {
            u = this.map[x][y - 1]; // haut
        }
        // Coins droite
        if (this.map[x + 1] != undefined) {
            r = this.map[x + 1][y]; // droite
            if (this.map[x + 1][y - 1] != undefined) {
                ur = this.map[x + 1][y - 1]; // haut droit
            }
            if (this.map[x + 1][y + 1] != undefined) {
                dr = this.map[x + 1][y + 1]; //bas droit
            }

        }
        return this.calculateDirection(u, d, l, r, ur, ul, dr, dl);
    },
    // Fonction qui calcule la direction d<un batisse par rapport aux case autour de lui
    Terrain.prototype.calculateDirection = function(u, d, l, r, ur, ul, dr, dl) {

        var direction = 'none';

        // si la case en haut a gauche est une intersection alors
        if (ul == 'interRoad') {
            //choisi une direction haut ou gauche au hasard
            if (Math.floor((Math.random() * 2) + 1) > 1) {
                direction = "up";
            } else {
                direction = "left";
            }
        }
        // si la case en bas a gauche est une intersection alors
        else if (dl == 'interRoad') {
            //choisi une direction bas ou gauche au hasard
            if (Math.floor((Math.random() * 2) + 1) > 1) {
                direction = "down";
            } else {
                direction = "left";
            }
        }
        // si la case en haut a droite est une intersection alors
        else if (ur == 'interRoad') {
            //choisi une direction haut ou droite au hasard
            if (Math.floor((Math.random() * 2) + 1) > 1) {
                direction = "up";
            } else {
                direction = "right";
            }
        }
        // si la case en bas a droite est une intersection alors
        else if (dr == 'interRoad') {
            //choisi une direction bas ou droite au hasard
            if (Math.floor((Math.random() * 2) + 1) > 1) {
                direction = "down";
            } else {
                direction = "right";
            }
        }
        // si la case en haut est une route alors
        else if (u == 'hRoad') {
            // mettre la direction en haut
            direction = "up";
        }
        // si la case en bas est une route alors
        else if (d == 'hRoad') {
            // mettre la direction en bas
            direction = "down";
        }
        // si la case a gauche est une route alors
        else if (l == 'vRoad') {
            // mettre la direction a gauche
            direction = "left";
        }
        // si la case a droite est une route alors
        else if (r == 'vRoad') {
            // mettre la direction a droite
            direction = "right";
        }

        return direction;
    },
    /*
     * Fonction qui calcule les proportions de batisses qu'on doit placer
     */
    Terrain.prototype.calculateBuildingProportions = function() {

        var nbResidential = 0;
        var nbIndustrial = 0;
        var nbPark = 0;
        var total = 0;

        this.findBlocks(); // trouve le nombre de blocs

        // calculer le nombre d'implacement par bloc
        for (var i = 0; i < this.blocks.length; i++) {

            var length = this.blocks[i][3] - this.blocks[i][1] + 1;
            var width = this.blocks[i][2] - this.blocks[i][0] + 1;

            total += ((length * 2) + (width * 2)) - 4;
        }

        // toruve le nombre reels des differents types de batisse a placer
        nbResidential = Math.round(total * this.ratioZones.residential);
        nbIndustrial = Math.round(total * this.ratioZones.industrial);
        nbPark = Math.round(total * this.ratioZones.park);
        nbEmpty = Math.round(total * this.ratioZones.empty);

        // les ajouter au array de type de batisses a placer
        for (var i = 0; i < nbIndustrial; i++) {
            this.zoneTypeMap.push('industrial');
        }
        for (var i = 0; i < nbResidential; i++) {
            this.zoneTypeMap.push('residential');
        }
        for (var i = 0; i < nbPark; i++) {
            this.zoneTypeMap.push('park');
        }
        for (var i = 0; i < nbEmpty; i++) {
            this.zoneTypeMap.push('empty');
        }

        this.zoneTypeMap = shuffle(this.zoneTypeMap); // melanger le array pour avoir un placement aleatoire

        // inspirer de http://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
        function shuffle(array) {
            var i = array.length; // index courrante
            var temp; // valeur temporaire pour l'echange de valeurs
            var randomI; // l'index aleatoire pour changer avec l'indexe courrante

            // Itere par dessus tous les elements
            while (0 !== i) {
                // Choisir un index au hasard
                randomI = Math.floor(Math.random() * i);
                i -= 1;

                // l'echaner avec l'index courrant
                temp = array[i];
                array[i] = array[randomI];
                array[randomI] = temp;
            }
            return array;
        }
    },
    /*
     * Fonction qui choissi un type de batisse au hasard
     */
    Terrain.prototype.pickRandomPropertyType = function(types) {

        var res;
        var nb = 0;

        for (var type in types) // pour chaque type

            if (Math.random() < 1 / ++nb) { // retourne un au hasard avec probabilite qui augemente pour chaqu'un
                res = type;
            }

        return res;
    },
    /*
     * Fonction pour trouver le nombre de bloc
     */
    Terrain.prototype.findBlocks = function() {
        for (var i = 0; i < this.wUnit; i++) { // pour chaque ligne horizontale de la carte
            for (var j = 0; j < this.lUnit; j++) { // pour chaque ligne veritcale
                if (i > 0 && j > 0) {
                    // Si nous somme dans le coin en haut a gauche d'un bloc
                    if ((this.map[i - 1][j - 1] == "interRoad" ||
                            this.map[i - 1][j - 1] == undefined) &&
                        this.map[i][j] == "grass") {

                        var k = i;
                        var l = j;

                        // trouve la largeur du block
                        while (this.map[k][l] != undefined &&
                            this.map[k][l] == "grass") {
                            k++;
                            if (this.map[k] == undefined) { // break si trop loin dans la matrice
                                break;
                            }
                        }
                        k--; // de-incremente pour la derniere passe
                        // trouve l'hauteur du bloc
                        while (this.map[k][l] != undefined &&
                            this.map[k][l] == "grass") {
                            l++;
                            if (this.map[l] == undefined) { // break si trop lloin dans la matrice
                                break;
                            }
                        }
                        l--; // de-incremente pour la derniere passe
                        this.blocks.push([
                            [i],
                            [j],
                            [k],
                            [l]
                        ]);
                    }
                }
            }
        }
    },
    /*
     *  Fonction pour placer des forets autour de la ville
     */
    Terrain.prototype.placeForests = function() {

        // pour chaque lot horizontale de la ville
        for (var i = -5; i <= this.width + 5; i++) {

            var x = i;
            if (Math.random() > .7) { // au Nord de la ville
                var y = Math.floor(Math.random() * 5) + 1; // distance entre 1 un 5 unites de plus loin que la ville

                theScene.add(ID.items.FOREST, {
                    x: x,
                    y: -1 * y
                }, undefined, {
                    units: this.unitsPerLot
                });
            }

            if (Math.random() > .7) { // Au sud de la ville
                var y = Math.floor(Math.random() * 5) + 1; // distance entre 1 un 5 unites de plus loin que la ville

                theScene.add(ID.items.FOREST, {
                    x: x,
                    y: y + this.length
                }, undefined, {
                    units: this.unitsPerLot
                });
            }
        }
        // pour chaque lot veritcale de la ville
        for (var i = -5; i <= this.length + 5; i++) {

            var y = i;

            if (Math.random() > .7) { // l'Ouest de la ville
                var x = Math.floor(Math.random() * 5) + 1; // distance entre 1 un 5 unites de plus loin que la ville

                theScene.add(ID.items.FOREST, {
                    x: -1 * x,
                    y: y
                }, undefined, {
                    units: this.unitsPerLot
                });
            }

            if (Math.random() > .7) { // A l'Est de la ville
                var x = Math.floor(Math.random() * 5) + 1; // distance entre 1 un 5 unites de plus loin que la ville

                theScene.add(ID.items.FOREST, {
                    x: x + this.width,
                    y: y
                }, undefined, {
                    units: this.unitsPerLot
                });
            }
        }

    }

/*
 * Fonction pour placer les poteaux electriques
 */
Terrain.prototype.placeStreetPoles = function(x, z, side, direction) {

    // TODO: POUR L'INSTANT JE DÉSACTIVE LES LUMIERES DEFFECTUEUSES CAR SEMBLE CAUSER UN PROBLEME AVEC LA LUMIERE AU SOL
    var params = {}; //{ defectiveness: 0 };

    if (Math.random() < 0.5)
        params.lights = false; // Pas de lampadaire sur la ligne

    if (direction === 0) {

        if (side === 0)
            theScene.add(ID.ELECTRIC_LINE, {
                x: x,
                z: z
            }, 0, params, function(elements) {});
        else
            theScene.add(ID.ELECTRIC_LINE, {
                x: x,
                z: z
            }, 2, params, function(elements) {});

    } else {

        if (side === 0)
            theScene.add(ID.ELECTRIC_LINE, {
                x: x,
                z: z
            }, 1, params, function(elements) {});
        else
            theScene.add(ID.ELECTRIC_LINE, {
                x: x,
                z: z
            }, 3, params, function(elements) {});

    }

}

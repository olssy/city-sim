**INF5071 - Infographie - UQAM A2015**

## Travail pratique 2 ##

 
**Étudiants :**

Stephane Olssen OLSS14037300
 
Thomas Robert de Massy ROBT24057409

**Professeur :**

Alexandre Blondin Massé

**Description**

Une simulation de ville avec des rues créées de façon procédurale ainsi que des bâtisses modelées dans Blender mais modifier aussi de façon procédurale dans l'application avec des hauteurs, teintes, fenêtres et toits variables. On peut explorer la ville, changer des options et des paramètres pour créer toutes sortes de variations. La ville est remplie d'objets uniques tels que des poubelles, des feux de circulation (synchroniser ensemble et qui change de lumière), des lampadaires fonctionnels (quelques un de "brisés" comme si une ampoule était brûlée), un ciel étoilé la nuit qui bouge et un soleil animé le jour et plusieurs autres détails à explorer.

**Contenu du projet**

	/fichiers_developpement/  répertoire contenant les diffférents fichiers utilisés pour concevoir le projet.
	/projet_final/            repertoire contenant le code exécutable du projet.

**Utilisation**

La simulation devrait partir toute seule lorsqu'on visite son URL, on peut ensuite modifier les paramètres et les options pour générer une nouvelle ville.

Localement pour démarrer le projet vous devez ouvrir le fichier projet_final/index.html* dans un navigateur. Les dernières versions des navigateurs Firefox*, Google Chrome, Internet Explorer et Opera sont supportés.

IMPORTANT: Si le projet est ouvert localement sur la machine, vous devez passer par un serveur web pour l'utiliser. Soit en utilisant un serveur web (ex: Apache) ou en utilisant Python et la commande suivante dans l'invite de commande au répertoire contenant le fichier *index.html*:

**Déplacements**

Les flèches servent pour les déplacements dans le monde ainsi que "Page Up" et "Page Down" pour regarder en haut et en bas.

**Paramètres**

Il y a des paramètres pour la longueur et la largeur de la ville ainsi que ceux des blocs de bâtiments, les proportions des différents types de bâtisses ainsi que la durée d'une journée dans la simulation en secondes. Dans les paramètres on peut aussi choisir de simuler un centre-ville (des gratte-ciel) ou afficher/cacher les éléments décoratifs de la ville (poubelles, arbres, panneaux de signalisations, lampadaires, feux de circulation, etc.)

Finalement, lorsque les paramètres sont choisis, il suffit de cliquer sur le bouton "Générer monde" pour créer une nouvelle ville avec les paramètres choisis.

**Options**

Les options sont appliquées en temps réel et  ne nécessitent pas la génération d'une nouvelle ville. Une première option est "ombrage" qui active ou désactive les ombrages dans la ville, en les désactivant on peut encore voir ceux qui étaient là, mais ils ne bougent plus avec l’éclairage. Ensuite, il y a l'option "Éclairage dynamique" qui à pour effet de remplacer la lumière du soleil par une lumière fixe qui ne change pas avec le temps.

**Limitation**

Parfois, lors de la génération d'une nouvelle ville, le monde ne se fait pas générer. Ça n'arrive pas souvent et nous avons cherché longtemps la cause sans jamais la trouver. C'est une erreur causée par l'engin physique pour on ne sait quelle raison. Il faut faire un "refresh" dans le furteur pour refaire marcher la physique.

Par défaut la qualité des graphiques est au maximum, sur un ordinateur ne possédant pas une carte graphique assez puissante les mouvements peuvent êtres saccadés, il suffit de désactiver les décorations de ville et les ombrages pour amélioré grandement la fluidité.

**Ressources**

Le plugin physi.js de three.js à été utilisé pour la physique du projet.

Tous les modèles 3D ont étés conçus à la main dans Blender.

Tous les textures viennent du site *www.textures.com* sauf les textures des gazons et de rues qui ont été faites par nous et les textures des boîtes de cartons qui viennent des sites *http://www.3dxo.com/textures/paper_cardboard* et *http://naldzgraphics.net/textures/cardboard-texture/*

